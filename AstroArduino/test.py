import time
import serial
import struct

def set_led_states(port, abort_state, engine_state):
    port.write([0x20 | int(abort_state) | (int(engine_state) << 1)])

def get_button_states(port):
    port.write([0x10])
    data = struct.unpack("<H", port.read(2))[0]
    states = []
    for i in xrange(9):
        states.append(bool(data & (1 << i)))
    return states
   
p = serial.Serial("COM11", baudrate=115200, timeout=10)
while len(p.read(1)) == 0: time.sleep(0.01)

def loop():
    set_led_states(p, False, False)
    print get_button_states(p)
    time.sleep(0.25)
    set_led_states(p, True, False)
    time.sleep(0.25)
    set_led_states(p, False, True)
    time.sleep(0.25)
    set_led_states(p, True, True)
    time.sleep(0.25)

try:
    while True:
        loop()
except KeyboardInterrupt:
    pass

p.close()
