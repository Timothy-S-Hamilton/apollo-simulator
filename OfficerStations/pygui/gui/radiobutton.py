from __future__ import division
import pygui
from pygui.gui.widget import Widget
from pygui.gui.label import Label


class RadioButton(Widget):
    def __init__(self, x, y, size, text, color=pygui.gray):
        super(RadioButton, self).__init__(x, y, color)
        self.size = size
        self.outline_color = pygui.dark_gray
        self.filled_color = pygui.very_dark_gray
        self.pressed = False
        self.in_flag = False
        self.label = Label(x + self.size * 2 + 10, y + 1, text, color=pygui.white)
        self.label.halign = "left"
        self.width = int(self.size / 2) + self.label.width
        self.height = int(self.size / 2) + self.label.height

    def update(self):
        self.label.x = self.x + self.size * 2 + 10
        self.label.y = self.y + 1

    def render(self):
        if self.visible:
            pygui.draw_circle((int(self.x), int(self.y)), self.size, self.color)
            pygui.draw_circle((int(self.x), int(self.y)), self.size + 1, self.outline_color, 2)
            if self.pressed:
                pygui.draw_circle((int(self.x), int(self.y)), int(self.size / 1.5), self.filled_color)
            self.label.render()

    def event(self, e_type, e_value):
        if e_type == "MouseDown" and e_value == 1:
            (mx, my) = pygui.get_mouse_position()
            distance = ((mx - self.x) ** 2 + (my - self.y) ** 2) ** (1 / 2)
            if distance <= self.size:
                self.in_flag = True
        if e_type == "MouseUp" and e_value == 1:
            if self.in_flag:
                self.in_flag = False
                (mx, my) = pygui.get_mouse_position()
                distance = ((mx - self.x) ** 2 + (my - self.y) ** 2) ** (1 / 2)
                if distance <= self.size:
                    self.in_flag = False
                    if self.pressed:
                        self.pressed = False
                    else:
                        self.pressed = True


class RadioButtonGroup(Widget):
    def __init__(self, is_exclusive=True):
        super(RadioButtonGroup, self).__init__(0, 0, pygui.black)
        pygui.add_mouse_listener(self)
        self.buttons = []
        self.exclusive = is_exclusive

    def add_button(self, rb):
        self.buttons.append(rb)

    def remove_button(self, rb):
        self.buttons.remove(rb)

    def get_activated(self):
        l = []
        for b in self.buttons:
            l.append(b.pressed)
        return l

    def get_activated_dict(self):
        l = {}
        for b in self.buttons:
            l[b.label.text] = b.pressed
        return l

    def deactivate_all(self):
        for b in self.buttons:
            b.pressed = False
            b.in_flag = False

    def get_buttons(self):
        return self.buttons

    def update(self):
        for b in self.buttons:
            b.x = self.x + b.start_pos[0]
            b.y = self.y + b.start_pos[1]
            b.update()

    def render(self):
        for b in self.buttons:
            b.render()

    def event(self, e_type, e_value):
        for b in self.buttons:
            p = b.pressed
            b.event(e_type, e_value)
            if p != b.pressed:
                if self.exclusive:
                    self.deactivate_all()
                    b.pressed = True
