﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApolloGuidanceComputer : MonoBehaviour
{
    public Text ProgDisplayText;
    public Text VerbDisplayText;
    public Text NounDisplayText;
    public Text R1DisplayText;
    public Text R2DisplayText;
    public Text R3DisplayText;
    public Text DeltaVRemainingDisplayText;
    public Text MetDisplayText;

    public Button VerbButton;
    public Button NounButton;
    public Button DeltaVSetButton;

    public InputField VerbInputField;
    public InputField NounInputField;
    public InputField DeltaVSetInputField;

    private static string PrefixString(string s, char chr, uint cnt)
    {
        bool negMode = s[0] == '-';

        while (s.Length < cnt)
        {
            if (negMode)
            {
                s = ' ' + s;
            }
            else
            {
                s = chr + s;
            }
        }

        return s;
    }

    public void UpdateMetText(uint t)
    {
        var a = TimeSpan.FromSeconds(t);
        var hr = Mathf.FloorToInt((float) a.TotalHours).ToString();
        var min = a.Minutes.ToString();
        var sec = a.Seconds.ToString();

        hr = PrefixString(hr, '0', 3);
        min = PrefixString(min, '0', 2);
        sec = PrefixString(sec, '0', 2);

        MetDisplayText.text = hr + " : " + min + " : " + sec;
    }

    public void UpdateDisplay(byte prog, byte verb, byte noun, float r1, float r2, float r3, float deltaV)
    {
        ProgDisplayText.text = PrefixString(prog.ToString(), '0', 2);
        VerbDisplayText.text = PrefixString(verb.ToString(), '0', 2);
        NounDisplayText.text = PrefixString(noun.ToString(), '0', 2);
        R1DisplayText.text = !float.IsNaN(r1) ? PrefixString(r1.ToString("N1").Replace(",", ""), '0', 6) : "";
        R2DisplayText.text = !float.IsNaN(r2) ? PrefixString(r2.ToString("N1").Replace(",", ""), '0', 6) : "";
        R3DisplayText.text = !float.IsNaN(r3) ? PrefixString(r3.ToString("N1").Replace(",", ""), '0', 6) : "";
        DeltaVRemainingDisplayText.text = PrefixString(deltaV.ToString("N1").Replace(",", ""), '0', 6);
    }

    public Tuple<byte, byte, float> GetFields()
    {
        byte verb;
        byte noun;
        float deltaV;

        try
        {
            verb = byte.Parse(VerbInputField.text);
        }
        catch (FormatException)
        {
            verb = 0;
        }

        try
        {
            noun = byte.Parse(NounInputField.text);
        }
        catch (FormatException)
        {
            noun = 0;
        }

        try
        {
            deltaV = float.Parse(DeltaVSetInputField.text);
        }
        catch (FormatException)
        {
            deltaV = 0;
        }

        return new Tuple<byte, byte, float>(verb, noun, deltaV);
    }
}
