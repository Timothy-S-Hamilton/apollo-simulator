from __future__ import division

class Widget(object):
    def __init__(self, x, y, color):
        super(Widget, self).__init__()
        self.start_pos = (x, y)
        self.x = x
        self.y = y
        self.color = color
        self.text = ""
        self.width = 0
        self.height = 0
        self.visible = True

    def update(self):
        pass

    def render(self):
        pass
