import time
import struct
import networking

DEBUG = True
protocol = networking.ServerNetworkProtocol('127.0.0.1', 10000)

astronaut_id = None
fido_id = None
boost_id = None
retro_id = None
guidance_id = None
gnc_id = None

fido_requested_abort = 0

led_states = [False] * 2
button_states = [False] * 9
verb = 0
noun = 0
deltaV = 0.0
def set_position(o_id, pos, orient):
    protocol.transmit_command(astronaut_id, 1, [
        o_id,
        pos[0], pos[1], pos[2], 
        orient[0][0], orient[0][1], orient[0][2], 
        orient[1][0], orient[1][1], orient[1][2], 
        orient[2][0], orient[2][1], orient[2][2]
    ])

def set_leds(abort, engine):
    protocol.transmit_command(astronaut_id, 2, [(int(abort) << 1) | int(engine)])

def get_leds():
    protocol.transmit_command(astronaut_id, 3)

def get_buttons():
    protocol.transmit_command(astronaut_id, 4)

def set_time(t):
    protocol.transmit_command(astronaut_id, 0, [int(t)])

def set_agc_values(prog, verb, noun, r1, r2, r3, dvr):
    protocol.transmit_command(astronaut_id, 5, [prog, verb, noun, r1, r2, r3, dvr])

def get_verb_field():
    protocol.transmit_command(astronaut_id, 6)

def get_noun_field():
    protocol.transmit_command(astronaut_id, 7)

def get_deltaV_field():
    protocol.transmit_command(astronaut_id, 8)


def led_state_update_cb(network_id, data):
    led_states[0] = bool(data[0] & 0x02)
    led_states[1] = bool(data[0] & 0x01)
    print 'led_state_update_cb:', led_states

def button_state_update_cb(network_id, data):
    for i in xrange(9):
        button_states[i] = bool(data[0] & (1 << i))
    print 'button_state_update_cb:', button_states

def apc_state_verb_update_cb(network_id, data):
    global verb
    print 'apc_state_verb_update_cb:', data
    verb = data[0]

def apc_state_noun_update_cb(network_id, data):
    global noun
    print 'apc_state_noun_update_cb:', data
    noun = data[0]

def apc_state_deltaV_update_cb(network_id, data):
    global deltaV
    print 'apc_state_deltaV_update_cb:', data
    deltaV = data[0]


def fido_button_pressed(network_id, data):
    global fido_requested_abort
    if DEBUG: print 'fido_button_pressed:', network_id, data
    fido_requested_abort = 1
    protocol.transmit_command(network_id, 12, [fido_requested_abort])

def set_client_type(network_id, data):
    global astronaut_id, fido_id, boost_id, retro_id, guidance_id, gnc_id
    client_type = data[0]
    if DEBUG: print "set_client_type:", network_id, data
    if client_type == 0:
        astronaut_id = network_id
        print 'Astronaut connected!'

        protocol.define_transmit_command(astronaut_id, 1, "<Bffffffffffff") # set object transform
        protocol.define_transmit_command(astronaut_id, 2, "<B") # set led states
        protocol.define_transmit_command(astronaut_id, 3, "") # request led states
        protocol.define_transmit_command(astronaut_id, 4, "") # request button states
        protocol.define_transmit_command(astronaut_id, 5, "<BBBffff") # set agc info
        protocol.define_transmit_command(astronaut_id, 6, "") # request verb
        protocol.define_transmit_command(astronaut_id, 7, "") # request noun
        protocol.define_transmit_command(astronaut_id, 8, "") # request deltaV

        protocol.define_receive_command(astronaut_id, 2, '<B', led_state_update_cb)
        protocol.define_receive_command(astronaut_id, 3, '<H', button_state_update_cb)
        protocol.define_receive_command(astronaut_id, 4, '<B', apc_state_verb_update_cb)
        protocol.define_receive_command(astronaut_id, 5, '<B', apc_state_noun_update_cb)
        protocol.define_receive_command(astronaut_id, 6, '<f', apc_state_deltaV_update_cb)

    elif client_type == 1:
        fido_id = network_id

        print 'FiDO connected!'

        protocol.define_receive_command(fido_id, 0, "", fido_button_pressed)
        protocol.define_transmit_command(fido_id, 1, "<fff")
        protocol.define_transmit_command(fido_id, 2, "<fff")
        protocol.define_transmit_command(fido_id, 3, "<f")
        protocol.define_transmit_command(fido_id, 4, "<f")
        protocol.define_transmit_command(fido_id, 5, "<f")
        protocol.define_transmit_command(fido_id, 6, "<f")
        protocol.define_transmit_command(fido_id, 7, "<f")
        protocol.define_transmit_command(fido_id, 8, "<f")
        protocol.define_transmit_command(fido_id, 9, "<f")
        protocol.define_transmit_command(fido_id, 10, "<f")
        protocol.define_transmit_command(fido_id, 11, "<B")
        protocol.define_transmit_command(fido_id, 12, "<B")
        protocol.define_transmit_command(fido_id, 13, "", True)
        protocol.define_transmit_command(fido_id, 14, "")
        protocol.define_transmit_command(fido_id, 15, "<B")
    elif client_type == 2:
        boost_id = network_id
    elif client_type == 3:
        retro_id = network_id
    elif client_type == 4:
        guidance_id = network_id
    elif client_type == 5:
        gnc_id = network_id
    else:
        print "Warning, Unknown Client Type:", client_type
        return
    
    protocol.define_transmit_command(network_id, 0, "<I") # Send MET

def new_connection_callback(network_id):
    if DEBUG: print 'new_connection_callback:', network_id
    protocol.define_transmit_command(network_id, 14, "")
    protocol.define_receive_command(network_id, 1, "<B", set_client_type)
    protocol.transmit_command(network_id, 14)

protocol.new_connection_callback = new_connection_callback

try:
    # Wait for at least 1 connection
    # while len(protocol.server.get_client_ids()) == 0:
    #     protocol.update()
    #     time.sleep(0.25)
    
    mission_start_time = time.time()

    dummy_matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    dummy_position1 = [10, 11, 12]
    dummy_position2 = [1, 2, 3]
    dummy_position3 = [-2, 4, 5]
    dummy_position4 = [5, -2, -4]

    sm = 0
    count = 0
    def fourth_timer_callback():
        global sm, count
        sm += 1
        count += 1
        if sm > 3:
            sm = 0
        
        if astronaut_id is not None and count % 12 == 0:
            get_verb_field()
            get_noun_field()
            get_deltaV_field()
        
        if astronaut_id is not None and sm == 0:
            set_leds(False, False)
            set_position(0, dummy_position1, dummy_matrix)
        elif astronaut_id is not None and sm == 1:
            set_leds(False, True)
            set_position(0, dummy_position2, dummy_matrix)
        elif astronaut_id is not None and sm == 2:
            set_leds(True, False)
            set_position(0, dummy_position3, dummy_matrix)
        elif astronaut_id is not None and sm == 3:
            set_leds(True, True)
            set_position(0, dummy_position4, dummy_matrix)
            set_agc_values(99, verb, noun, 1234.5, 123.55, 0.4123, deltaV)

    fourth_timer = networking.Timer(1)
    fourth_timer.add_callback(fourth_timer_callback)
    fourth_timer.start()

    def met_timer_cb():
        met = int(time.time() - mission_start_time)
        if DEBUG: print 'met_timer_cb:', met

        if astronaut_id is not None: protocol.transmit_command(astronaut_id, 0, [met])
        if fido_id is not None: protocol.transmit_command(fido_id, 0, [met])
        if boost_id is not None: protocol.transmit_command(boost_id, 0, [met])
        if retro_id is not None: protocol.transmit_command(retro_id, 0, [met])
        if guidance_id is not None: protocol.transmit_command(guidance_id, 0, [met])
        if gnc_id is not None: protocol.transmit_command(gnc_id, 0, [met])

    met_timer = networking.Timer(0.5)
    met_timer.add_callback(met_timer_cb)
    met_timer.start()

    ping = True
    def fido_test_timer_cb():
        global ping
        if fido_id is None: return
        protocol.transmit_command(fido_id, 1, [1.543654, 2.3, 3.1])
        protocol.transmit_command(fido_id, 2, [4.5, 22222.5, 0.321423])
        protocol.transmit_command(fido_id, 3, [10000.1])
        protocol.transmit_command(fido_id, 4, [13412.2341])
        protocol.transmit_command(fido_id, 5, [125.213223])
        protocol.transmit_command(fido_id, 6, [234145.4567])
        protocol.transmit_command(fido_id, 7, [-245.3245])
        protocol.transmit_command(fido_id, 8, [100.5])
        protocol.transmit_command(fido_id, 9, [9000.25])
        protocol.transmit_command(fido_id, 10, [0.12344534])
        protocol.transmit_command(fido_id, 11, [1])
        protocol.transmit_command(fido_id, 12, [0])
        ping = not ping
        if ping:
            handle = open("officer_test_send.pdf", "rb")
            handle.seek(0, 2)
            file_size = handle.tell()
            handle.seek(0, 0)
            pdf_data = handle.read(file_size)
            handle.close()
            protocol.transmit_command(fido_id, 15, [1])
            protocol.transmit_command(fido_id, 13, pdf_data)

    fido_test_timer = networking.Timer(5)
    fido_test_timer.add_callback(fido_test_timer_cb)
    fido_test_timer.start()

    while True:
        fourth_timer.update()
        met_timer.update()

        fido_test_timer.update()

        protocol.update()

        # Run Update Loop at 60 Hz
        time.sleep(1.0 / 60.0)

except KeyboardInterrupt:
    pass

protocol.close()
