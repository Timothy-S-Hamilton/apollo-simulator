from __future__ import division
import pygui
from pygui.gui.widget import Widget
from pygui.gui.label import Label


class Button(Widget):
    def __init__(self, x, y, width, height, text, action=None):
        super(Button, self).__init__(x, y, pygui.gray)
        pygui.add_mouse_listener(self)
        self.width = width
        self.height = height
        self.rect = (x, y, width, height)
        self.label = Label(x + width / 2, y + height / 2, text)

        self.label.font_size = 1
        (text_w, text_h) = pygui.get_text_dimensions(self.label.font, self.label.font_size, self.label.text)
        while text_w < self.rect[2] and text_h < self.rect[3]:
            self.label.font_size += 1
            (text_w, text_h) = pygui.get_text_dimensions(self.label.font, self.label.font_size, self.label.text)
        while text_w > self.rect[2] or text_h > self.rect[3]:
            self.label.font_size -= 1
            (text_w, text_h) = pygui.get_text_dimensions(self.label.font, self.label.font_size, self.label.text)

        self.text = text
        self.hover_text = text
        self.pressed_text = text
        self.label.color = pygui.white
        self.normal_color = self.color
        self.outline_color = pygui.silver
        self.pressed_color = pygui.blue
        self.hover_color = pygui.green
        self.down = False
        self.action = action

    def set_colors(self, outline_color=pygui.gray, hover_color=pygui.green, pressed_color=pygui.blue,
                   body_color=pygui.gray, label_color=pygui.white):
        self.pressed_color = pressed_color
        self.outline_color = outline_color
        self.hover_color = hover_color
        self.normal_color = body_color
        self.label.color = label_color

    def update(self):
        if self.color != self.pressed_color:
            if pygui.contains(self.rect, pygui.get_mouse_position()):
                self.color = self.hover_color
                self.label.text = self.hover_text
            else:
                self.color = self.normal_color
                self.label.text = self.text
                self.down = False
        else:
            if not pygui.contains(self.rect, pygui.get_mouse_position()):
                self.color = self.normal_color

        self.rect = (self.x, self.y, self.width, self.height)
        self.label.x = self.x + self.width / 2
        self.label.y = self.y + self.height / 2

    def render(self):
        if self.visible:
            pygui.draw_rect(self.rect, self.color)
            pygui.draw_rect(self.rect, self.outline_color, 2)
            self.label.render()

    def event(self, e_type, e_value):
        if e_type == "MouseDown" and e_value == 1:
            if pygui.contains(self.rect, pygui.get_mouse_position()):
                self.color = self.pressed_color
                self.label.text = self.pressed_text
                self.down = True
        if e_type == "MouseUp" and e_value == 1:
            if pygui.contains(self.rect, pygui.get_mouse_position()):
                if self.down:
                    self.down = False
                    self.color = self.normal_color
                    self.action()
