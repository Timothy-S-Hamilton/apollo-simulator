import sys
import time
import struct
import pygame
import pygui
from networking import *
from datetime import timedelta
from pygui.gui.button import Button
from pygui.gui.label import Label
from pdf2image import convert_from_path

DEBUG = False

CLIENT_TYPE = 1

met = 0
position = [0.0, 0.0, 0.0]
velocity = [0.0, 0.0, 0.0]
distance_to_earth_center = 0.0
altitude_above_earth = 0.0
distance_to_moon_center = 0.0
altitude_above_moon = 0.0
speed = 0.0
spacecraft_mass = 0.0
engine_max_thrust = 0.0
current_engine_thrust = 0.0
sphere_of_influence = 0
abort_request_flag = 0
selected_pdf = 0

network_location = raw_input("Server IP & Port: ") if len(sys.argv) < 2 else sys.argv[1]

ip = network_location.split(":")[0]
port = int(network_location.split(":")[1])

print "Connecting to:", ip + ":" + str(port)

protocol = ClientNetworkProtocol(ip, port)

def set_met(data):
    global met
    if DEBUG: print "set_met:", data
    met = data[0]

def set_position(data):
    global position
    if DEBUG: print "set_position:", data
    position[0] = data[0]
    position[1] = data[1]
    position[2] = data[2]

def set_velocity(data):
    global velocity
    if DEBUG: print "set_velocity:", data
    velocity[0] = data[0]
    velocity[1] = data[1]
    velocity[2] = data[2]

def set_distance_to_earth_center(data):
    global distance_to_earth_center
    if DEBUG: print "set_distance_to_earth_center:", data
    distance_to_earth_center = data[0]

def set_altitude_above_earth(data):
    global altitude_above_earth
    if DEBUG: print "set_altitude_above_earth:", data
    altitude_above_earth = data[0]

def set_distance_to_moon_center(data):
    global distance_to_moon_center
    if DEBUG: print "set_distance_to_moon_center:", data
    distance_to_moon_center = data[0]

def set_altitude_above_moon(data):
    global altitude_above_moon
    if DEBUG: print "set_altitude_above_moon:", data
    altitude_above_moon = data[0]

def set_speed(data):
    global speed
    if DEBUG: print "set_speed:", data
    speed = data[0]

def set_spacecraft_mass(data):
    global spacecraft_mass
    if DEBUG: print "set_spacecraft_mass:", data
    spacecraft_mass = data[0]

def set_engine_max_thrust(data):
    global engine_max_thrust
    if DEBUG: print "set_engine_max_thrust:", data
    engine_max_thrust = data[0]

def set_current_engine_thrust(data):
    global current_engine_thrust
    if DEBUG: print "set_current_engine_thrust:", data
    current_engine_thrust = data[0]

def set_sphere_of_influence(data):
    global sphere_of_influence
    if DEBUG: print "set_sphere_of_influence:", data
    sphere_of_influence = data[0]

def set_abort_request_flag(data):
    global abort_request_flag
    if DEBUG: print "set_abort_request_flag:", data
    abort_request_flag = data[0]

DIRECT_MODE = True

def set_pdf_chart(data):
    if DEBUG: print "set_pdf_chart:", data

    if DIRECT_MODE:
        if selected_pdf == 1:
            fname = "orbit_earth_moon.png"
        elif selected_pdf == 2:
            fname = "orbit_earth.png"
        elif selected_pdf == 3:
            fname = "altitude.png"
        elif selected_pdf == 4:
            fname = "velocity.png"
        elif selected_pdf == 5:
            fname = "orbit_moon.png"
        else:
            print 'Error, Unknown PNG Index:', selected_pdf
            return

        handle = open(fname, "wb")
        handle.write(data)
        handle.flush()
        handle.close()

        scale = 1.15

        if fname == "orbit_earth_moon.png":
            pygui.load_image("earth_moon_orbit", "orbit_earth_moon.png")
            pygui.scale_image("earth_moon_orbit", "earth_moon_orbit", scale, scale)
        elif fname == "orbit_earth.png":
            pygui.load_image("orbit_earth", "orbit_earth.png")
            pygui.scale_image("orbit_earth", "orbit_earth", scale, scale)
        elif fname == "altitude.png":
            pygui.load_image("altitude", "altitude.png")
            pygui.scale_image("altitude", "altitude", scale, scale)
        elif fname == "velocity.png":
            pygui.load_image("velocity", "velocity.png")
            pygui.scale_image("velocity", "velocity", scale, scale) 
        elif fname == "orbit_moon.png":
            pygui.load_image("orbit_moon", "orbit_moon.png")
            pygui.scale_image("orbit_moon", "orbit_moon", scale, scale)
    else:
        if selected_pdf == 1:
            fname = "orbit_earth_moon.pdf"
        elif selected_pdf == 2:
            fname = "orbit_earth.pdf"
        elif selected_pdf == 3:
            fname = "altitude.pdf"
        elif selected_pdf == 4:
            fname = "velocity.pdf"
        elif selected_pdf == 5:
            fname = "orbit_moon.pdf"
        else:
            print 'Error, Unknown PDF Index:', selected_pdf
            return

        handle = open(fname, "wb")
        handle.write(data)
        handle.close()

        scale = 0.60

        if fname == "orbit_earth_moon.pdf":
            earth_moon_orbit_pages = convert_from_path('orbit_earth_moon.pdf', 200)
            earth_moon_orbit_pages[0].save('out1.png', 'PNG')
            pygui.load_image("earth_moon_orbit", "out1.png")
            pygui.scale_image("earth_moon_orbit", "earth_moon_orbit", scale, scale)
        elif fname == "orbit_earth.pdf":
            earth_orbit_pages = convert_from_path('orbit_earth.pdf', 200)
            earth_orbit_pages[0].save('out2.png', 'PNG')
            pygui.load_image("earth_orbit", "out2.png")
            pygui.scale_image("earth_orbit", "earth_orbit", scale, scale)
        elif fname == "altitude.pdf":
            altitude_pages = convert_from_path('altitude.pdf', 200)
            altitude_pages[0].save('out3.png', 'PNG')
            pygui.load_image("altitude", "out3.png")
            pygui.scale_image("altitude", "altitude", scale, scale)
        elif fname == "velocity.pdf":
            velocity_pages = convert_from_path('velocity.pdf', 200)     
            velocity_pages[0].save('out4.png', 'PNG')
            pygui.load_image("velocity", "out4.png")
            pygui.scale_image("velocity", "velocity", scale, scale) 
        elif fname == "orbit_moon.pdf":
            orbit_moon_pages = convert_from_path('orbit_moon.pdf', 200)     
            orbit_moon_pages[0].save('out5.png', 'PNG')
            pygui.load_image("orbit_moon", "out5.png")
            pygui.scale_image("orbit_moon", "orbit_moon", scale, scale)

def client_type_request(data):
    if DEBUG: print "client_type_request:", data
    protocol.transmit_command(1, [CLIENT_TYPE])

def set_pdf_index(data):
    global selected_pdf
    if DEBUG: print "set_pdf_chart:", data
    selected_pdf = data[0]

protocol.define_receive_command(0, "<I", set_met)
protocol.define_receive_command(1, "<fff", set_position)
protocol.define_receive_command(2, "<fff", set_velocity)
protocol.define_receive_command(3, "<f", set_distance_to_earth_center)
protocol.define_receive_command(4, "<f", set_altitude_above_earth)
protocol.define_receive_command(5, "<f", set_distance_to_moon_center)
protocol.define_receive_command(6, "<f", set_altitude_above_moon)
protocol.define_receive_command(7, "<f", set_speed)
protocol.define_receive_command(8, "<f", set_spacecraft_mass)
protocol.define_receive_command(9, "<f", set_engine_max_thrust)
protocol.define_receive_command(10, "<f", set_current_engine_thrust)
protocol.define_receive_command(11, "<B", set_sphere_of_influence)
protocol.define_receive_command(12, "<B", set_abort_request_flag)
protocol.define_receive_command(13, "", set_pdf_chart, True)
protocol.define_receive_command(14, "", client_type_request)
protocol.define_receive_command(15, "<B", set_pdf_index)

protocol.define_transmit_command(0, "") # Send Button Pressed Event
protocol.define_transmit_command(1, "<B") # Send Client Type

pygui.init("Flight Dynamics Officer (FIDO)", (1280, 720))

running = True
clock = pygame.time.Clock()

active_image = 0
buttons = []
labels = []

def make_label(x, y, text, l_font_size=32):
    l = Label(x, y, text, font_size=l_font_size)
    l.halign = 'left'
    l.valign = 'top'
    labels.append(l)
    return l

pygui.create_image("earth_moon_orbit", 10, 10)
pygui.create_image("orbit_earth", 10, 10)
pygui.create_image("altitude", 10, 10)
pygui.create_image("velocity", 10, 10)
pygui.create_image("moon_orbit", 10, 10)

def button_callback():
    protocol.transmit_command(0)

abort_indicator_location = (1100, 10, 80, 30)
abort_button = Button(abort_indicator_location[0] + abort_indicator_location[2] + 10, 10, 80, 30, "Abort", button_callback)
buttons.append(abort_button)

def earth_orbit_callback():
    global active_image
    active_image = 0

def earth_moon_orbit_callback():
    global active_image
    active_image = 1

def altitude_callback():
    global active_image
    active_image = 2

def velocity_callback():
    global active_image
    active_image = 3

def moon_orbit_callback():
    global active_image
    active_image = 4

button_y = 120
earth_orbit_button = Button(760, button_y, 80, 30, "Earth Orbit", earth_orbit_callback)
buttons.append(earth_orbit_button)

earth_moon_orbit_button = Button(earth_orbit_button.x + earth_orbit_button.width + 10, button_y, 150, 30, "Earth-Moon Orbit", earth_moon_orbit_callback)
buttons.append(earth_moon_orbit_button)

moon_orbit_button = Button(earth_moon_orbit_button.x + earth_moon_orbit_button.width + 10, button_y, 80, 30, "Moon Orbit", moon_orbit_callback)
buttons.append(moon_orbit_button)

altitude_button = Button(moon_orbit_button.x + moon_orbit_button.width + 10, button_y, 80, 30, "Altitude", altitude_callback)
buttons.append(altitude_button)

velocity_button = Button(altitude_button.x + altitude_button.width + 10, button_y, 80, 30, "Velocity", velocity_callback)
buttons.append(velocity_button)

x_start = 5
spacing = 5
met_label = make_label(x_start, 5, "MET: 000:00:00")
distance_from_earth_label = make_label(x_start, met_label.y + met_label.height + spacing, "Distance to Earth Center (m): 0")
distance_from_moon_label = make_label(x_start, distance_from_earth_label.y + distance_from_earth_label.height + spacing, "Distance to Moon Center (m): 0")
x_label = make_label(x_start, distance_from_moon_label.y + distance_from_moon_label.height + spacing, "X (m): 0")
y_label = make_label(x_start, x_label.y + x_label.height + spacing, "Y (m): 0")
z_label = make_label(x_start, y_label.y + y_label.height + spacing, "Z (m): 0")
speed_label = make_label(x_start, z_label.y + z_label.height + spacing, "Speed (m/s): 0")
vx_label = make_label(x_start, speed_label.y + speed_label.height + spacing, "Vx (m/s): 0")
vy_label = make_label(x_start, vx_label.y + vx_label.height + spacing, "Vy (m/s): 0")
vz_label = make_label(x_start, vy_label.y + vy_label.height + spacing, "Vz (m/s): 0")
altitude_above_earth_label = make_label(x_start, vz_label.y + vz_label.height + spacing, "Altitude Above Earth (km): 0")
altitude_above_moon_label = make_label(x_start, altitude_above_earth_label.y + altitude_above_earth_label.height + spacing, "Altitude Above Moon (km): 0")
current_engine_thrust_label = make_label(x_start, altitude_above_moon_label.y + altitude_above_moon_label.height + spacing, "Current Engine Thrust (N): 0")
mass_label = make_label(x_start, current_engine_thrust_label.y + current_engine_thrust_label.height + spacing, "Spacecraft Mass (kg): 0")
thrust_label = make_label(x_start, mass_label.y + mass_label.height + spacing, "Max Thrust (N): 0")
sphere_of_influence_label = make_label(x_start, thrust_label.y + thrust_label.height + spacing, "Sphere of Influence: Earth")

def prefix(s, ch, cnt):
    while len(s) < cnt:
        s = ch + s
    return s

done = False
while not done:
    done = pygui.update()
    
    for b in buttons: b.update()

    protocol.update()

    # ------ Update MET Text ------
    td = timedelta(seconds=met)
    s = td.total_seconds()
    hours, remainder = divmod(s, 3600)
    minutes = remainder // 60
    seconds = round(remainder - (minutes * 60))
    hours_str = prefix(str(int(hours)), '0', 3)
    minutes_str = prefix(str(int(minutes)), '0', 2)
    seconds_str = prefix(str(int(seconds)), '0', 2)
    met_label.text = "MET: " + hours_str + ":" + minutes_str + ":" + seconds_str
    # ------ Update MET Text ------

    x_label.text = "X (m): " + str(round(position[0], 5))
    y_label.text = "Y (m): " + str(round(position[1], 5))
    z_label.text = "Z (m): " + str(round(position[2], 5))
    vx_label.text = "Vx (m/s): " + str(round(velocity[0], 5))
    vy_label.text = "Vy (m/s): " + str(round(velocity[1], 5))
    vz_label.text = "Vz (m/s): " + str(round(velocity[2], 5))
    speed_label.text = "Speed (km/s): " + str(round(speed, 5))
    distance_from_earth_label.text = "Distance to Earth Center (m): " + str(round(distance_to_earth_center, 5))
    distance_from_moon_label.text = "Distance to Moon Center (m): " + str(round(distance_to_moon_center, 5))
    altitude_above_earth_label.text = "Altitude Above Earth (km): " + str(round(altitude_above_earth, 5))
    altitude_above_moon_label.text = "Altitude Above Moon (km): " + str(round(altitude_above_moon, 5))
    thrust_label.text = "Max Thrust (N): " + str(round(engine_max_thrust, 5))
    current_engine_thrust_label.text = "Current Engine Thrust (N): " + str(round(current_engine_thrust, 5))
    mass_label.text = "Spacecraft Mass (kg): " + str(round(spacecraft_mass, 5))
    sphere_of_influence_label.text = "Sphere of Influence: " + ("Earth" if sphere_of_influence == 0 else "Moon")

    pygui.clear_screen()

    for l in labels: l.render()
    for b in buttons: b.render()

    name = ""
    if active_image == 0:
        name = "orbit_earth"
    elif active_image == 1:
        name = "earth_moon_orbit"
    elif active_image == 2:
        name = "altitude"
    elif active_image == 3:
        name = "velocity"
    elif active_image == 4:
        name = "moon_orbit"

    if abort_request_flag:
        pygui.draw_rect(abort_indicator_location, (255, 0, 0))
    else:
        pygui.draw_rect(abort_indicator_location, (128, 128, 128))

    pygui.draw_image(name, 1280, 720, 'right', 'bottom')

    pygui.display_flip()

protocol.close()
pygui.quit()
