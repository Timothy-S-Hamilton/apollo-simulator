﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using DigitalRuby.Earth;
using UnityEngine;
using UnityEngine.UI;

// GUIDO Station TODO
// TODO: MET (hhh:mm:ss)
// TODO: Orbital Eccentricity
// TODO: Altitude at periapsis (km)
// TODO: Altitude at apoapsis (km)
// TODO: SPHERE OF INFLUENCE (0 Earth, 1 Moon)
// TODO: ATTITUDE X (1 PROGRADE, -1 RETROGRADE)
// TODO: ATTITUDE Z (1 HEAD-UP, -1 HEAD-DOWN)
// TODO: ATT HOLD (0 FALSE, 1 TRUE)
// TODO: Implement Transceiver with Main Simulation

// GUIDANCE, NAVIGATION, AND CONTROL (GNC) TODO
// TODO: MET (hhh:mm:ss)
// TODO: ABORT REQUEST (0 FALSE, 1 TRUE)
// TODO: ABORT (0 FALSE, 1 TRUE)
// TODO: ENGINE (0 OFF, 1 ON)
// TODO: S-IVB SEP (0 FALSE, 1 TRUE)
// TODO: SM SEP (0 FALSE, 1 TRUE)
// TODO: DROGUE CHUTES (0 STOWED, 1 DEPLOYED)
// TODO: MAIN CHUTES (0 STOWED, 1 DEPLOYED)
// TODO: ATTITUDE X (1 PROGRADE, -1 RETROGRADE)
// TODO: ATTITUDE Z (1 HEAD-UP, -1 HEAD-DOWN)
// TODO: ATT HOLD (0 FALSE, 1 TRUE)
// TODO: Implement Transceiver with Main Simulation

// Booster Station TODO
// TODO: MET (hhh:mm:ss)
// TODO: Spacecraft mass (kg)
// TODO: Current engine thrust (N)
// TODO: S-IC ENGINE 1 (0 ON, 1 OFF)
// TODO: S-IC ENGINE 2 (0 ON, 1 OFF)
// TODO: S-IC ENGINE 3 (0 ON, 1 OFF)
// TODO: S-IC ENGINE 4 (0 ON, 1 OFF)
// TODO: S-IC ENGINE 5 (0 ON, 1 OFF)
// TODO: S-II ENGINE 1 (0 ON, 1 OFF)
// TODO: S-II ENGINE 2 (0 ON, 1 OFF)
// TODO: S-II ENGINE 3 (0 ON, 1 OFF)
// TODO: S-II ENGINE 4 (0 ON, 1 OFF)
// TODO: S-II ENGINE 5 (0 ON, 1 OFF)
// TODO: S-II SEP (0 FALSE, 1 TRUE)
// TODO: S-IVB ENGINE (0 ON, 1 OFF)
// TODO: S-IVB SEP (0 FALSE, 1 TRUE)
// TODO: Implement Transceiver with Main Simulation

// RETRO Station TODO
// TODO: MET (hhh:mm:ss)
// TODO: Altitude above the Earth (km)
// TODO: Spacecraft mass (kg)
// TODO: Speed (m/s)
// TODO: Spacecraft mass (kg)
// TODO: Current engine thrust (N)
// TODO: Which sphere of influence? (0=Earth, 1=Moon)
// TODO: Orbital eccentricity
// TODO: Altitude at periapsis (km)
// TODO: Altitude at apoapsis (km)
// TODO: ABORT REQUEST (0 FALSE, 1 TRUE)
// TODO: ABORT (0 FALSE, 1 TRUE)
// TODO: ENGINE (0 OFF, 1 ON)
// TODO: SM SEP (0 FALSE, 1 TRUE)
// TODO: DROGUE CHUTES (0 STOWED, 1 DEPLOYED)
// TODO: MAIN CHUTES (0 STOWED, 1 DEPLOYED)
// TODO: ATTITUDE X (1 PROGRADE, -1 RETROGRADE)
// TODO: ATTITUDE Z (1 HEAD-UP, -1 HEAD-DOWN)
// TODO: Implement Transceiver with Main Simulation

public class NetworkManager : MonoBehaviour
{
    public InputField NetworkInputField;
    public Text ErrorText;
    public Button ConnectButton;

    private const float Scale = 1000.0f;
    private bool _ready = false;
    private bool _stopTimeout = false;
    private bool _receiving = false;
    private readonly Queue<byte> _incomingBuffer = new Queue<byte>();    
    private ObjectManager _objectManager;
    private HardwareManager _hardwareManager;
    private ApolloGuidanceComputer _apolloGuidanceComputer;
    private Socket _socket;
    private IPEndPoint _remoteEp;

    internal class ReceiveStateObject
    {
        // Client socket.  
        public Socket WorkSocket;
        
        // Size of receive buffer.  
        public int BufferSize;
        
        // Receive buffer.  
        public byte[] Buffer;
        
        // Amount of data that has been successfully received.
        public int Received;

        public ReceiveStateObject(int bufferSize)
        {
            BufferSize = bufferSize;
            Received = 0;
            Buffer = new byte[BufferSize];
        }
    }

    internal class TransmitStateObject
    {
        // Client socket.
        public Socket WorkSocket;
        
        // Data being transmitted.
        public byte[] Data;

        // Amount of data that has been successfully sent.
        public int Sent;

        public TransmitStateObject(byte[] data)
        {
            Sent = 0;
            Data = data;
        }
    }

    public void ConnectButtonClicked()
    {
        NetworkInputField.interactable = false;
        ConnectButton.interactable = false;
        NetworkInputField.gameObject.SetActive(true);
        if (!_ready)
        {
            ErrorText.gameObject.SetActive(true);
            ErrorText.text = "Connecting...";
            ErrorText.color = Color.yellow;
            Connect();
        }
        else
        {
            Disconnect();
        }
    }

    public void VerbButtonClicked()
    {
        TransmitAgcVerb();
        _apolloGuidanceComputer.VerbInputField.text = "";
    }

    public void NounButtonClicked()
    {
        TransmitAgcNoun();
        _apolloGuidanceComputer.NounInputField.text = "";
    }

    public void DeltaVButtonClicked()
    {
        TransmitAgcDeltaV();
        _apolloGuidanceComputer.DeltaVSetInputField.text = "";
    }

    IEnumerator ConnectTimeout()
    {
        // Connect to the remote endpoint. 
        _socket.BeginConnect(_remoteEp, ConnectCallback, _socket);
        yield return new WaitForSeconds(3);
        _socket.Dispose();
        _socket = null;
        ErrorText.text = "Failed!";
        ErrorText.color = Color.red;
        NetworkInputField.interactable = true;
        ConnectButton.interactable = true;
    }

    IEnumerator ConnectedMessage()
    {
        ErrorText.text = "Connected!";
        ErrorText.color = Color.green;
        yield return new WaitForSeconds(3);
        ConnectButton.GetComponentInChildren<Text>().text = "Disconnect";
        ConnectButton.interactable = true;
        NetworkInputField.gameObject.SetActive(false);
        ErrorText.gameObject.SetActive(false);
    }

    private void Connect()
    {
        try
        {
            // Establish the remote endpoint for the socket.
            IPAddress ipAddress;
            int port;
            string[] parts;
            try
            {
                parts = NetworkInputField.text.Split(':');
                ipAddress = IPAddress.Parse(parts[0]);
                port = int.Parse(parts[1]);
            }
            catch (Exception)
            {
                ErrorText.text = "Invalid!";
                ErrorText.color = Color.red;
                NetworkInputField.interactable = true;
                ConnectButton.interactable = true;
                return;
            }

            _remoteEp = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.
            _socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp)
            {
                ReceiveTimeout = 100,
                SendTimeout = 100
            };

            StartCoroutine("ConnectTimeout");
        }
        catch (Exception)
        {
            ErrorText.text = "Failed!";
            ErrorText.gameObject.SetActive(true);
            NetworkInputField.interactable = true;
            ConnectButton.interactable = true;
        }
    }

    private void Disconnect()
    {
        _ready = false;

        // TODO: there is a bug here, where execution gets here and should not. Happens when server restarts, and user clicks connect again.

        // Release the socket.
        _socket.Shutdown(SocketShutdown.Both);
        _socket.Close();
        _socket = null;

        ConnectButton.GetComponentInChildren<Text>().text = "Connect";
        NetworkInputField.interactable = true;
        ConnectButton.interactable = true;

        _apolloGuidanceComputer.NounButton.interactable = false;
        _apolloGuidanceComputer.VerbButton.interactable = false;
        _apolloGuidanceComputer.DeltaVSetButton.interactable = false;
        _apolloGuidanceComputer.NounInputField.interactable = false;
        _apolloGuidanceComputer.VerbInputField.interactable = false;
        _apolloGuidanceComputer.DeltaVSetInputField.interactable = false;
    }

    private void ConnectCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.  
            Socket client = (Socket)ar.AsyncState;

            // Complete the connection.
            client.EndConnect(ar);

            _stopTimeout = true;
            _ready = true;
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private void Receive(Socket client, int amt)
    {
        try
        {
            // Create the state object.  
            ReceiveStateObject receiveState = new ReceiveStateObject(amt) { WorkSocket = client };

            // Begin receiving the data from the remote device.
            client.BeginReceive(receiveState.Buffer, 0, receiveState.BufferSize, 0, ReceiveCallback, receiveState);

            _receiving = true;
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void ReceiveCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the state object and the client socket from the asynchronous state object.  
            ReceiveStateObject receiveState = (ReceiveStateObject)ar.AsyncState;
            Socket client = receiveState.WorkSocket;

            if (!client.Connected)
            {
                _receiving = false;
                return;
            }

            // Read data from the remote device.  
            int bytesRead = client.EndReceive(ar);
            receiveState.Received += bytesRead;

            if (receiveState.Received != receiveState.BufferSize)
            {
                // Get the rest of the data.
                client.BeginReceive(receiveState.Buffer, receiveState.Received,
                    receiveState.BufferSize - receiveState.Received, 0, ReceiveCallback, receiveState);
                return;
            }
            
            lock (_incomingBuffer)
            {
                for (uint i = 0; i < receiveState.BufferSize; i++)
                {
                    _incomingBuffer.Enqueue(receiveState.Buffer[i]);
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        _receiving = false;
    }

    private void Send(Socket client, string data)
    {
        // Convert the string data to byte data using ASCII encoding.  
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        TransmitStateObject transmitStateObject = new TransmitStateObject(byteData) { WorkSocket = client };

        // Begin sending the data to the remote device.  
        client.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, transmitStateObject);
    }

    private void Send(Socket client, byte[] byteData)
    {
        TransmitStateObject transmitStateObject = new TransmitStateObject(byteData) { WorkSocket = client };

        // Begin sending the data to the remote device.  
        client.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, transmitStateObject);
    }

    private void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.  
            TransmitStateObject transmitStateObject = (TransmitStateObject)ar.AsyncState;

            // Complete sending the data to the remote device.
            int bytesSent = transmitStateObject.WorkSocket.EndSend(ar);
            transmitStateObject.Sent += bytesSent;
        
            if (transmitStateObject.Sent < transmitStateObject.Data.Length)
            {
                transmitStateObject.WorkSocket.BeginSend(transmitStateObject.Data,
                    transmitStateObject.Sent,
                    transmitStateObject.Data.Length - transmitStateObject.Sent,
                    SocketFlags.None,
                    SendCallback,
                    transmitStateObject);
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void Command0()
    {
        if (_incomingBuffer.Count >= 5)
        {
            byte[] rawBytes4 = new byte[5];
            for (uint i = 0; i < 5; i++)
            {
                rawBytes4[i] = _incomingBuffer.Dequeue();
            }

            uint metVal = BitConverter.ToUInt32(rawBytes4, 1);
            _apolloGuidanceComputer.UpdateMetText(metVal);
        }
    }

    private void Command1()
    {
        if (_incomingBuffer.Count >= 50)
        {
            byte[] rawBytes0 = new byte[50];
            for (uint i = 0; i < 50; i++)
            {
                rawBytes0[i] = _incomingBuffer.Dequeue();
            }

            byte objectKind = rawBytes0[1];

            float px = BitConverter.ToSingle(rawBytes0, 2 + (4 * 0));
            float py = BitConverter.ToSingle(rawBytes0, 2 + (4 * 1));
            float pz = BitConverter.ToSingle(rawBytes0, 2 + (4 * 2));

            float rM00 = BitConverter.ToSingle(rawBytes0, 2 + (4 * 3));
            float rM01 = BitConverter.ToSingle(rawBytes0, 2 + (4 * 4));
            float rM02 = BitConverter.ToSingle(rawBytes0, 2 + (4 * 5));

            float rM20 = BitConverter.ToSingle(rawBytes0, 2 + (4 * 9));
            float rM21 = BitConverter.ToSingle(rawBytes0, 2 + (4 * 10));
            float rM22 = BitConverter.ToSingle(rawBytes0, 2 + (4 * 11));

            Vector3 pos = new Vector3(px, py, pz);
            pos /= Scale;

            Vector3 forward = new Vector3(rM00, rM01, rM02);
            Vector3 down = new Vector3(rM20, rM21, rM22);

            if (objectKind == 0)
            {
                _objectManager.UpdateEarth(pos, forward, down);
            }
            else if (objectKind == 1)
            {
                _objectManager.UpdateMoon(pos, forward, down);
            }
            else if (objectKind == 2)
            {
                _objectManager.UpdateShip(pos, forward, down);
            }
            else
            {
                Debug.Log("Unknown Object Type: " + objectKind);
            }
        }
    }

    private void Command2()
    {
        if (_incomingBuffer.Count >= 2)
        {
            byte[] rawBytes1 = new byte[2];
            for (uint i = 0; i < 2; i++)
            {
                rawBytes1[i] = _incomingBuffer.Dequeue();
            }

            int ledState = rawBytes1[1];
            bool abortState = Convert.ToBoolean(ledState & 0x02);
            bool engineState = Convert.ToBoolean(ledState & 0x01);

            _hardwareManager.SetLedStates(abortState, engineState);
        }
    }

    private void Command3()
    {
        _incomingBuffer.Dequeue();

        var ledStates = _hardwareManager.GetLedStates();
        bool abortLed = ledStates.Item1;
        bool engineLed = ledStates.Item2;

        byte[] ledData = new byte[2];
        ledData[0] = 2;
        ledData[1] |= (byte)(abortLed ? 2 : 0);
        ledData[1] |= (byte)(engineLed ? 1 : 0);

        Send(_socket, ledData);
    }

    private void Command4()
    {
        _incomingBuffer.Dequeue();
        TransmitButtonStates();
    }

    private void Command5()
    {
        if (_incomingBuffer.Count >= 20)
        {
            byte[] rawBytes5 = new byte[20];
            for (uint i = 0; i < 20; i++)
            {
                rawBytes5[i] = _incomingBuffer.Dequeue();
            }

            byte prog = rawBytes5[1];
            byte verb = rawBytes5[2];
            byte noun = rawBytes5[3];

            float r1 = BitConverter.ToSingle(rawBytes5, 4 + (4 * 0));
            float r2 = BitConverter.ToSingle(rawBytes5, 4 + (4 * 1));
            float r3 = BitConverter.ToSingle(rawBytes5, 4 + (4 * 2));
            float deltaV = BitConverter.ToSingle(rawBytes5, 4 + (4 * 3));

            _apolloGuidanceComputer.UpdateDisplay(prog, verb, noun, r1, r2, r3, deltaV);
        }
    }

    private void Command6()
    {
        _incomingBuffer.Dequeue();
        TransmitAgcVerb();
    }

    private void Command7()
    {
        _incomingBuffer.Dequeue();
        TransmitAgcNoun();
    }

    private void Command8()
    {
        _incomingBuffer.Dequeue();
        TransmitAgcDeltaV();
    }

    private void Command14()
    {
        _incomingBuffer.Dequeue();
        TransmitClientType();
    }

    private void TransmitClientType()
    {
        byte[] data = new byte[2];
        data[0] = 1;
        data[1] = 0;
        Send(_socket, data);
    }

    private void TransmitAgcVerb()
    {
        Tuple<byte, byte, float> fields = _apolloGuidanceComputer.GetFields();

        byte[] raw = new byte[2];
        raw[0] = 4;
        raw[1] = fields.Item1;
        Send(_socket, raw);
    }

    private void TransmitAgcNoun()
    {
        Tuple<byte, byte, float> fields = _apolloGuidanceComputer.GetFields();

        byte[] raw = new byte[2];
        raw[0] = 5;
        raw[1] = fields.Item2;
        Send(_socket, raw);
    }

    private void TransmitAgcDeltaV()
    {
        Tuple<byte, byte, float> fields = _apolloGuidanceComputer.GetFields();

        byte[] raw = new byte[5];
        raw[0] = 6;
        byte[] fBytes = BitConverter.GetBytes(fields.Item3);
        for (uint i = 0; i < 4; i++)
        {
            raw[i + 1] = fBytes[i];
        }
        
        Send(_socket, raw);
    }

    private void TransmitButtonStates()
    {
        bool[] buttonStates = _hardwareManager.GetButtonStates();

        ushort packed = 0;
        for (int i = 0; i < 9; i++)
        {
            packed |= (ushort)(buttonStates[i] ? (1 << i) : 0);
        }

        byte[] data = new byte[3];
        byte[] packedBytes = BitConverter.GetBytes(packed);

        data[0] = 3;
        data[1] = packedBytes[0];
        data[2] = packedBytes[1];

        Send(_socket, data);
    }

    private void DecodePackets()
    {
        if (_incomingBuffer.Count == 0) { return; }

        byte messageType = _incomingBuffer.Peek();

        switch (messageType)
        {
            case 0:
                Command0();
                break;
            case 1:
                Command1();
                break;
            case 2:
                Command2();
                break;
            case 3:
                Command3();
                break;
            case 4:
                Command4();
                break;
            case 5:
                Command5();
                break;
            case 6:
                Command6();
                break;
            case 7:
                Command7();
                break;
            case 8:
                Command8();
                break;
            case 14:
                Command14();
                break;
            default:
                Debug.LogError("Unknown Message Type: " + messageType);
                _incomingBuffer.Dequeue();
                break;
        }
    }

    private void Start()
    {
        _objectManager = GetComponent<ObjectManager>();
        _hardwareManager = GetComponent<HardwareManager>();
        _apolloGuidanceComputer = GetComponent<ApolloGuidanceComputer>();

        // For debugging (So I don't have to type it in every time.)
        _hardwareManager.ComInputField.text = "COM11";
        NetworkInputField.text = "172.16.0.210:10000";

        const float moonRadius = 1.7375e6f / Scale;
        const float earthRadius = 6373296.0f / Scale;

        _objectManager.Earth.transform.localScale = new Vector3(earthRadius, earthRadius, earthRadius);        
        _objectManager.Moon.transform.localScale = new Vector3(moonRadius, moonRadius, moonRadius);
    }

    private void Update()
    {
        if (_stopTimeout)
        {
            _stopTimeout = false;
            StopCoroutine("ConnectTimeout");
            StartCoroutine("ConnectedMessage");

            _apolloGuidanceComputer.NounButton.interactable = true;
            _apolloGuidanceComputer.VerbButton.interactable = true;
            _apolloGuidanceComputer.DeltaVSetButton.interactable = true;
            _apolloGuidanceComputer.NounInputField.interactable = true;
            _apolloGuidanceComputer.VerbInputField.interactable = true;
            _apolloGuidanceComputer.DeltaVSetInputField.interactable = true;
        }

        if (!_ready) { return; }

        if (_socket != null && !_socket.Connected)
        {
            ConnectButton.GetComponentInChildren<Text>().text = "Connect";
            NetworkInputField.interactable = true;
            ConnectButton.interactable = true;
            NetworkInputField.gameObject.SetActive(true);
            return;
        }

        if (_socket != null && !_receiving)
        {
            Receive(_socket, _socket.Available);
        }

        lock (_incomingBuffer)
        {
            DecodePackets();
        }

        if (_hardwareManager.ButtonsHaveChanged())
        {
            TransmitButtonStates();
        }
    }

    private void OnApplicationQuit()
    {
        if (_socket != null)
        {
            try
            {
                _socket.Shutdown(SocketShutdown.Both);
                _socket.Close();
                _socket = null;
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }
    }
}
