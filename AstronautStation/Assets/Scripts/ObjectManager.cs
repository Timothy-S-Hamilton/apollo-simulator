﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    public GameObject SpaceShip;
    public GameObject Earth;
    public GameObject Moon;

    public static Vector3 AxisSwap(Vector3 h)
    {
        // HX -> UZ
        // HY -> UX
        // HZ -> -UY

        // UX -> HY
        // UY -> -HZ
        // UZ -> HX

        Vector3 U = Vector3.zero;
        U.x = h.y;
        U.y = -h.z;
        U.z = h.x;
        return U;
    }

    private void UpdateObject(GameObject obj, Vector3 position, Vector3 forward, Vector3 down)
    {
        position = AxisSwap(position);
        forward = AxisSwap(forward);
        down = AxisSwap(down);

        obj.transform.position = position;
        obj.transform.rotation = Quaternion.LookRotation(forward, -down);
    }

    public void UpdateShip(Vector3 position, Vector3 forward, Vector3 down)
    {
        UpdateObject(SpaceShip, position, forward, down);
    }

    public void UpdateEarth(Vector3 position, Vector3 forward, Vector3 down)
    {
        UpdateObject(Earth, position, forward, down);
    }

    public void UpdateMoon(Vector3 position, Vector3 forward, Vector3 down)
    {
        UpdateObject(Moon, position, forward, down);
    }
}
