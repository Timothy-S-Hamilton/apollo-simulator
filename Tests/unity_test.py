import time
import struct
import networking

DEBUG = True

protocol = networking.ServerNetworkProtocol('127.0.0.1', 10000)
astronaut_id = None

def set_client_type(network_id, data):
    client_type = data[0]
    if client_type == 0:
        global astronaut_id
        astronaut_id = network_id

        protocol.define_transmit_command(astronaut_id, 1, "<Bffffffffffff") # set object transform
        protocol.define_transmit_command(astronaut_id, 2, "<B") # set led states
        protocol.define_transmit_command(astronaut_id, 3, "") # request led states
        protocol.define_transmit_command(astronaut_id, 4, "") # request button states
        protocol.define_transmit_command(astronaut_id, 5, "<BBBffff") # set agc info
        protocol.define_transmit_command(astronaut_id, 6, "") # request agc info

        protocol.define_receive_command(astronaut_id, 2, '<B', led_state_update_cb)
        protocol.define_receive_command(astronaut_id, 3, '<H', button_state_update_cb)
        protocol.define_receive_command(astronaut_id, 4, '<BBf', apc_state_update_cb)
    else:
        print "Warning, Unknown Client Type:", client_type
        return
    
    protocol.define_transmit_command(network_id, 0, "<I") # Send MET

def new_connection_callback(network_id):
    if DEBUG: print 'new_connection_callback:', network_id
    protocol.define_transmit_command(network_id, 14, "")
    protocol.define_receive_command(network_id, 1, "<B", set_client_type)
    protocol.transmit_command(network_id, 14)

protocol.new_connection_callback = new_connection_callback

led_states = [False] * 2
button_states = [False] * 9
verb = 0
noun = 0
deltaV = 0.0

def set_position(o_id, pos, orient):
    protocol.transmit_command(astronaut_id, 1, [
        o_id,
        pos[0], pos[1], pos[2], 
        orient[0][0], orient[0][1], orient[0][2], 
        orient[1][0], orient[1][1], orient[1][2], 
        orient[2][0], orient[2][1], orient[2][2]
    ])

def set_leds(abort, engine):
    protocol.transmit_command(astronaut_id, 2, [(int(abort) << 1) | int(engine)])

def get_leds():
    protocol.transmit_command(astronaut_id, 3)

def get_buttons():
    protocol.transmit_command(astronaut_id, 4)

def set_time(t):
    protocol.transmit_command(astronaut_id, 0, [int(t)])

def set_agc_values(prog, verb, noun, r1, r2, r3, dvr):
    protocol.transmit_command(astronaut_id, 5, [prog, verb, noun, r1, r2, r3, dvr])

def get_agc_fields():
    protocol.transmit_command(astronaut_id, 6)


def led_state_update_cb(network_id, data):
    led_states[0] = bool(data[0] & 0x02)
    led_states[1] = bool(data[0] & 0x01)
    print 'led_state_update_cb:', led_states

def button_state_update_cb(network_id, data):
    for i in xrange(9):
        button_states[i] = bool(data[0] & (1 << i))
    print 'button_state_update_cb:', button_states

def apc_state_update_cb(network_id, data):
    global verb, noun, deltaV
    verb = data[0]
    noun = data[1]
    deltaV = data[2]
    print 'apc_state_update_cb:', verb, noun, deltaV


try:
    # Wait for at least 1 connection
    # while len(protocol.server.get_client_ids()) == 0:
    #     protocol.update()
    #     time.sleep(0.25)

    dummy_matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    dummy_position1 = [10, 11, 12]
    dummy_position2 = [1, 2, 3]
    dummy_position3 = [-2, 4, 5]
    dummy_position4 = [5, -2, -4]

    mission_start_time = time.time()

    sm = 0
    count = 0
    def fourth_timer_callback():
        global sm, count
        sm += 1
        count += 1
        if sm > 3:
            sm = 0
        
        if astronaut_id is not None and count % 12 == 0:
            get_agc_fields()
        
        if astronaut_id is not None and sm == 0:
            set_leds(False, False)
            set_position(0, dummy_position1, dummy_matrix)
        elif astronaut_id is not None and sm == 1:
            set_leds(False, True)
            set_position(0, dummy_position2, dummy_matrix)
        elif astronaut_id is not None and sm == 2:
            set_leds(True, False)
            set_position(0, dummy_position3, dummy_matrix)
        elif astronaut_id is not None and sm == 3:
            set_leds(True, True)
            set_position(0, dummy_position4, dummy_matrix)
            set_agc_values(99, verb, noun, 1234.5, 123.55, 0.4123, deltaV)

    fourth_timer = networking.Timer(1)
    fourth_timer.add_callback(fourth_timer_callback)
    fourth_timer.start()

    def met_timer_cb():
        met = int(time.time() - mission_start_time)
        if DEBUG: print 'met_timer_cb:', met
        if astronaut_id is not None: set_time(met)

    met_timer = networking.Timer(0.5)
    met_timer.add_callback(met_timer_cb)
    met_timer.start()
    
    while True:
        protocol.update()

        met_timer.update()
        fourth_timer.update()

        time.sleep(1.0 / 60.0)

except KeyboardInterrupt:
    pass

"""
message_type_byte

Set Position (byte id, Vec3, Mat3x3) -> None
Set LED (bool, bool) -> None
Get LED () -> byte (holds 2 bool values)
Get Buttons () ->  byte, byte (holds 9 bool values)
Set Time (uint32_t) -> None

Packet Structure:
    Type Byte
    Data

Packet Types:
    0: Update Object State
        Object ID (Earth: 0, Moon: 1, Ship: 2) Byte
        float x, float y, float z
        float rotation_matrix00, float rotation_matrix01, float rotation_matrix02
        float rotation_matrix10, float rotation_matrix11, float rotation_matrix12
        float rotation_matrix20, float rotation_matrix21, float rotation_matrix22
    1: Set LED State
        LED State Bitfield (Bit 0: Engine LED, Bit 1: Abort LED) Byte
    2: Get LED State
    3: Get Button States
    4: Set Time
        uint32_t time


For the ship attitude, the matrix is
    att_matrix = np.array([[x_b],[y_b],[z_b]])
Where x, y, z are the unit vectors of the ship's "body coordinates".
    x: body forward, in the direction of the nose, the pointed end of the Command Module. 
    y: body right, points to the astronauts' right hand (starboard). 
    z: body down, points in the direction of the astronauts' feet.

This is a right-handed coordinate system.
The components of x at any moment in the simulation are computed relative to the universe's x, y, z coordinates. Also a right-handed system. 
The Earth is at (0,0,0) with the North Pole pointing upward along the +z axis, and the equator lies in the x-y plane. 
The Moon is at position
    x = -3.85e8 (meters)
    y = 0.
    z = 0.
The Earth has a radius 6.37e6 m, and the Moon has a radius 1.7375e6 m.
"""
