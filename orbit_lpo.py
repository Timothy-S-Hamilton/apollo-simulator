#!/usr/bin/env python3

# ORBIT_LPO
# Computes the lunar parking orbit
# Stops after one orbit.

import numpy as np
import math
import sys
from astropy import units as u
from poliastro.bodies import Earth, Moon
from poliastro.twobody import Orbit
from poliastro.twobody.propagation import kepler
from poliastro.twobody.propagation import cowell
import matplotlib.pyplot as P

SAVE_PLOTS=0      # 1=save, 0=display

t_incr=15*u.s  # Time increment
t_incr_annotate=10.*u.min     # Time increment for timestamps on plots

# Earth & Moon coordinates
# ------------------------
pos_earth =[0., 0., 0.]*u.km       #  Earth position in km (for drawing)
pos_moon  =[-3.85e5, 0., 0.]*u.km  #  Moon position in km (for drawing & reference frame)
vel_moon  =[0., 0., 0.]*u.km/u.s   #  Moon velocity in km (for reference frame)

x_Earth=pos_earth[0].value # In km
y_Earth=pos_earth[1].value
z_Earth=pos_earth[2].value
x_Moon= pos_moon[0].value
y_Moon= pos_moon[1].value
z_Moon= pos_moon[2].value
R_Earth=6373.296*u.km
R_Moon=1.7375e3*u.km
R_SOI_Moon=6.629e4*u.km            # Radius of Moon's Sphere of Influence in km

# Lists for orbit plotting
stage1_x=[]
stage1_y=[]
stage1_z=[]
annotate_x=[]
annotate_y=[]
annotate_z=[]
annotate_label=[]

if len(sys.argv)==2:        # Command-line option: filename for input parameters
    infilename=sys.argv[1]
    infile=open(infilename, 'r')
    parameters={}                            # Create parameter dictionary
    for param_line in infile:
        if param_line != '\n':               # Skip blank lines
            param_temp = param_line.split()  # If line not blank, split it
            if param_temp[0] != '#':         # If not a comment line (marked with '#')
                param_name = param_temp[0]   # 1st element is parameter name
                param_value= param_temp[1]   # 2nd element is parameter value
                parameters[param_name] = param_value
    infile.close()
    t_input         = float(parameters['t'])*u.s  # must be in seconds
    x_0             = float(parameters['x_0'])    # must be in meters
    y_0             = float(parameters['y_0'])
    z_0             = float(parameters['z_0'])
    v_read_0x       = float(parameters['v_0x'])   # must be in m/s
    v_read_0y       = float(parameters['v_0y'])
    v_read_0z       = float(parameters['v_0z'])

print('t_input=  ',t_input)
print('x_0=      ',x_0)
print('y_0=      ',y_0)
print('z_0=      ',z_0)
print('v_read_0x=',v_read_0x)
print('v_read_0y=',v_read_0y)
print('v_read_0z=',v_read_0z)

# Calculate velocity unit vector
v_read_mag=math.sqrt(v_read_0x**2 + v_read_0y**2 + v_read_0z**2)
v_0x_hat=v_read_0x/v_read_mag
v_0y_hat=v_read_0y/v_read_mag
v_0z_hat=v_read_0z/v_read_mag

print('Speed before maneuver (m/s): {:.1f}'.format(v_read_mag))
print('Enter speed after maneuver (m/s) or')
input_temp=input('hit <enter> for no change:  ')
try:
    v_0_mag=float(input_temp)
    delta_v=v_0_mag-v_read_mag
    FLAG_DELTAV=1
except:
    v_0_mag=v_read_mag
    FLAG_DELTAV=0

v_0x=v_0_mag*v_0x_hat
v_0y=v_0_mag*v_0y_hat
v_0z=v_0_mag*v_0z_hat

print("\n\n\n*********************")
print(      "* Trajectory Design *")
print(      "*********************\n\n\n")


r_in=[x_0, y_0, z_0]*u.m
v_in=[v_0x, v_0y, v_0z]*u.m/u.s

r_m_in=r_in-pos_moon
v_m_in=v_in-pos_moon

lon_init=math.atan2(r_m_in[1].value/r_m_in[0].value) # Initial longitude

# Create moon-centered orbit
orbit_moon=Orbit.from_vectors(Moon,r_m_in,v_m_in)
ecc=orbit_moon.ecc
sma=orbit_moon.a
peri_alt=sma*(1. - ecc) - R_Moon
apo_alt =sma*(1. + ecc) - R_Moon

# Propagate forward until longitude=0
i=0
delta_t=0
FLAG_LON_INIT=0
while FLAG_LON_INIT==0 and delta_t<1*u.d:  # Upper limit of 1 day to search over
    delta_t=i*t_incr
    # 'Future' values end in "_f".
    orbit_moon_f=orbit_moon.propagate(delta_t, method=cowell) # Use Cowell for Moon
    r_m_f=(orbit_moon_f.r).to(u.km)
    v_m_f=(orbit_moon_f.v).to(u.km/u.s)
    r_m_f_mag=math.sqrt((r_m_f[0].value**2)+(r_m_f[1].value**2)+(r_m_f[2].value**2))*u.km
    v_m_f_mag=math.sqrt((v_m_f[0].value**2)+(v_m_f[1].value**2)+(v_m_f[2].value**2))*u.km/u.s
    
    # Append position to list for plotting        
    stage1_x.append(r_m_f[0].value)
    stage1_y.append(r_m_f[1].value)
    stage1_z.append(r_m_f[2].value)
    
    # Calculate longitude:
    lon=math.atan2(r_m_f[1].value,r_m_f[0].value)
    delta_lon=lon-lon_init
    if i>0:
        if delta_lon>0 and delta_lon_prev<0: # If current & previous points straddle init lon
            i_lon_init=i                         # go with the current point.
            delta_t_lon_init=delta_t
            t_total_lon_init=delta_t_lon_init+t_input
            r_m_lon_init=r_m_f
            v_m_lon_init=v_m_f
            r_m_lon_init_mag=r_m_f_mag
            v_m_lon_init_mag=v_m_f_mag
            alt_lon_init=r_m_lon_init_mag-R_Moon
            
            r_lon_init=r_m_lon_init+pos_moon
            v_lon_init=v_m_lon_init+vel_moon
            
            print("Total time:         {0:.1f}".format(t_total_lon_init.to(u.d))+\
                      " or {0:.1f}".format(t_total_lon_init.to(u.h))+"  or  {0:0.2f}".format(t_total_lon_init))
            print("altitude:           {0:0.5}".format(alt_lon_init))
            print("speed:              {0:0.4}".format(v_lon_init_mag))
            print("apolune altitude:   {0:0.4}".format(apo_alt))
            print("perilune altitude:  {0:0.4}".format(peri_alt))
            if FLAG_DELTAV:
                print('delta-v (m/s)    {:,.1f}'.format(delta_v))
            else:
                print('No delta-v.')
            print('t   (s)          {:,.0f}'.format(t_total_lon_init.to(u.s)))
            print('x0  (m)          {:,.0f}'.format(r_lon_init[0].to(u.m)))
            print('y0  (m)          {:,.0f}'.format(r_lon_init[1].to(u.m)))
            print('z0  (m)          {:,.0f}'.format(r_lon_init[2].to(u.m)))
            print('v0x (m/s)        {:,.1f}'.format(v_lon_init[0].to(u.m/u.s)))
            print('v0y (m/s)        {:,.1f}'.format(v_lon_init[1].to(u.m/u.s)))
            print('v0z (m/s)        {:,.1f}'.format(v_lon_init[2].to(u.m/u.s)))
            print('\nFor further orbit design, \n')
            print('Copy directly to input parameter file:')
            print('--------------------------------------\n')
            print('t             {:.0f}'.format(t_total_lon_init.to(u.s).value))
            print('x_0           {:.0f}'.format(r_lon_init[0].to(u.m).value))
            print('y_0           {:.0f}'.format(r_lon_init[1].to(u.m).value))
            print('z_0           {:.0f}'.format(r_lon_init[2].to(u.m).value))
            print('v_0x          {:.1f}'.format(v_lon_init[0].to(u.m/u.s).value))
            print('v_0y          {:.1f}'.format(v_lon_init[1].to(u.m/u.s).value))
            print('v_0z          {:.1f}'.format(v_lon_init[2].to(u.m/u.s).value))
            print('\n')
            FLAG_LON_INIT=1
    lon_prev=lon
    i=i+1
if FLAG_LON_INIT==0:
    print('\n**************************************************')
    print(  '* FAILED TO REACH INITIAL LONGITUDE WITHIN 1 DAY *')
    print(  '**************************************************\n')

            




# Lunar Parking Orbit Plot
# -----------------------
# Draw Moon (x-y plane):
moon_x=[(R_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
moon_y=[(R_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(moon_x,moon_y,color='gray')
# Plot orbit (x-y plane):
P.plot(stage1_x,stage1_y,color='blue')
#for ii in range(len(annotate_label)):
#    P.annotate(annotate_label[ii], (annotate_x[ii],annotate_y[ii]))
P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
P.xlabel('x (km)')
P.ylabel('y (km)')
P.title('Lunar Parking Orbit')
if SAVE_PLOTS:
    P.savefig('orbit_lpo.pdf')
else:
    P.show()


print('\nTo find spacecraft state vector at time t, ')
print('enter time in minutes between',t_input.to(u.min),'and ',t_total_lon_init.to(u.min))
input_temp=input('Time: (min) ')
t_selected=float(input_temp)*u.min

delta_t=t_selected-t_input   # Must start from beginning of calculation
orbit_moon_f=orbit_moon.propagate(delta_t, method=cowell) # Use Cowell for Moon
r_m_f=(orbit_moon_f.r).to(u.m)     # Convert to m
v_m_f=(orbit_moon_f.v).to(u.m/u.s) # Convert to m/s
r_f=r_m_f+pos_moon
v_f=v_m_f+vel_moon
x_selected=r_f[0].value
y_selected=r_f[1].value
z_selected=r_f[2].value
vx_selected=v_f[0].value
vy_selected=v_f[1].value
vz_selected=v_f[2].value

    
# Print results:
print('*********************************')
print('  State Vector at Selected Time   \n')
print('To move spacecraft to position, ')
print('read the following to Simulation Director ')
print('for manual entry to input parameter file:')
print('----------------------------------\n')
if FLAG_DELTAV:
    print('delta-v (m/s)                  {:,.1f}'.format(delta_v))
    print('velocity after maneuver (m/s)  {:,.1f}'.format(v_0_mag))
else:
    print('No delta-v.')
    print('initial velocity: (m/s) {:,.1f}'.format(v_0_mag))
print('t   (s)          {:,.0f}'.format(t_selected.to(u.s)))
print('x0  (m)          {:,.0f}'.format(x_selected))
print('y0  (m)          {:,.0f}'.format(y_selected))
print('z0  (m)          {:,.0f}'.format(z_selected))
print('v0x (m/s)        {:,.1f}'.format(vx_selected))
print('v0y (m/s)        {:,.1f}'.format(vy_selected))
print('v0z (m/s)        {:,.1f}'.format(vz_selected))       
print('\nFor further trajectory design,')
print('Copy directly to input parameter file:')
print('--------------------------------------\n')
print('t             {:.0f}'.format(t_selected.to(u.s)))
print('x_0           {:.0f}'.format(x_selected))        
print('y_0           {:.0f}'.format(y_selected))        
print('z_0           {:.0f}'.format(z_selected))        
print('v_0x          {:.1f}'.format(vx_selected))       
print('v_0y          {:.1f}'.format(vy_selected))       
print('v_0z          {:.1f}'.format(vz_selected))       
print('\n')
