#!/usr/bin/env python

# Space flight simulation, v.6
# Timothy S. Hamilton September 24, 2018
# Using Verlet integration
# Adds atmospheric drag

import math
import numpy as np
import matplotlib.pyplot as P
import time
import curses
import curses.textpad
import sys
import datetime
import networkingV1
import atexit

# Debugging options
DEBUG         =0    # Prints variable values and quits
MAKE_PLOTS    =1    # After quit, 1=make plots 0=don't make plots
INIT_RADIAL   =1    # Initial position specified as (1) radial (0) cartesian coords
EARTH_MOON    =0    # Plotting range options: 0=Earth orbit, 1=Earth to Moon.

# What separate equipment & stations to use?
USE_PANEL     =1    # 0=No control panel attached, 1=use control panel, disable some keystrokes
USE_ASTRONAUTS=1    # Separate astronauts' computer
USE_SIMDIR    =0    # Have a Simulation Director
                    # *** Eventually: this will read in a parameter file every several cycles that
                    # sets variables, including the ability to override astronauts' controls.
USE_FDO       =1    # FDO station
USE_GUIDO     =1    # GUIDO station
USE_GNC       =1    # GNC station
USE_CAPCOM    =0    # CAPCOM station
USE_BOOSTER   =1    # BOOSTER station
USE_RETRO     =1    # RETRO station
USE_MCC = USE_FDO+USE_GUIDO+USE_GNC+USE_CAPCOM+USE_BOOSTER+USE_RETRO
                    # If USE_MCC=0, then NO Mission Control stations in use.
                    #           >0       at least one MCC station is in use.
USE_EXTERNAL=USE_MCC+USE_PANEL+USE_ASTRONAUTS
                    # If USE_EXTERNAL=0, then the only interaction is with this computer.
#USE_LAUNCH    =0    # 1=perform launch program from ground, 0=begin in space at initial position & velocity
ORBIT_INSERTION_FLAG = 0

ASTRONAUT_NET_ID_LIST = set()
FDO_NET_ID_LIST = set()
GUIDO_NET_ID_LIST = set()
GNC_NET_ID_LIST = set()
CAPCOM_NET_ID_LIST = set()
BOOSTER_NET_ID_LIST = set()
RETRO_NET_ID_LIST = set()
met = 0

met_timer = networkingV1.Timer(0.25)
led_timer = networkingV1.Timer(0.5)
station_sync_timer = networkingV1.Timer(0.1)
log_handle = open("log.txt", "w")

def log(*data):
    for i in xrange(len(data)):
        log_handle.write(str(data[i]))
        if i != len(data) - 1:
            log_handle.write(" ")
    log_handle.write("\n")
    log_handle.flush()

NETWORKED = False
if USE_MCC or USE_EXTERNAL:
    protocol = networkingV1.ServerNetworkProtocol()
    address_msg = 'Your Server Address is: ' + protocol.ip + ":" + str(protocol.port)
    log(address_msg)
    print address_msg
    raw_input("Press Enter to Continue...")
    NETWORKED = True

def clean_up():
    if NETWORKED:
        protocol.close()
    log_handle.close()

atexit.register(clean_up)

def set_position(o_id, pos, orient):
    for net_id in ASTRONAUT_NET_ID_LIST:
        protocol.transmit_command(net_id, 1, [
            o_id,
            pos[0], pos[1], pos[2],
            orient[0][0][0], orient[0][0][1], orient[0][0][2],
            orient[1][0][0], orient[1][0][1], orient[1][0][2],
            orient[2][0][0], orient[2][0][1], orient[2][0][2]
        ])

def set_leds(abort, engine):
    for net_id in ASTRONAUT_NET_ID_LIST:
        protocol.transmit_command(net_id, 2, [(int(abort) << 1) | int(engine)])

def set_agc_values(prog, verb, noun, r1, r2, r3, dvr):
    for net_id in ASTRONAUT_NET_ID_LIST:
        protocol.transmit_command(net_id, 5, [prog, verb, noun, r1, r2, r3, dvr])

def button_state_update_cb(network_id, data):
    global ABORT_FLAG, ENGINE_FLAG, SEP_SIVB_FLAG, SEP_SM_FLAG
    global PROGRADE_FLAG, HEADUP_FLAG, ATT_HOLD_FLAG
    global CHUTE_DROGUE_FLAG, CHUTE_MAIN_FLAG

    button_states = []
    for i in xrange(9):
        button_states.append(bool(data[0] & (1 << i)))
    log('button_state_update_cb:', button_states)

    ABORT_FLAG = int(button_states[0])
    ENGINE_FLAG = int(button_states[1])
    SEP_SIVB_FLAG = int(button_states[2])
    SEP_SM_FLAG = int(button_states[3])

    CHUTE_DROGUE_FLAG = int(button_states[4])
    CHUTE_MAIN_FLAG = int(button_states[5])

    if button_states[6]: PROGRADE_FLAG = 1
    else: PROGRADE_FLAG = -1

    if button_states[7]: HEADUP_FLAG = 1
    else: HEADUP_FLAG = -1

    ATT_HOLD_FLAG = int(button_states[8])

def apc_state_verb_update_cb(network_id, data):
    global agc_verb
    log('apc_state_verb_update_cb:', data)
    agc_verb = data[0]

def apc_state_noun_update_cb(network_id, data):
    global agc_noun
    log('apc_state_noun_update_cb:', data)
    agc_noun = data[0]

def apc_state_deltaV_update_cb(network_id, data):
    global delta_v_set
    log('apc_state_deltaV_update_cb:', data)
    delta_v_set = data[0]

def fido_button_pressed(network_id, data):
    global ABORT_REQUEST_FLAG
    log('fido_button_pressed:', network_id, data)
    ABORT_REQUEST_FLAG = 1

def set_client_type(network_id, data):
    client_type = data[0]
    log("set_client_type:", network_id, data)

    if client_type == 0:
        ASTRONAUT_NET_ID_LIST.add(network_id)
        log('Astronaut connected!')

        protocol.define_transmit_command(network_id, 1, "<Bffffffffffff") # set object transform
        protocol.define_transmit_command(network_id, 2, "<B") # set led states
        protocol.define_transmit_command(network_id, 3, "") # request led states
        protocol.define_transmit_command(network_id, 4, "") # request button states
        protocol.define_transmit_command(network_id, 5, "<BBBffff") # set agc info
        protocol.define_transmit_command(network_id, 6, "") # request verb
        protocol.define_transmit_command(network_id, 7, "") # request noun
        protocol.define_transmit_command(network_id, 8, "") # request deltaV

        protocol.define_receive_command(network_id, 3, '<H', button_state_update_cb)
        protocol.define_receive_command(network_id, 4, '<B', apc_state_verb_update_cb)
        protocol.define_receive_command(network_id, 5, '<B', apc_state_noun_update_cb)
        protocol.define_receive_command(network_id, 6, '<f', apc_state_deltaV_update_cb)
    elif client_type == 1:
        FDO_NET_ID_LIST.add(network_id)

        log('FIDO connected!')

        protocol.define_receive_command(network_id, 0, "", fido_button_pressed)
        protocol.define_transmit_command(network_id, 1, "<fff")
        protocol.define_transmit_command(network_id, 2, "<fff")
        protocol.define_transmit_command(network_id, 3, "<f")
        protocol.define_transmit_command(network_id, 4, "<f")
        protocol.define_transmit_command(network_id, 5, "<f")
        protocol.define_transmit_command(network_id, 6, "<f")
        protocol.define_transmit_command(network_id, 7, "<f")
        protocol.define_transmit_command(network_id, 8, "<f")
        protocol.define_transmit_command(network_id, 9, "<f")
        protocol.define_transmit_command(network_id, 10, "<f")
        protocol.define_transmit_command(network_id, 11, "<B")
        protocol.define_transmit_command(network_id, 12, "<B")
        protocol.define_transmit_command(network_id, 13, "", True)
        protocol.define_transmit_command(network_id, 14, "")
        protocol.define_transmit_command(network_id, 15, "<B")
    elif client_type == 2:
        GUIDO_NET_ID_LIST.add(network_id)

        log('GUIDO connected!')

        protocol.define_transmit_command(network_id, 1, "<f")
        protocol.define_transmit_command(network_id, 2, "<f")
        protocol.define_transmit_command(network_id, 3, "<f")
        protocol.define_transmit_command(network_id, 4, "<B")
        protocol.define_transmit_command(network_id, 5, "<b")
        protocol.define_transmit_command(network_id, 6, "<b")
        protocol.define_transmit_command(network_id, 7, "<B")
    elif client_type == 3:
        GNC_NET_ID_LIST.add(network_id)

        log('GNC connected!')

        protocol.define_transmit_command(network_id, 1, "<B")
        protocol.define_transmit_command(network_id, 2, "<B")
        protocol.define_transmit_command(network_id, 3, "<B")
        protocol.define_transmit_command(network_id, 4, "<B")
        protocol.define_transmit_command(network_id, 5, "<B")
        protocol.define_transmit_command(network_id, 6, "<B")
        protocol.define_transmit_command(network_id, 7, "<B")
        protocol.define_transmit_command(network_id, 8, "<b")
        protocol.define_transmit_command(network_id, 9, "<b")
        protocol.define_transmit_command(network_id, 10, "<B")
    elif client_type == 4:
        BOOSTER_NET_ID_LIST.add(network_id)

        log('BOOSTER connected!')

        protocol.define_transmit_command(network_id, 1, "<f")
        protocol.define_transmit_command(network_id, 2, "<f")
        protocol.define_transmit_command(network_id, 3, "<B")
        protocol.define_transmit_command(network_id, 4, "<B")
        protocol.define_transmit_command(network_id, 5, "<B")
        protocol.define_transmit_command(network_id, 6, "<B")
        protocol.define_transmit_command(network_id, 7, "<B")
        protocol.define_transmit_command(network_id, 8, "<B")
        protocol.define_transmit_command(network_id, 9, "<B")
        protocol.define_transmit_command(network_id, 10, "<B")
        protocol.define_transmit_command(network_id, 11, "<B")
        protocol.define_transmit_command(network_id, 12, "<B")
        protocol.define_transmit_command(network_id, 13, "<B")
        protocol.define_transmit_command(network_id, 15, "<B")
        protocol.define_transmit_command(network_id, 16, "<B")
        protocol.define_transmit_command(network_id, 17, "<B")
    elif client_type == 5:
        RETRO_NET_ID_LIST.add(network_id)

        log('RETRO connected!')

        protocol.define_transmit_command(network_id, 0, "<I")
        protocol.define_transmit_command(network_id, 1, "<f")
        protocol.define_transmit_command(network_id, 2, "<f")
        protocol.define_transmit_command(network_id, 3, "<f")
        protocol.define_transmit_command(network_id, 4, "<f")
        protocol.define_transmit_command(network_id, 5, "<B")
        protocol.define_transmit_command(network_id, 6, "<f")
        protocol.define_transmit_command(network_id, 7, "<f")
        protocol.define_transmit_command(network_id, 8, "<f")
        protocol.define_transmit_command(network_id, 9, "<B")
        protocol.define_transmit_command(network_id, 10, "<B")
        protocol.define_transmit_command(network_id, 11, "<B")
        protocol.define_transmit_command(network_id, 12, "<B")
        protocol.define_transmit_command(network_id, 13, "<B")
        protocol.define_transmit_command(network_id, 15, "<B")
        protocol.define_transmit_command(network_id, 16, "<b")
        protocol.define_transmit_command(network_id, 17, "<b")
    elif client_type == 6:
        CAPCOM_NET_ID_LIST.add(network_id)

        log('CAPCOM connected!')
    else:
        log("Warning, Unknown Client Type:", client_type)
        return

    protocol.define_transmit_command(network_id, 0, "<I") # Send MET

def new_connection_callback(network_id):
    log('new_connection_callback:', network_id)
    protocol.define_transmit_command(network_id, 14, "")
    protocol.define_receive_command(network_id, 1, "<B", set_client_type)
    protocol.transmit_command(network_id, 14)

def lost_connection_callback(network_id):
    log('lost_connection_callback:', network_id)
    if network_id in ASTRONAUT_NET_ID_LIST:
        ASTRONAUT_NET_ID_LIST.remove(network_id)
        return
    if network_id in FDO_NET_ID_LIST:
        FDO_NET_ID_LIST.remove(network_id)
        return
    if network_id in GUIDO_NET_ID_LIST:
        GUIDO_NET_ID_LIST.remove(network_id)
        return
    if network_id in GNC_NET_ID_LIST:
        GNC_NET_ID_LIST.remove(network_id)
        return
    if network_id in CAPCOM_NET_ID_LIST:
        CAPCOM_NET_ID_LIST.remove(network_id)
        return
    if network_id in BOOSTER_NET_ID_LIST:
        BOOSTER_NET_ID_LIST.remove(network_id)
        return
    if network_id in RETRO_NET_ID_LIST:
        RETRO_NET_ID_LIST.remove(network_id)
        return

if NETWORKED: 
    protocol.new_connection_callback = new_connection_callback
    protocol.lost_connection_callback = lost_connection_callback

#if USE_LAUNCH:
#    LAUNCH_FLAG=1   # 1 during launch phase
#else:
#    LAUNCH_FLAG=0   # 0 after exiting atmosphere or when starting from space.

# Key assignments--change these here if desired
KEY_ENGINE    =' '  # Engine on/off
KEY_QUIT      ='q'
KEY_SPEED_INC ='+'  # Increase simulation speed
KEY_SPEED_DEC ='-'  # Decrease simulation speed
KEY_PAUSE     ='x'  # Pause simulation
KEY_PROGRADE  ='p'  # Set attitude prograde
KEY_RETROGRADE='r'  # Set attitude retrograde
KEY_HEADUP    ='u'  # Set attitude head-up
KEY_HEADDOWN  ='d'  # Set attitude head-down
KEY_EDITOR    ='e'  # Enter editor for more detailed variables
KEY_SHOWPLOT  ='s'  # Show spacecraft position
KEY_SAVEDATA  ='f'  # Save data file of spacecraft state
KEY_CHUTES    ='c'  # Deploy parachutes
KEY_VERB      ='v'  # Guidance computer verb entry
KEY_NOUN      ='n'  # Guidance computer noun entry
KEY_ABORT     ='a'  # Abort  ***NOT IMPLEMENTED YET***
KEY_DELTAV    ='t'  # Set Target delta-v
KEY_FAIL_SII_1='1'  # S-II engine #1 fails
KEY_FAIL_SII_2='2'  # S-II engines #1 & 2 fail



# Coordinates to place text & graphics
DSKY_U=3   # upper line for DSKY
DSKY_L=52  # left column for DSKY
STAT_U=40  # upper line for status bar
STAT_L=54  # left column for status bar
WIN_N_BLANK_LINES=40

SIC_F1_1_FLAG = 0  # *** Might Move these to Initial Values section.
SIC_F1_2_FLAG = 0
SIC_F1_3_FLAG = 0
SIC_F1_4_FLAG = 0
SIC_F1_5_FLAG = 0
SII_J2_1_FLAG = 0
SII_J2_2_FLAG = 0
SII_J2_3_FLAG = 0
SII_J2_4_FLAG = 0
SII_J2_5_FLAG = 0
SEP_SIC_FLAG  = 0
SEP_SII_FLAG  = 0
SIVB_J2_1_FLAG= 0
SEP_SIVB_FLAG = 0

# Simulation parameters
# ---------------------
dt=        0.1                     # s           Time step in "simulation time."  Adjustable
delay=     dt                      #             Delay after each iteration. Sets sim speed. Adjustable
outfilerootname='craft_state_v6_'  # Root filename to use in naming output data files
#lon_fudge=0.86                    # fudge factor for longitude polynomial
FDO_plot_interval=30               # How many seconds between writing FDO's plots?
last_time=time.time()              # Used in counting intervals for writing FDO's plots
ext = "png"                        # File extension for FDO's plots
SimDir_display_interval=2          # How many loop intervals between displaying data on Simulation Director's screen?
t_countdown=-0.25                  # Length of countdown in minutes (must be negative)
#t_orbit_insertion=708.            # Time to end launch polynomials and begin normal simulation
t_orbit_insertion=576.0            # Time to end launch polynomials and begin normal simulation
                                   # 708 is historically correct; 576 works in practice to get a circular orbit.
TEMP_FLAG=0

# Constants
G=         6.67E-11 # N m^2 kg^-2 Gravitational constant
g_E=       9.80665  # m/s^2       gravitational acceleration at Earth surface
M_air=     0.0289644# kg/mol      molar mass of Earth's air
R_gas=     8.3144598# N m mol^-1 K^-1 universal gas constant for air
M_Earth=   5.97E+24 # kg          Mass of Earth
x_Earth=   0.       # m           Position of center of Earth (origin)
y_Earth=   0.       # m
z_Earth=   0.       # m
#R_Earth=   6.37e6   # m           Radius of Earth
R_Earth=   6373296. # m           Radius of Earth in NASA data  *** UPDATE TO THIS VALUE ***
M_Moon=    7.35E+22 # kg          Mass of Moon
x_Moon=   -3.85e8   # m           Position of the center of Moon
y_Moon=    0.       # m
z_Moon=    0.       # m
R_Moon=    1.7375e6 # m           Radius of Moon
R_SOI_Moon=6.629e7  # m           Radius of Moon's Sphere of Influence
FLAG_ATM=  1        #             0=No atmosphere, 1=use exp atmosphere below altitude of...
#h_max_atm= 86000.   # m           Upper limit of atmosphere model
h_max_atm= 170000.   # m           Upper limit of atmosphere model
ionization_KE=7725. # J/m^3       Minimum KE/vol of relative wind for ionization blackout.
v_sound=   340.     # m/s         Speed of sound ***Should calculate as fct. of altitude!
KE_breakup=6000.    # J/m^3       Minimum KE/vol of CM breakup if prograde  ***I made up this number



# +-----------------+
# | Spacecraft data |
# +-----------------+
# Mass
# ----
m_SIC  =      135218.   # kg          S-IC dry mass
m_SIC_fuel = 2151035.   # kg          S-IC fuel mass
m_SII  =       39048.   # kg          S-II dry mass
m_SII_fuel =  451730.   # kg          S-II fuel mass
m_SIVB =       13311.   # kg          S-IVB dry mass
m_SIVB_fuel = 106605.   # kg          S-IVB fuel mass
m_SM         =  6110.   # kg          SM dry mass
m_SM_SPS_fuel= 18410.   # kg          SM SPS fuel mass
m_CM   =        5307.   # kg          CM mass w/o heat shield
m_htsd =        1360.   # kg          heat shield mass
m_LM  =         4173.   # kg          LM dry mass
m_LM_DPS_fuel= 10523.   # kg          LM DPS fuel mass?? *** Not separating descent/ascent stages here. ***
m_A13_MCC5  =  39488.   # kg          Apollo 13 mass @ MCC-5
m_A8_TLI  =    28817.   # kg          Apollo 8 mass @ TLI

# Engine settings
# ---------------
T_SIC_F1=7770.e3  # N          F-1 engine max thrust (vacuum)
T_SII_J2=1033.1e3 # N          J-2 engine max thrust (vacuum)
T_SIVB=1.001e6    # N          S-IV B max thrust (Note slight difference with J-2 above)
T_SPS=91202.      # N          SM's Service Propulsion System thrust
T_DPS=45038.      # N          LM's Descent Propulsion System max thrust
T_SM_RCS=440.*4   # N          Set of 4 (???) SM RCS thrusters in translational control
T_CM_RCS=410.*4   # N          Set of 4 (???) CM RCS thrusters in translational control

# Other
# -----
A_CM =         11.71 # m^2         Apollo CM cross-sectional area (down the axis of symmetry)
A_drogue=      10.59 # m^2         Drogue parachute cross-section area
A_main=        1171. # m^2         3 Main parachutes cross-section area
C_D_CM_super = 1.55  #             CM drag coefficient average for supersonic
C_D_CM_sub   = 0.9   #             CM drag coefficient for subsonic
C_D_drogue   = 0.5   #             2 Ribbon parachutes  ***Look correct value up!***
                     #             Deploy at 7.3 km; should slow CM (w/o heat shield) to 55 m/s
C_D_main     = 0.5   #             3 Ringsail parachutes  ***Look correct value up!***
                     #             Deploy at 3.3 km; should slow CM (w/o heat shield) to 10 m/s
fudge_chute1=  0.005 #             ***Fudge factor to REDUCE drogue chute drag to realistic values
                     #             Engage at 6-7 km altitude, reduces speed to ~55 m/s
fudge_chute2=  0.05  #             ***Fudge factor to REDUCE main chute drag to realistic values
                     #             Engage at ~5 km altitude, reduces speed to ~10 m/s
L_D =          0.31  #             Lift-to-drag ratio for 160-deg angle-of-attack
t_chute_max  = 10.   # s           Time to fully open parachutes  ***Look correct value up!***
SAFE_EARTH =   30.   # m/s         Min. speed for safe splashdown on Earth
SAFE_MOON  =   10.   # m/s         Min. speed for safe touchdown on Moon

# Initial values
# --------------
#if USE_LAUNCH:
#    #t=570.
#    t=t_countdown*60 # s          Length of countdown to launch, in minutes
#else:    
#    t=         0    # s           Initial time for mission clock.
i=             0    #             Initial iteration number
ENGINE_FLAG=   0    #             Engine initially OFF
#FLAG_ATTITUDE='p'   # p/r/m [n/s] Init attitude: Prograde/Retrograde/Manual [North/South unsupported]
CHUTE_STATUS=  0    #             Parachute status
t_chute=       0    #             Stopwatch for parachute deployment
D_chute=       0    #             Parachute drag (before deployment)
T=             0.   # N           Initial engine thrust (off)
agc_verb=      0    #             Apollo Guidance Computer verb
agc_noun=      0    #             AGC noun
agc_prog=      0    #             AGC program
agc_r1=        0    #             AGC register 1
agc_r2=        0    #             AGC register 2
agc_r3=        0    #             AGC register 3
CRASH_E=       0    #             Flag for crashing into Earth
CRASH_M=       0    #             Flag for crashing into Moon
LAND_E=        0    #             Flag for landing on Earth
LAND_M=        0    #             Flag for landing on Moon
sma=           0    # km          Semi-major axis of Keplerian orbit
ecc=           0    #             Eccentricity of Keplerian orbit
inclin=        0    # deg         Inclination of Keplerian orbit
peri=          0    # m           Periapsis radius
peri_alt=      0    # km          Periapsis altitude
apo=           0    # m           Apoapsis radius
apo_alt=       0    # km          Apoapsis altitude
time_loop_diff=0.   #             Time to complete main program loop
h_dot=         0.   # m/s         Rate of change of altitude
rho=           0.   # kg/m^3      Air density
KE_wind=       0.   #             Kinetic energy/volume of relative wind
FLAG_FAIL_SII_1=0
FLAG_FAIL_SII_2=0

agc_r1_fmt="{0:06.0f}"
agc_r2_fmt="{0:06.0f}"
agc_r3_fmt="{0:06.0f}"



ORBIT_SHAPE='elliptical' # elliptical/parabolic/hyperbolic  Shape of orbit
MOON_FLAG=         0  #          Which sphere of influence: 0=Earth, 1=Moon
HEADUP_FLAG=       1  #          1=Head-up attitude, -1=Head-down attitude
PROGRADE_FLAG=     1  #          1=Prograde, -1=retrograde
ATT_HOLD_FLAG=     0  #          0=update attitude, 1=hold attitude constant
SEP_SIVB_FLAG=     0  #          0=S-IVB stage is attached,    1=S-IVB has been separated
SEP_SM_FLAG=       0  #          0=Service Module is attached, 1=SM has been separated
SEP_HTSD_FLAG=     0  #          0=CM heatshield is attached,  1=heatshield has been separated
#CURRENT_ENGINE='SIVB' #          {SIVB, SPS, NONE}, depending on current staging
ION_BLACKOUT_FLAG= 0  #          Ionization blackout: 0=No blackout.  1=Blackout.
MOON_BLACKOUT_FLAG=0  #          Blackout from being on dark side of moon: 0=in contact, 1=blackout
GROUNDSTATION_FLAG=1  #          Ground station radio coverage: 0=none. 1=in contact.
ABORT_FLAG=        0  #          0=not aborting, 1=aborting
ABORT_REQUEST_FLAG=0  #          0=no abort requested, 1=abort requested
AGC_V06_FLAG=      0  #          0 when verb 6 is first entered, 1 after its noun values have been updated
delta_v_target=    0. # m/s      Target delta-v for maneuver
v_target=          0. # m/s      target
BREAKUP_FLAG=      0  #          0=spaceraft intact, 1=spacecraft breakup
CHUTE_DROGUE_FLAG= 0
CHUTE_MAIN_FLAG=   0
SIVB_J2_1_BURN1_FLAG=0
which_poly_r='None'
which_poly_long='None'
SIC_F1_1_FLAG=0
SIC_F1_2_FLAG=0
SIC_F1_3_FLAG=0
SIC_F1_4_FLAG=0
SIC_F1_5_FLAG=0
SII_J2_1_FLAG=0
SII_J2_2_FLAG=0
SII_J2_3_FLAG=0
SII_J2_4_FLAG=0
SII_J2_5_FLAG=0
SIVB_J2_1_FLAG=0


# Create lists to store data for plots:
# -------------------------------------
t_min_list=[]
t_min_F_gE_list=[]
t_min_F_gM_list=[]
theta_E_deg_list=[]
phi_E_deg_list=[]
h_E_km_list=[]
h_M_km_list=[]
x_list=[]
y_list=[]
z_list=[]
x_km_list=[]
y_km_list=[]
z_km_list=[]
v_list=[]
F_gE_list=[]
F_gM_list=[]
g_sub_list=[]
h_dot_list=[]
KE_wind_list=[]


# Initialize curses
win = curses.initscr()
win.timeout(10)     # Delay (ms) while waiting for input
curses.cbreak()
curses.noecho()
curses.start_color()
curses.use_default_colors()
win.keypad(1)
curses.init_pair(1,curses.COLOR_GREEN,curses.COLOR_WHITE)
curses.init_pair(2,curses.COLOR_BLACK,curses.COLOR_RED)
curses.init_pair(3,curses.COLOR_BLUE, curses.COLOR_WHITE)

# ----------------
# Initial position
# ----------------
#if USE_LAUNCH:
#    # h_E0= 0.         # m  Launch from ground
#    r_E0= 6373355.     # m  Uses NASA data
#    h_E0= r_E0-R_Earth
#else:
#    # h_E0= 185200.    # m  Initial Earth altitude for orbit (100 nmi)
#    h_E0= 85000.       # m    "      "      "      for testing atmospheric reentry (85 km)
#if INIT_RADIAL:
#    # Set initial longitude & latitude here:
#    #lon_0= -80.604161  # Pad 39A Cape Canaveral   
#    #lat_0=  28.608057
#    lon_0= -80.604161  # deg.   Pad 39A Cape Canaveral longitude   
#    lat_0=   0.0       # deg.   Equator
#    
#    # Calculate init radial coordinates
#    r_E0         = R_Earth + h_E0
#    phi_E0_deg   = lon_0      # Later will need to account for rotation of the earth!
#    theta_E0_deg = 90.-lat_0  # theta is colatitude
#    #r_E0     = 6631036.64897
#    #phi_E0   = 0.0
#    #theta_E0 = 1.41941279729
#    
#    # Convert phi & theta from deg -> rad
#    phi_E0   = (math.pi/180.)*phi_E0_deg
#    theta_E0 = (math.pi/180.)*theta_E0_deg
#    
#    # Calculate x_0, y_0, z_0
#    x_0= r_E0 * math.sin(theta_E0) * math.cos(phi_E0)
#    y_0= r_E0 * math.sin(theta_E0) * math.sin(phi_E0)
#    z_0= r_E0 * math.cos(theta_E0)
#else:
#    # Else use cartesian initial coordinates
#    x_0= R_Earth + h_E0 # m           Initial x
#    y_0= 0.             # m           Initial y
#    z_0= 1000000.       # m           Initial z
#
## Initial velocity:
#if USE_LAUNCH:
#    # Don't use the following, b/c simulation thinks you're crashing!
#    # v_0 = 465.10        # m/s         Inertial velocity on ground at equator
#    v_0=0.
#else:
#    v_0 = 7802.          # m/s         LEO circular orbit
#    # v_0y= 10822.        # m/s         Initial v_y @ Apollo 8 TLI v
#    # v_0y= 10926.        # m/s         Initial v_y for my TLI--swing behind Moon
#    # v_0y= 10913.        # m/s         Initial v_y for my TLI--figure-8
#if INIT_RADIAL:
#    # Set initial velocity here
#    # Valid for init v tangent to Earth, azimuth due East
#    # Orbital Mechanics, 3rd ed., pp. 67-70
#    v_0x= -1.*v_0*math.sin(phi_E0)
#    v_0y=     v_0*math.cos(phi_E0)
#    v_0z= 0.
#else:
#    # Cartesian initial velocity--only works for LEO near equator!
#    v_0x= 0.            # m/s         Initial v_x
#    v_0y= 7802.         # m/s         Initial v_y Circular Earth orbit
#    v_0z= 0.            # m/s         Initial v_z


# +---------------------------+
# | Read Input Parameter File |
# +---------------------------+
if len(sys.argv)==2:        # Command-line option: filename for input parameters
    infilename=sys.argv[1]

    infile=open(infilename, 'r')
    parameters={}                            # Create parameter dictionary
    for param_line in infile:
        if param_line != '\n':               # Skip blank lines
            param_temp = param_line.split()  # If line not blank, split it
            if param_temp[0] != '#':         # If not a comment line (marked with '#')
                param_name = param_temp[0]   # 1st element is parameter name
                param_value= param_temp[1]   # 2nd element is parameter value
                parameters[param_name] = param_value
    infile.close()

    #t_hr            = float(parameters['t_hr'])
    #t_min           = float(parameters['t_min'])
    #t_sec           = float(parameters['t_sec'])
    #m_craft         = float(parameters['m_craft'])
    #T_max           = float(parameters['T_max'])
    #ENGINE_FLAG     = int(parameters['ENGINE_FLAG'])
    #CHUTE_STATUS    = int(parameters['CHUTE_STATUS'])
    #t_chute         = float(parameters['t_chute'])
    #T               = float(parameters['T'])
    #MOON_FLAG       = int(parameters['MOON_FLAG'])
    USE_LAUNCH      = int(parameters['USE_LAUNCH'])
    SEP_SIVB_FLAG   = int(parameters['SEP_SIVB_FLAG'])
    SEP_SM_FLAG     = int(parameters['SEP_SM_FLAG'])
    PROGRADE_FLAG   = int(parameters['PROGRADE_FLAG'])
    HEADUP_FLAG     = int(parameters['HEADUP_FLAG'])
    agc_verb        = int(parameters['agc_verb'])
    agc_noun        = int(parameters['agc_noun'])
    INIT_RADIAL     = int(parameters['INIT_RADIAL'])
    t               = float(parameters['t'])
    x_0             = float(parameters['x_0'])
    y_0             = float(parameters['y_0'])
    z_0             = float(parameters['z_0'])
    v_0x            = float(parameters['v_0x'])
    v_0y            = float(parameters['v_0y'])
    v_0z            = float(parameters['v_0z'])


# Calculate required initial variables from those read in from file
v_0  = math.sqrt((v_0x**2) + (v_0y**2) + (v_0z**2))    
r_E0 = math.sqrt((x_0**2)  + (y_0**2)  + (z_0**2))
h_E0 = r_E0 - R_Earth

# Initial mass and engine settings
if SEP_SIVB_FLAG==0:
    #m_craft = m_A8_TLI        # S-IVB & SM attached. ***Why is this so much less than the total below?
    m_craft = m_SIVB + m_SM + m_SM_SPS_fuel + m_CM + m_htsd  # Start with *dry* S-IVB, SM, & CM
    CURRENT_ENGINE='SIVB'
    T_max=T_SIVB

if SEP_SIVB_FLAG==1 and SEP_SM_FLAG==0:
    m_craft = m_SM + m_SM_SPS_fuel + m_CM + m_htsd  # Start with SM & CM
    CURRENT_ENGINE='SPS'
    T_max=T_SPS

if SEP_SM_FLAG==1 and SEP_HTSD_FLAG==0:
    m_craft = m_CM + m_htsd  # Start with CM, heatshield attached
    CURRENT_ENGINE='NONE'
    T_max=0.

if SEP_HTSD_FLAG==1:
    m_craft = m_CM           # Start with CM, heatshield jettisoned
    CURRENT_ENGINE='NONE'
    T_max=0.


# Calculate initial radial coordinates
# ------------------------------------
r_E0=     math.sqrt((x_0-x_Earth)**2 + (y_0-y_Earth)**2 + (z_0-z_Earth)**2) # dist to Earth center
phi_E0=   math.atan2((y_0-y_Earth),(x_0-x_Earth))        # Earth phi of ground track
theta_E0= math.acos((z_0-z_Earth)/r_E0)                  # Earth theta of ground track

r_M0=     math.sqrt((x_0-x_Moon)**2 + (y_0-y_Moon)**2 + (z_0-z_Moon)**2)   # dist to Moon center
phi_M0=   math.atan2((y_0-y_Moon),(x_0-x_Moon))          # Moon phi of ground track
theta_M0= math.acos((z_0-z_Moon)/r_M0)                   # Moon theta of ground track
h_M0=     r_M0 - R_Moon
if r_M0<R_SOI_Moon:   # Spacecraft is within Moon's Sphere of Influence
    MOON_FLAG=1
else:
    MOON_FLAG=0

if MOON_FLAG:
    h_current0=h_M0
else:
    h_current0=h_E0

# +-----------------------------------+
# | Calculate initial attitude matrix |
# +-----------------------------------+
if MOON_FLAG:    # If orbiting Moon
    v_rel=np.array([v_0x,v_0y,v_0z])
    r_rel=np.array([x_0-x_Moon,y_0-y_Moon,z_0-z_Moon])
else:            # If orbiting Earth
    v_rel=np.array([v_0x,v_0y,v_0z])
    r_rel=np.array([x_0-x_Earth,y_0-y_Earth,z_0-z_Earth])
v_rel_mag=math.sqrt((v_rel[0]**2)+(v_rel[1]**2)+(v_rel[2]**2))
r_rel_mag=math.sqrt((r_rel[0]**2)+(r_rel[1]**2)+(r_rel[2]**2))

# If not using PROGRADE/RETROGRADE switch,
# then attitude can be specified more generally
# with Euler angles or Roll, Pitch, Yaw.
#
# Set commanded attitude here:
# 
# 

# If using PROGRADE/RETROGRADE switch:
if v_rel_mag > 0.:
    x_b=PROGRADE_FLAG*v_rel/v_rel_mag  # x_b points in/against direction of motion
    y_b=-1.*PROGRADE_FLAG*HEADUP_FLAG*np.cross((r_rel/r_rel_mag),(v_rel/v_rel_mag))
    z_b=np.cross(x_b,y_b)
if v_rel_mag == 0.:
    print r_rel,v_rel
    x_b=np.array([1.,0.,0.])            # *** Arbitrary initial value, but find a better way to handle this
    y_b=np.array([0.,1.,0.])
    z_b=np.array([0.,0.,1.])

if USE_LAUNCH:  # *** Calculate initial attitude for any given lon,lat
    x_b=(r_rel/r_rel_mag)        # x_b points vertically (align with radial vector from Earth)
    z_b=np.cross(x_b,[0.,0.,1.]) # z_b points West  (radius cross North axis)
    y_b=np.cross(z_b,x_b)        # y_b points North (completes the RH system)
att_matrix=np.array([[x_b],[y_b],[z_b]])  # *** Check order of rows/cols

# Calculate Roll, Pitch, Yaw angles from current REFSMMAT here:
# *** TO DO ***
# 



# +------------------------+
# | Display permanent text |
# +------------------------+
win.addstr(0,10,"Apollo 8",curses.A_BOLD)
win.addstr(DSKY_U-2,DSKY_L-2,"APOLLO GUIDANCE COMPUTER",curses.A_BOLD)
win.addstr(DSKY_U+2,DSKY_L+12,"PRO",curses.A_BOLD)
win.addstr(DSKY_U+5,DSKY_L,"VERB",curses.A_BOLD)
win.addstr(DSKY_U+5,DSKY_L+12,"NOUN",curses.A_BOLD)
win.addstr(DSKY_U+8,DSKY_L+2,"R1",curses.A_BOLD)
win.addstr(DSKY_U+9,DSKY_L+2,"R2",curses.A_BOLD)
win.addstr(DSKY_U+10,DSKY_L+2,"R3",curses.A_BOLD)
win.addstr(DSKY_U+13,DSKY_L,"DELTA-V REMAINING",curses.A_BOLD)
# Draw boxes around sections of text
curses.textpad.rectangle(win,DSKY_U+1,DSKY_L+8,DSKY_U+3,DSKY_L+11)  # PROGRAM
curses.textpad.rectangle(win,DSKY_U+4,DSKY_L+4,DSKY_U+6,DSKY_L+7)   # VERB
curses.textpad.rectangle(win,DSKY_U+4,DSKY_L+8,DSKY_U+6,DSKY_L+11)  # NOUN
curses.textpad.rectangle(win,DSKY_U+7,DSKY_L+4,DSKY_U+11,DSKY_L+11) # Registers
win.refresh()

def met_timer_cb():
    global met
    met = int((t_hr * 60 * 60) + (t_min * 60) + t_sec)

    for net_id in ASTRONAUT_NET_ID_LIST: protocol.transmit_command(net_id, 0, [met])
    for net_id in FDO_NET_ID_LIST: protocol.transmit_command(net_id, 0, [met])
    for net_id in GUIDO_NET_ID_LIST: protocol.transmit_command(net_id, 0, [met])
    for net_id in GNC_NET_ID_LIST: protocol.transmit_command(net_id, 0, [met])
    for net_id in CAPCOM_NET_ID_LIST: protocol.transmit_command(net_id, 0, [met])
    for net_id in BOOSTER_NET_ID_LIST: protocol.transmit_command(net_id, 0, [met])
    for net_id in RETRO_NET_ID_LIST: protocol.transmit_command(net_id, 0, [met])

def led_timer_cb():
    set_leds(ABORT_REQUEST_FLAG, ENGINE_FLAG)

def station_sync_timer_cb():
    set_position(0, [x_Earth, y_Earth, z_Earth], [
        [[1, 0, 0]],
        [[0, 1, 0]],
        [[0, 0, -1]]
    ])
    set_position(1, [x_Moon, y_Moon, z_Moon], [
        [[1, 0, 0]],
        [[0, 1, 0]],
        [[0, 0, -1]]
    ])
    set_position(2, [x, y, z], att_matrix)
    if agc_verb == 0:
        nan = float('nan')
        set_agc_values(agc_prog, agc_verb, agc_noun, nan, nan, nan, delta_v_rm)
    else:
        set_agc_values(agc_prog, agc_verb, agc_noun, agc_r1, agc_r2, agc_r3, delta_v_rm)

    for net_id in FDO_NET_ID_LIST:
        if ION_BLACKOUT_FLAG==0 and MOON_BLACKOUT_FLAG==0 and GROUNDSTATION_FLAG==1:
            protocol.transmit_command(net_id, 1, [x, y, z])
            protocol.transmit_command(net_id, 2, [v_x, v_y, v_z])
            protocol.transmit_command(net_id, 3, [r_E])
            protocol.transmit_command(net_id, 4, [h_E_km])
            protocol.transmit_command(net_id, 5, [r_M])
            protocol.transmit_command(net_id, 6, [h_M_km])
            protocol.transmit_command(net_id, 7, [v])
            protocol.transmit_command(net_id, 8, [m_craft])
            protocol.transmit_command(net_id, 9, [T_max])
            protocol.transmit_command(net_id, 10, [T])
            protocol.transmit_command(net_id, 11, [MOON_FLAG])
            protocol.transmit_command(net_id, 12, [ABORT_REQUEST_FLAG])

    for net_id in GUIDO_NET_ID_LIST:
        if ION_BLACKOUT_FLAG==0 and MOON_BLACKOUT_FLAG==0 and GROUNDSTATION_FLAG==1:
            protocol.transmit_command(net_id, 1, [ecc])
            protocol.transmit_command(net_id, 2, [peri_alt])
            protocol.transmit_command(net_id, 3, [apo_alt])
            protocol.transmit_command(net_id, 4, [MOON_FLAG])
            protocol.transmit_command(net_id, 5, [PROGRADE_FLAG])
            protocol.transmit_command(net_id, 6, [HEADUP_FLAG])
            protocol.transmit_command(net_id, 7, [ATT_HOLD_FLAG])

    for net_id in GNC_NET_ID_LIST:
        if ION_BLACKOUT_FLAG==0 and MOON_BLACKOUT_FLAG==0 and GROUNDSTATION_FLAG==1:
            protocol.transmit_command(net_id, 1, [ABORT_REQUEST_FLAG])
            protocol.transmit_command(net_id, 2, [ABORT_FLAG])
            protocol.transmit_command(net_id, 3, [ENGINE_FLAG])
            protocol.transmit_command(net_id, 4, [SEP_SIVB_FLAG])
            protocol.transmit_command(net_id, 5, [SEP_SM_FLAG])
            protocol.transmit_command(net_id, 6, [CHUTE_DROGUE_FLAG])
            protocol.transmit_command(net_id, 7, [CHUTE_MAIN_FLAG])
            protocol.transmit_command(net_id, 8, [PROGRADE_FLAG])
            protocol.transmit_command(net_id, 9, [HEADUP_FLAG])
            protocol.transmit_command(net_id, 10, [ATT_HOLD_FLAG])

    for net_id in BOOSTER_NET_ID_LIST:
        if ION_BLACKOUT_FLAG==0 and MOON_BLACKOUT_FLAG==0 and GROUNDSTATION_FLAG==1:
            protocol.transmit_command(net_id, 1, [m_craft])
            protocol.transmit_command(net_id, 2, [T])
            protocol.transmit_command(net_id, 3, [SIC_F1_1_FLAG])
            protocol.transmit_command(net_id, 4, [SIC_F1_2_FLAG])
            protocol.transmit_command(net_id, 5, [SIC_F1_3_FLAG])
            protocol.transmit_command(net_id, 6, [SIC_F1_4_FLAG])
            protocol.transmit_command(net_id, 7, [SIC_F1_5_FLAG])
            protocol.transmit_command(net_id, 8, [SEP_SIC_FLAG])
            protocol.transmit_command(net_id, 9, [SII_J2_1_FLAG])
            protocol.transmit_command(net_id, 10, [SII_J2_2_FLAG])
            protocol.transmit_command(net_id, 11, [SII_J2_3_FLAG])
            protocol.transmit_command(net_id, 12, [SII_J2_4_FLAG])
            protocol.transmit_command(net_id, 13, [SII_J2_5_FLAG])
            protocol.transmit_command(net_id, 15, [SEP_SII_FLAG])
            protocol.transmit_command(net_id, 16, [SIVB_J2_1_FLAG])
            protocol.transmit_command(net_id, 17, [SEP_SIVB_FLAG])

    for net_id in RETRO_NET_ID_LIST:
        if ION_BLACKOUT_FLAG==0 and MOON_BLACKOUT_FLAG==0 and GROUNDSTATION_FLAG==1:
            protocol.transmit_command(net_id, 1, [h_E_km])
            protocol.transmit_command(net_id, 2, [m_craft])
            protocol.transmit_command(net_id, 3, [v])
            protocol.transmit_command(net_id, 4, [T])
            protocol.transmit_command(net_id, 5, [MOON_FLAG])
            protocol.transmit_command(net_id, 6, [ecc])
            protocol.transmit_command(net_id, 7, [peri_alt])
            protocol.transmit_command(net_id, 8, [apo_alt])
            protocol.transmit_command(net_id, 9, [ABORT_REQUEST_FLAG])
            protocol.transmit_command(net_id, 10, [ABORT_FLAG])
            protocol.transmit_command(net_id, 11, [ENGINE_FLAG])
            protocol.transmit_command(net_id, 12, [SEP_SM_FLAG])
            protocol.transmit_command(net_id, 13, [CHUTE_DROGUE_FLAG])
            protocol.transmit_command(net_id, 15, [CHUTE_MAIN_FLAG])
            protocol.transmit_command(net_id, 16, [PROGRADE_FLAG])
            protocol.transmit_command(net_id, 17, [HEADUP_FLAG])

met_timer.add_callback(met_timer_cb)
led_timer.add_callback(led_timer_cb)
station_sync_timer.add_callback(station_sync_timer_cb)

met_timer.start()
led_timer.start()
station_sync_timer.start()

# ***** THIS IS THE MAIN SIMULATION LOOP *****
# Set up an infinite loop; break with KEY_QUIT
# ********************************************
while 1:
    if NETWORKED: 
        protocol.update()
        met_timer.update()
        led_timer.update()
        station_sync_timer.update()

    time_loop_init=datetime.datetime.now()  # For timing the main loop

    # Current simulation time
    if t<0:            # For countdown, do modulo division on abs value
        t_disp=abs(t)  # do modulo division on abs value
        T_SIGN_FLAG=-1 # & multiply hrs*-1 afterward
        COUNTDOWN_FLAG=1
    else:
        t_disp=t       # Normal clock
        T_SIGN_FLAG=1
        COUNTDOWN_FLAG=0
    if t<t_orbit_insertion and USE_LAUNCH:  # Launch program ends at this time
        LAUNCH_FLAG=1
    else:
        LAUNCH_FLAG=0


    t_min,t_sec=divmod(t_disp,60) # calculates time in hr, min, sec
    t_hr, t_min=divmod(t_min,60)  # using modulo
    t_hr=T_SIGN_FLAG*t_hr         # Put time sign on hours

    t_in_min=t/60.       # total min         
    t_in_hr=t_in_min/60. # total hrs
    t_in_day=t_in_hr/24. # total days

    if DEBUG:      # If DEBUG, break immediately from loop and print variables
        break


    # +----------------------------+
    # | Check for landing or crash |
    # +----------------------------+
    if h_E0<=0. and LAUNCH_FLAG==0: # Touch Earth but not sitting on the pad
        if v_0<SAFE_EARTH: # Safe landing speed
            LAND_E=1
        else:
            CRASH_E=1
        break             # Go to finishing routines

    if h_M0<=0.:          # Touch Moon
        if v_0<SAFE_MOON: # Safe landing speed
            LAND_M=1
        else:
            CRASH_M=1
        break             # Go to finishing routines







    # Check for keyboard interaction
    # (Some keys disabled when other stations online)
    # -----------------------------------------------
    keypress=win.getch()
    if keypress == ord(KEY_QUIT):  # QUIT simulation
        win.addstr(1,35,"QUITTING", curses.A_BOLD | curses.color_pair(2))
        win.refresh()
        time.sleep(1)
        break # Exit the while loop
    if keypress == ord(KEY_ENGINE):  # ENGINE toggle on/off
        if ENGINE_FLAG:              # If engine already on
            ENGINE_FLAG=0            # ...turn it off
            #T=0
            time.sleep(0.5)          # Pause for time to remove finger from key
        else:                        # If engine already off
            ENGINE_FLAG=1            # ...turn it on
            #T=T_max
            time.sleep(0.5)          # Pause for time to remove finger from key
    if keypress == ord(KEY_FAIL_SII_1):  # S-II Engine #1 fails
        FLAG_FAIL_SII_1=1
    if keypress == ord(KEY_FAIL_SII_2):  # S-II Engine #1 & 2 fail
        FLAG_FAIL_SII_2=1
    if keypress == ord(KEY_SPEED_INC):# Increase simulation speed by 2x
        delay=delay/2.
    if keypress == ord(KEY_SPEED_DEC):# Decrease simulation speed by 2x
        delay=delay*2.
    if keypress == ord(KEY_PROGRADE):   # Set attitude prograde
        PROGRADE_FLAG=1
    if keypress == ord(KEY_RETROGRADE): # Set attitude retrograde
        PROGRADE_FLAG=-1
    if keypress == ord(KEY_HEADUP):      # Set attitude head-up
        HEADUP_FLAG=1
    if keypress == ord(KEY_HEADDOWN):      # Set attitude head-down
        HEADUP_FLAG=-1
    if keypress == ord(KEY_PAUSE):      # Pause simulation
        win.addstr(3,10,"                                 ")
        win.refresh()
        win.addstr(3,10,"PAUSED  Press any key to continue",curses.A_BOLD)
        win.refresh()
        while win.getch()==-1:        # Wait for any keypress
            time.sleep(1)             # Wait 1 sec before checking again
        win.addstr(3,10,"                                 ")
    # ------------
    # |  EDITOR  |
    # ------------
    if keypress == ord(KEY_EDITOR):   # Enter detailed variable information
        win.timeout(-1)               # Set to blocking--wait for entry
        instring=win.getstr()         # Accept a string, terminated by <enter>
        win.addstr(2,10,instring)     # Print the string
        win.refresh()
        time.sleep(2)                 # 2 sec delay before continuing


        #if instring.split()[0]=="attitude":      # FORMAT: attitude #att_x #att_y #att_z
        #    if len(instring.split())==4:
        #        try:
        #            att_x=float(instring.split()[1])     # Components must be normalized
        #            att_y=float(instring.split()[2])     # Eventually add syntax checking
        #            att_z=float(instring.split()[3])     # Might add normalization check
        #            FLAG_ATTITUDE='m'                    # Set attitude flag to manual
        #        except:
        #            win.addstr(3,10,"ERROR",curses.A_BOLD) # If not floating point
        #            win.refresh()
        #            time.sleep(2)
        #            win.addstr(3,10,"                                 ")
        #            win.refresh()
        #    else:
        #            win.addstr(3,10,"ERROR",curses.A_BOLD) # If not three parameters
        #            win.refresh()
        #            time.sleep(2)
        #            win.addstr(3,10,"                                 ")
        #            win.refresh()


        if instring.split()[0]=="dv":            # FORMAT: dv #deltav
            if len(instring.split())==2:
                try:
                    delta_v_set=float(instring.split()[1])   # Set target delta-v (m/s)
                    v_target   = v_0+delta_v_set               # Set target velocity
                except:
                    win.addstr(3,10,"ERROR",curses.A_BOLD) # If not floating point
                    win.refresh()
                    time.sleep(2)
                    win.addstr(3,10,"                                 ")
                    win.refresh()
            else:
                win.addstr(3,10,"ERROR",curses.A_BOLD) # If not 1 parameter
                win.refresh()
                time.sleep(2)
                win.addstr(3,10,"                                 ")
                win.refresh()

        if instring.split()[0]=='staging':       # FORMAT: staging {sivb,sm}
            if len(instring.split())==2:
                try:
                    if instring.split()[1]=='sivb':
                        SEP_SIVB_FLAG=1
                    if instring.split()[1]=='sm':
                        SEP_SM_FLAG=1
                except:
                    win.addstr(3,10,"ERROR",curses.A_BOLD) # If not recognized term
                    win.refresh()
                    time.sleep(2)
                    win.addstr(3,10,"                                 ")
                    win.refresh()
            else:
                win.addstr(3,10,"ERROR",curses.A_BOLD) # If not 1 parameter
                win.refresh()
                time.sleep(2)
                win.addstr(3,10,"                                 ")
                win.refresh()


        if instring.split()[0]=="mass":          # FORMAT: mass #mass
            if len(instring.split())==2:
                try:
                    m_craft=float(instring.split()[1])   # Set spacecraft mass (kg)
                except:
                    win.addstr(3,10,"ERROR",curses.A_BOLD) # If not floating point
                    win.refresh()
                    time.sleep(2)
                    win.addstr(3,10,"                                 ")
                    win.refresh()
            else:
                win.addstr(3,10,"ERROR",curses.A_BOLD) # If not 1 parameter
                win.refresh()
                time.sleep(2)
                win.addstr(3,10,"                                 ")
                win.refresh()


        if instring.split()[0]=="thrust":        # FORMAT: thrust #thrust
            if len(instring.split())==2:
                try:
                    T_max=float(instring.split()[1])     # Set engine thrust (N)
                except:
                    win.addstr(3,10,"ERROR",curses.A_BOLD) # If not floating point
                    win.refresh()
                    time.sleep(2)
                    win.addstr(3,10,"                                 ")
                    win.refresh()
            else:
                win.addstr(3,10,"ERROR",curses.A_BOLD) # If not 1 parameter
                win.refresh()
                time.sleep(2)
                win.addstr(3,10,"                                 ")
                win.refresh()

        #if instring.split()[0]=="center":        # FORMAT: center body
        #    if len(instring.split())==2:
        #        ORBIT_BODY=instring.split()[1]       # body="earth" or "moon"
        #        # *** Replace with MOON_FLAG ***
        #
        #    else:
        #        win.addstr(3,10,"ERROR",curses.A_BOLD) # If not 1 parameter
        #        win.refresh()
        #        time.sleep(2)
        #        win.addstr(3,10,"                                 ")
        #        win.refresh()

        if instring.split()[0]=="dt":            # FORMAT: dt #dt
            if len(instring.split())==2:
                try:
                    dt=float(instring.split()[1])        # Set simulation timesep (sec)
                except:
                    win.addstr(3,10,"ERROR",curses.A_BOLD) # If not floating point
                    win.refresh()
                    time.sleep(2)
                    win.addstr(3,10,"                                 ")
                    win.refresh()
            else:
                win.addstr(3,10,"ERROR",curses.A_BOLD) # If not 1 parameter
                win.refresh()
                time.sleep(2)
                win.addstr(3,10,"                                 ")
                win.refresh()

        win.addstr(2,10,"                                 ")
        win.refresh()
        win.timeout(10)               # Return to original setting
    # -------------------
    # |  END OF EDITOR  |
    # -------------------

    if keypress == ord(KEY_CHUTES) and USE_PANEL==0:
        # Parachute status: 0=no chutes, 1=drogues, 2=main
        if CHUTE_STATUS==2:
            CHUTE_STATUS=2  # Don't change anything if main is already deployed.
        if CHUTE_STATUS==1:
            CHUTE_STATUS=2  # Deploy main
            t_chute=0       # reset timer for unreefing
        if CHUTE_STATUS==0:
            CHUTE_STATUS=1  # Deploy drogues
            t_chute=0       # reset timer for unreefing
    if keypress == ord(KEY_SHOWPLOT):

        # Orbital Plot (x-y plane):
        if EARTH_MOON:
            P.xlim(-4.e5,8.e3) # Use when going to Moon
            P.ylim(-8.e4,8.e4)
        else:
            P.xlim(-8.e3,8.e3)  # Use for Earth orbit
            P.ylim(-8.e3,8.e3)
        # Draw Earth (x-y plane):
        theta_circle=range(361)
        earth_x=[((R_Earth/1000.)*math.cos(theta_temp*(math.pi/180.))+(x_Earth/1000.)) for theta_temp in theta_circle]
        earth_y=[((R_Earth/1000.)*math.sin(theta_temp*(math.pi/180.))+(y_Earth/1000.)) for theta_temp in theta_circle]
        P.plot(earth_x,earth_y,color='green')
        # Draw Moon (x-y plane):
        moon_x=[((R_Moon/1000.)*math.cos(theta_temp*(math.pi/180.))+(x_Moon/1000.)) for theta_temp in theta_circle]
        moon_y=[((R_Moon/1000.)*math.sin(theta_temp*(math.pi/180.))+(y_Moon/1000.)) for theta_temp in theta_circle]
        P.plot(moon_x,moon_y,color='gray')
        # Plot orbit (x-y plane):
        P.plot(x_km_list,y_km_list)
        P.plot([0],[0],'ro')        # Mark North Pole with red circle
        P.plot([x_0/1000.],[y_0/1000.],'rx')  # Mark current position with red x
        P.xlabel('x (km)')
        P.ylabel('y (km)')
        P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
        #P.show(block=False)  # block=False maintains program flow w/o having to close plot window
        P.show()


        # Plot altitudes
        P.clf()
        if MOON_FLAG:
            #P.semilogy(t_min_list,h_M_km_list)
            P.plot(t_min_list,h_M_km_list)
        else:
            #P.semilogy(t_min_list,h_E_km_list)
            P.plot(t_min_list,h_E_km_list)
        P.xlabel('t (min)')
        P.ylabel('Altitude, h (km)')
        P.show()

        # Plot velocity
        P.clf()
        P.plot(t_min_list,v_list)
        P.xlabel('t (min)')
        P.ylabel('v (m/s)')
        P.show()

        # Plot h-dot
        P.clf()
        P.ylim(-300,1)
        P.plot(t_min_list,h_dot_list)
        P.xlabel('t (min)')
        P.ylabel('h-dot (m/s)')
        P.show()

        # Plot g-forces
        P.clf()
        P.plot(t_min_list,g_sub_list)
        P.xlabel('t (min)')
        P.ylabel('Acceleration (g)')
        P.show()

        # Plot KE of relative wind
        P.clf()
        P.plot(t_min_list,KE_wind_list)
        P.xlabel('t (min)')
        P.ylabel('KE/volume of relative wind (J/m^-3)')
        P.show()

        # Plot altitude vs. velocity
        P.clf()
        P.plot(v_list,h_E_km_list)
        P.xlabel('v (m/s)')
        P.ylabel('Altitude (km)')
        P.show()

    if keypress == ord(KEY_SAVEDATA):  # Save data file with current spacecraft state
        now=datetime.datetime.now()
        outfilesuffix=str(now).split()[0]+'-'+str(now).split()[1]
        outfilename=outfilerootname+outfilesuffix

        outfile=open(outfilename, 'w')
        #outfile.write('m_craft       '+str(m_craft)+'\n')
        #outfile.write('T_max         '+str(T_max)+'\n')
        #outfile.write('ENGINE_FLAG   '+str(ENGINE_FLAG)+'\n')
        #outfile.write('CHUTE_STATUS   '+str(CHUTE_STATUS)+'\n')
        #outfile.write('t_chute       '+str(t_chute)+'\n')
        #outfile.write('T             '+str(T)+'\n')
        #outfile.write('MOON_FLAG     '+str(MOON_FLAG)+'\n')
        outfile.write('t_hr          '+str(t_hr)+'\n')
        outfile.write('t_min         '+str(t_min)+'\n')
        outfile.write('t_sec         '+str(t_sec)+'\n')
        outfile.write('INIT_RADIAL   0\n')   # Setting INIT_RADIAL=0 for now.  Use cartesian.
        outfile.write('USE_LAUNCH    '+str(USE_LAUNCH)+'\n')
        outfile.write('SEP_SIVB_FLAG '+str(SEP_SIVB_FLAG)+'\n')
        outfile.write('SEP_SM_FLAG   '+str(SEP_SM_FLAG)+'\n')
        outfile.write('PROGRADE_FLAG '+str(PROGRADE_FLAG)+'\n')
        outfile.write('HEADUP_FLAG   '+str(HEADUP_FLAG)+'\n')
        outfile.write('agc_verb      '+str(agc_verb)+'\n')
        outfile.write('agc_noun      '+str(agc_noun)+'\n')
        outfile.write('t             '+str(t)+'\n')
        outfile.write('x_0           '+str(x_0)+'\n')
        outfile.write('y_0           '+str(y_0)+'\n')
        outfile.write('z_0           '+str(z_0)+'\n')
        outfile.write('v_0x          '+str(v_0x)+'\n')
        outfile.write('v_0y          '+str(v_0y)+'\n')
        outfile.write('v_0z          '+str(v_0z)+'\n')
        outfile.close()

    if keypress == ord(KEY_VERB):  # VERB entry
        win.addstr(DSKY_U+5,DSKY_L+5,"  ")
        win.timeout(-1)
        s = win.getstr()
        win.timeout(10)
        try:
            agc_verb = int(s) # Force it to be an integer
        except:
            s=0
            agc_verb = s      # If you don't type an integer, it sets to 0
        if agc_verb==6:
            AGC_V06_FLAG=0    # V6 requires immediate assignment of variables
                              # the first cycle V6 is entered, so set flag=0.
        win.addstr(DSKY_U+5,DSKY_L+5,str(agc_verb), curses.color_pair(1))
        win.refresh()
    if keypress == ord(KEY_NOUN):  # NOUN entry
        win.addstr(DSKY_U+5,DSKY_L+9,"  ")
        win.timeout(-1)
        s = win.getstr()
        win.timeout(10)
        try:
            agc_noun = int(s) # Force it to be an integer
        except:
            s=0
            agc_noun = s      # If you don't type an integer, it sets to 0
        win.addstr(DSKY_U+5,DSKY_L+9,str(agc_noun), curses.color_pair(1))
        win.refresh()
    # ------------------------------

    # Velocity
    v_0=math.sqrt(v_0x**2 + v_0y**2 + v_0z**2)     # Set initial v for this iteration
    delta_v_rm = v_target-v_0                      # Remaining delta-v to target v


    # +---------------------------+
    # | Calculate attitude matrix |
    # +---------------------------+
    if MOON_FLAG:    # If in Moon's SOI
        v_rel=np.array([v_0x,v_0y,v_0z])
        r_rel=np.array([x_0-x_Moon,y_0-y_Moon,z_0-z_Moon])
    else:            # If in Earth's SOI
        v_rel=np.array([v_0x,v_0y,v_0z])
        r_rel=np.array([x_0-x_Earth,y_0-y_Earth,z_0-z_Earth])
    v_rel_mag=math.sqrt((v_rel[0]**2)+(v_rel[1]**2)+(v_rel[2]**2))
    r_rel_mag=math.sqrt((r_rel[0]**2)+(r_rel[1]**2)+(r_rel[2]**2))

    # If not using PROGRADE/RETROGRADE switch,
    # then attitude can be specified more generally
    # with Euler angles or Roll, Pitch, Yaw.
    #
    # Set commanded attitude here:
    # *** TO DO ***
    # 

    if r_rel_mag>0. and v_rel_mag > 0. and ATT_HOLD_FLAG != 1: # if I CAN do cross product & am not in ATT HOLD...
        r_cross_v=np.cross((r_rel/r_rel_mag),(v_rel/v_rel_mag))
        r_cross_v_mag=np.linalg.norm(r_cross_v)
        if r_cross_v_mag>0.:                                   # if v not parallel/antiparallel to r
            x_b=PROGRADE_FLAG*v_rel/v_rel_mag                  # x_b points in/against direction of motion
            y_b=-1.*PROGRADE_FLAG*HEADUP_FLAG*r_cross_v
            z_b=np.cross(x_b,y_b)
    # if v_rel_mag == 0. or ATT_HOLD_FLAG=1 or v parallel/antiparallel to r, don't update axis directions.

    att_matrix=np.array([[x_b],[y_b],[z_b]])  # *** Check order of rows/cols
    #win.addstr(4,10,"x_b "+str(x_b[0])+" "+str(x_b[1])+" "+str(x_b[2]))
    #win.refresh()
    att_x=x_b[0]  # This is redundant.  I can just use x_b components directly.
    att_y=x_b[1]
    att_z=x_b[2]

    # Calculate Roll, Pitch, Yaw angles:
    # *** TO DO ***
    # 
    # Calculate flight path pitch relative to Earth/Moon horizon
    # Can be displayed during launch to watch Saturn V pitch over
    if v_rel_mag>0. and r_rel_mag>0.:
        v_hat=v_rel/v_rel_mag
        r_hat=r_rel/r_rel_mag
        v_dot_r=np.dot(v_hat,r_hat)
        if v_dot_r>1.:
            v_dot_r=1.  # This avoids a domain error in next step
        beta=math.acos(v_dot_r)
        beta_deg=beta*180./math.pi
        pitch_E_deg=90.-beta_deg
        #pitch_E_deg=90.-(math.acos(np.dot((v_rel/v_rel_mag),(r_rel/r_rel_mag)))*180./math.pi)
    else:
        x_dot_r=np.dot(x_b,(r_rel/r_rel_mag))
        if x_dot_r>1.:
            x_dot_r=1.  # This avoids a domain error in next step
        pitch_E_deg=90.-(math.acos(x_dot_r)*180./math.pi)

    # Gravity
    # -------
    # Earth Gravity, F_gE
    # For components, see notebook, p. 14
    if (h_E0 <= 0.) or (h_M0 <= 0.) or (COUNTDOWN_FLAG==1):  # On surface of Earth or Moon, don't calculate gravity
        F_gE=0.
        F_gM=0.
    else:
        F_gE=-1.*G*M_Earth*m_craft/(r_E0**2) # Earth gravity magnitude (N) (note sign)
        F_gM=-1.*G*M_Moon*m_craft/(r_M0**2)  # Moon gravity magnitude (N) (note sign)
    # Earth gravity, F_gE
    F_gEx=F_gE*math.sin(theta_E0)*math.cos(phi_E0)
    F_gEy=F_gE*math.sin(theta_E0)*math.sin(phi_E0)
    F_gEz=F_gE*math.cos(theta_E0)
    # Moon gravity, F_gM
    F_gMx=F_gM*math.sin(theta_M0)*math.cos(phi_M0)
    F_gMy=F_gM*math.sin(theta_M0)*math.sin(phi_M0)
    F_gMz=F_gM*math.cos(theta_M0)

    if not LAUNCH_FLAG:   # If not in a countdown or launch, calculate dynamics

        # Mass and engine settings
        if SEP_SIVB_FLAG==0:
            #m_craft = m_A8_TLI        # S-IVB & SM attached. ***Why is this so much less than the total below?
            m_craft = m_SIVB + m_SM + m_SM_SPS_fuel + m_CM + m_htsd  # *dry* S-IVB, SM, & CM
            CURRENT_ENGINE='SIVB'
            T_max=T_SIVB

        if SEP_SIVB_FLAG==1 and SEP_SM_FLAG==0:
            m_craft = m_SM + m_SM_SPS_fuel + m_CM + m_htsd  # SM & CM
            CURRENT_ENGINE='SPS'
            T_max=T_SPS

        if SEP_SM_FLAG==1 and SEP_HTSD_FLAG==0:
            m_craft = m_CM + m_htsd  # CM, heatshield attached
            CURRENT_ENGINE='NONE'
            T_max=0.

        if SEP_HTSD_FLAG==1:
            m_craft = m_CM           # CM, heatshield jettisoned
            CURRENT_ENGINE='NONE'
            T_max=0.

        if agc_prog==20:             # AGC Program 20 switches thrust to SM's RCS
            CURRENT_ENGINE='RCS'
            T_max=T_SM_RCS

        # +----------------+
        # | Begin Dynamics |
        # +----------------+

        # ---------------------------
        if ORBIT_INSERTION_FLAG:   # First iteration after orbital insertion, set position & velocity
            ORBIT_INSERTION_FLAG=0 # Turn flag off.
            v_0=7793.1
            v_0x= -1.*v_0*math.sin(phi_E0)
            v_0y=     v_0*math.cos(phi_E0)
            v_0z= 0.
            ENGINE_FLAG=0

        # +----------------------------+
        # | Calculate force components |
        # +----------------------------+
        # Gravity done above.

        # Engine thrust
        # -------------
        if ENGINE_FLAG:
            T=T_max
        else:
            T=0.

        T_x=T*att_x
        T_y=T*att_y
        T_z=T*att_z

        # Air resistance
        # --------------
        if h_E0 >= h_max_atm:                    # No lift or drag above 86 km
            D=0.
            L=0.
        if h_E0  < h_max_atm:                    # If altitude below 86 km
            # Calculate CM drag coefficient C_D_CM
            # from http://exrocketman.blogspot.com/2012/08/blunt-capsule-drag-data.html
            if v_0<v_sound:  # subsonic
                C_D_CM=C_D_CM_sub
            if v_0>=v_sound: # supersonic
                C_D_CM=C_D_CM_super  # average C_D for supersonic
            # Calculate air density, rho
            #rho=1.28  # kg/m^3  at sea level only!
            if h_E0 >= 0. and h_E0 < 11000.:
                h_b  =0.
                rho_b=1.2250  # kg/m^3
                T_air=288.15  # K
                L_air=-0.0065 # K/m
            if h_E0 >= 11000. and h_E0 < 20000.:
                h_b  =11000.
                rho_b=0.36391
                T_air=216.65
                L_air=0.0
            if h_E0 >= 20000. and h_E0 < 32000.:
                h_b  =20000.
                rho_b=0.08803
                T_air=216.65
                L_air=0.001
            if h_E0 >= 32000. and h_E0 < 47000.:
                h_b  =32000.
                rho_b=0.01322
                T_air=228.65
                L_air=0.0028
            if h_E0 >= 47000. and h_E0 < 51000.:
                h_b  =47000.
                rho_b=0.00143
                T_air=270.65
                L_air=0.0
            if h_E0 >= 51000. and h_E0 < 71000.:
                h_b  =51000.
                rho_b=0.00086
                T_air=270.65
                L_air=-0.0028
            if h_E0 >= 71000. and h_E0 < h_max_atm:
                h_b  =71000.
                rho_b=0.000064
                T_air=214.65
                L_air=-0.002

            if L_air != 0:  # Atmospheric Lapse rate != 0
                rho = rho_b*(T_air/(T_air + L_air*(h_E0-h_b)))**(1. + (g_E*M_air)/(R_gas*L_air))
            else:           # Atmospheric Lapse rate  = 0
                rho = rho_b*math.exp((-1.*g_E*M_air*(h_E0-h_b))/(R_gas*T_air))

            # Calculate kinetic energy of relative wind, for determining ionization blackout
            # *** Use speed relative to Earth surface, incl. rotation effects when that's modeled
            KE_wind     = 0.5*rho*(v_0**2)  # KE/vol of relative wind
            if (KE_wind>ionization_KE) and (v_0>v_sound):       # Ionization blackout?
                ION_BLACKOUT_FLAG=1
            else:
                ION_BLACKOUT_FLAG=0

            # Figure out how to handle spacecraft breakup on reentry.  
            # The following is triggered during launch!
            #if (KE_wind>KE_breakup) and (v_0>v_sound) and (PROGRADE_FLAG==1):
            #    BREAKUP_FLAG=1
            #    break

            if CHUTE_STATUS==1:
                # Drogue chute
                t_chute=t_chute+dt
                chute_fraction=t_chute/t_chute_max
                D_chute= fudge_chute1 * (0.5*C_D_drogue*rho*A_drogue*(v_0**2)) * chute_fraction
            if CHUTE_STATUS==2:
                # Main chute
                t_chute=t_chute+dt
                chute_fraction=t_chute/t_chute_max
                D_chute= fudge_chute2 * (0.5*C_D_main*rho*A_main*(v_0**2)) * chute_fraction

            D_CM=0.5*C_D_CM*rho*A_CM*(v_0**2)   # Drag formula
            L=L_D*D_CM                          # Lift
            D=D_chute+D_CM
        if v_0 != 0.:         # To avoid division by 0.
            D_x=-1*D*v_0x/v_0
            D_y=-1*D*v_0y/v_0
            D_z=-1*D*v_0z/v_0
        else:
            D_x=0.
            D_y=0.
            D_z=0.

        # Lift vector points along CM's z_b axis:
        L_x=L*z_b[0]
        L_y=L*z_b[1]
        L_z=L*z_b[2]

        # Subtotal of forces *excluding* gravity
        # Use for calculating g forces relative to free-fall
        F_sub_x=T_x + D_x + L_x
        F_sub_y=T_y + D_y + L_y
        F_sub_z=T_z + D_z + L_z
        F_sub=math.sqrt(F_sub_x**2 + F_sub_y**2 + F_sub_z**2)
        a_sub=F_sub/m_craft
        g_sub=a_sub/g_E      # Felt acceleration in units of g's


        F_x=F_gEx + F_gMx + T_x + D_x + L_x   # Sum all forces
        F_y=F_gEy + F_gMy + T_y + D_y + L_y
        F_z=F_gEz + F_gMz + T_z + D_z + L_z

        # Kinematics:
        # -----------
        a_x=F_x/m_craft                      # acceleration
        a_y=F_y/m_craft
        a_z=F_z/m_craft

        v_x=a_x*dt + v_0x                    # velocity
        v_y=a_y*dt + v_0y
        v_z=a_z*dt + v_0z
        v=math.sqrt(v_x**2 + v_y**2 + v_z**2)

        x=0.5*a_x*dt**2 + v_0x*dt + x_0      # position
        y=0.5*a_y*dt**2 + v_0y*dt + y_0
        z=0.5*a_z*dt**2 + v_0z*dt + z_0

        # +-----------------+
        # | End of dynamics |
        # +-----------------+

    if COUNTDOWN_FLAG:  # Set when t<0.  No motion.
        a=0.
        a_x=0.
        a_y=0.
        a_z=0.
        v=0.
        v_x=0.
        v_y=0.
        v_z=0.
        x=x_0
        y=y_0
        z=z_0
        g_sub=1.0

    if LAUNCH_FLAG:     # If in Countdown or Launch:
        ION_BLACKOUT_FLAG=0
        D=0. # Drag
        L=0. # Lift

        # +----------------------+
        # | Countdown and Launch |
        # +----------------------+
        # Launch program
        # Geocentric Distance
        # -------------------
        which_poly_r='None'
        if t<0.:                     # Constant value
            r_E=6373.355*1000. # m
        if t>=0. and t<98.2:         # Polynomial 1
            which_poly_r='R Poly 1'
            r_E = ((3.68861E-12)*(t**6) - (1.02618E-09)*(t**5) + (4.41034E-08)*(t**4) \
                        + (1.66632E-05)*(t**3) + (6.28379E-04)*(t**2) + (4.78802E-03)*t + 6.37336E+03)*1000. # m
        if t>=98.2 and t<305.04:     # Polynomial 2
            which_poly_r='R Poly 2'
            r_E = ((4.47538E-13)*(t**6) - (9.36229E-10)*(t**5) + (7.61048E-07)*(t**4) \
                        - (3.09966E-04)*(t**3) + (6.53192E-02)*(t**2) - (5.94121E+00)*t + 6.57815E+03)*1000. # m
        if t>=305.04 and t<t_orbit_insertion:   # Polynomial 3
            which_poly_r='R Poly 3'
            r_E = ((7.01974E-14)*(t**6) - (1.99174E-10)*(t**5) + (2.26344E-07)*(t**4) \
                        - (1.30046E-04)*(t**3) + (3.85842E-02)*(t**2) - (5.06276E+00)*t + 6.67799E+03)*1000. # m
        #if t>=656.5:                 # Poly 3 has a wiggle at the end to avoid.
        #    r_E=6563203. # m           Hold constant
        if t>=t_orbit_insertion:
            which_poly_r='None'

        # Longitude (deg) = phi_E_deg
        # ---------------------------
        which_poly_long='None'
        if t<9.3:                     # Constant value
            phi_E_deg=-80.6041 # deg
            #lon_poly=0.
        if t>=9.3 and t<98.45:                   # Polynomial 1
            which_poly_long='Long Poly 1'
            phi_E_deg = (-4.31495E-14)*(t**6) + (1.05285E-11)*(t**5) + (8.75452E-10)*(t**4) \
                - (1.59125E-08)*(t**3) - (9.21278E-07)*(t**2) + (9.12768E-06)*t - 8.06041E+01 # deg
        if t>=98.45 and t<373.201:               # Polynomial 2
            which_poly_long='Long Poly 2'
            phi_E_deg = (-1.58756E-14)*(t**6) + (1.85492E-11)*(t**5) - (7.24723E-09)*(t**4) \
                + (7.08745E-07)*(t**3) + (2.50452E-04)*(t**2) - (4.70074E-02)*t - 7.84391E+01 # deg
        if t>=373.201 and t<t_orbit_insertion:   # Polynomial 3
            which_poly_long='Long Poly 3'
            phi_E_deg = (8.88857E-15)*(t**6) - (2.54467E-11)*(t**5) + (2.95795E-08)*(t**4) \
                - (1.78776E-05)*(t**3) + (5.99938E-03)*(t**2) - (1.03617E+00)*t - 6.67123E+00 # deg
        if t>=t_orbit_insertion:
            which_poly_long='None'

        #if t>=708.:            # Longitude poylnomial 3 speeds spacecraft up at the end, so set final position & speed:
        #    phi_E_deg=-52.6941 # deg
        #    v_0=7793.1 # m/s Inertial velocity at orbital insertion

        # Latitude=0 deg.  Stays on the equator.
        # --------------------------------------
        theta_E_deg=90.

        # Convert angles to radians
        phi_E   = (math.pi/180.)*phi_E_deg
        theta_E = (math.pi/180.)*theta_E_deg

        # Convert to Cartesian coords
        x= r_E * math.sin(theta_E) * math.cos(phi_E)
        y= r_E * math.sin(theta_E) * math.sin(phi_E)
        z= r_E * math.cos(theta_E)

        # Calculate velocity components
        v_x=(x-x_0)/dt
        v_y=(y-y_0)/dt
        v_z=(z-z_0)/dt
        v=math.sqrt(v_x**2 + v_y**2 + v_z**2)

        # Calculate acceleration components
        a_grav_x=F_gEx/m_craft
        a_grav_y=F_gEy/m_craft
        a_grav_z=F_gEz/m_craft
        a_tot_x=(v_x-v_0x)/dt     # Total acceleration taken from kinematics 
        a_tot_y=(v_y-v_0y)/dt
        a_tot_z=(v_z-v_0z)/dt
        a_sub_x=a_tot_x-a_grav_x  # Felt acceleration is total-gravitational
        a_sub_y=a_tot_y-a_grav_y
        a_sub_z=a_tot_z-a_grav_z
        a_sub=math.sqrt(a_sub_x**2 + a_sub_y**2 + a_sub_z**2)
        g_sub=a_sub/g_E

        ORBIT_INSERTION_FLAG=1

        # Rocket engine flags
        # -------------------
                                 # Saturn IC
        if t>-8.9 and t<-5.:     # Engine start (note staggered sequence)
            ENGINE_FLAG=1        # The t<-5. condition lets me forcibly 
            T_max=5.*T_SIC_F1    # turn them off later if engine failure.
        if t>=-6.4 and t<5.:
            SIC_F1_5_FLAG=1      # Inboard flag
        if t>=-6.1 and t<-5.:    # Outboard flags
            SIC_F1_1_FLAG=1
            SIC_F1_3_FLAG=1
        if t>=-6.0 and t<-5.:
            SIC_F1_4_FLAG=1
        if t>=-5.9 and t<-5.:
            SIC_F1_2_FLAG=1
        if t>=-6.4 and t<-6.1:   # Thrust
            T=T_SIC_F1
        if t>=-6.1 and t<-6.0:
            T=3.*T_SIC_F1
        if t>=-6.0 and t<-5.9:
            T=4.*T_SIC_F1
        if t>=-5.9 and t<-5.:
            T=5.*T_SIC_F1
        if t>=135.2 and t<161.6: # Engine cutoff
            SIC_F1_5_FLAG=0
            T=4.*T_SIC_F1
        if t>=161.6 and t<164.:
            ENGINE_FLAG=0
            T=0.
            SIC_F1_1_FLAG=0
            SIC_F1_2_FLAG=0
            SIC_F1_3_FLAG=0
            SIC_F1_4_FLAG=0
        if t>=162.:              # S-IC separation (can stay =1 at all future times)
            SEP_SIC_FLAG=1
                                 # Saturn II
        if t>=164. and t<166.:   # Engine start
            ENGINE_FLAG=1        # The t<166. condition lets me forcibly turn them off
            T_max=5.*T_SII_J2    # later if engine failure.
            T=T_max
            SII_J2_1_FLAG=1
            SII_J2_2_FLAG=1
            SII_J2_3_FLAG=1
            SII_J2_4_FLAG=1
            SII_J2_5_FLAG=1
        if FLAG_FAIL_SII_1:  # Single engine failure
            SII_J2_1_FLAG=0
            T=T_max-T_SII_J2
        if FLAG_FAIL_SII_2:  # Two engine failure
            SII_J2_1_FLAG=0
            SII_J2_2_FLAG=0
            T=T_max-(2*T_SII_J2)
        if t>=460.6 and t<548.2: # Engine cutoff
            T=4.*T_SII_J2
            SII_J2_5_FLAG=0
        if t>=548.2 and t<549.:
            ENGINE_FLAG=0
            T=0.
            SII_J2_1_FLAG=0
            SII_J2_2_FLAG=0
            SII_J2_3_FLAG=0
            SII_J2_4_FLAG=0
            SII_J2_5_FLAG=0
        if t>=549.:              # S-II separation (can stay =1 at all future times)
            SEP_SII_FLAG=1
                                              # Saturn IVB
        if t>=550. and t<t_orbit_insertion:   # Engine on
            SIVB_J2_1_BURN1_FLAG=1
            ENGINE_FLAG=1
            T_max=T_SIVB
            T=T_max
            SIVB_J2_1_FLAG=1             # S-IVB started once for orbital insertion,

        # ---------------------------------------------------
        # *** The following if statement DOES NOT WORK!!! ***
        # ---------------------------------------------------
        if t>=t_orbit_insertion and SIVB_J2_1_BURN1_FLAG==1:
            ENGINE_FLAG=0                # shut down, then started again manually for TLI.
            SIVB_J2_1_FLAG=0             # For TLI, SEP_SIVB_FLAG set by control panel
            T=0.

    # +-------------------------+
    # | End Launch Calculations |
    # +-------------------------+





    # Distances to Earth and Moon
    # ---------------------------
    r_E=math.sqrt((x-x_Earth)**2 + (y-y_Earth)**2 + (z-z_Earth)**2) # distance to Earth center (m)
    h_E=r_E-R_Earth                                # altitude above the Earth (m)
    h_E_km=h_E/1000.                               # altitude above the Earth (km)

    r_M=math.sqrt((x-x_Moon)**2 + (y-y_Moon)**2 + (z-z_Moon)**2)    # distance to Moon center (m)
    h_M=r_M-R_Moon                                 # altitude above the Moon (m)
    h_M_km=h_M/1000.                               # altitude above the Moon (km)
    if r_M<R_SOI_Moon:   # Spacecraft is within Moon's Sphere of Influence
        MOON_FLAG=1
    else:
        MOON_FLAG=0
    if not MOON_FLAG:
        h_current=h_E_km
    if MOON_FLAG:
        h_current=h_M_km
    h_dot=(h_current-h_current0)*1000./dt  # Rate of change of altitude (m/s)


    # Ground track: angular positions relative to Earth & Moon
    # --------------------------------------------------------
    phi_E=math.atan2((y-y_Earth),(x-x_Earth))      # Earth longitude  (rad)
    theta_E=math.acos((z-z_Earth)/r_E)             # Earth colatitude (rad)
    phi_E_deg=phi_E*180./math.pi                   # Earth longitude  (deg)
    theta_E_deg=theta_E*180./math.pi               # Earth colatitude (deg)

    phi_M=math.atan2((y-y_Moon),(x-x_Moon))        # Moon longitude  (rad)
    theta_M=math.acos((z-z_Moon)/r_M)              # Moon colatitude (rad)
    phi_M_deg=phi_M*180./math.pi                   # Moon longitude  (deg)
    theta_M_deg=theta_M*180./math.pi               # Moon colatitude (deg)

    # Is the spacecraft behind the Moon, causing a blackout?
    if phi_M_deg>90. or phi_M_deg<-90.:
        MOON_BLACKOUT_FLAG=1
    else:
        MOON_BLACKOUT_FLAG=0

    # Choose which body is being orbited: Earth/Moon
    if not MOON_FLAG:
        r=r_E
        r_rel_vec=np.array([x-x_Earth,y-y_Earth,z-z_Earth])
        v_vec=np.array([v_x,v_y,v_z]) # *** If Earth moves, subtract its v here
        R_body=R_Earth
        mu=G*M_Earth
    if MOON_FLAG:
        r=r_M
        r_rel_vec=np.array([x-x_Moon,y-y_Moon,z-z_Moon])
        v_vec=np.array([v_x,v_y,v_z]) # *** If Moon moves, subtract its v here
        R_body=R_Moon
        mu=G*M_Moon
    # *** Note that if I go to using moving Moon or Earth, I'll need to update the relative position & velocity vectors!
    # I need to update to relative r & v for other elements, too!

    # +-------------------------------------+
    # | Derive Keplerian orbital parameters |
    # +-------------------------------------+
    # if not LAUNCH_FLAG:  # If not in countdown or launch
    if True:  # Calculate these always
        #
        # Computationally intensive?  Only update every several seconds.  Add if/then on time.
        # 
        # Eccentricity, ecc
        ecc_x = (1./mu)*((v**2 - (mu/r))*x - (x*v_x + y*v_y + z*v_z)*v_x)
        ecc_y = (1./mu)*((v**2 - (mu/r))*y - (x*v_x + y*v_y + z*v_z)*v_y)
        ecc_z = (1./mu)*((v**2 - (mu/r))*z - (x*v_x + y*v_y + z*v_z)*v_z)
        ecc   = math.sqrt(ecc_x**2 + ecc_y**2 + ecc_z**2)

        # Inclination  *** Works
        W_vec  = np.cross(r_rel_vec,v_vec)
        if np.linalg.norm(W_vec.dot(W_vec)) > 0.:
            W_hat  = W_vec/(np.sqrt(W_vec.dot(W_vec)))
            inclin = math.acos(W_hat[2]) #*180./math.pi
            # Longitude of Ascending Node, Omega  *** Has sign error--check for quadrant?
            N_vec  = np.cross(np.array([0,0,1.]),W_hat)
            N_hat  = N_vec/(np.sqrt(N_vec.dot(N_vec)))
            Omega  = math.acos(N_hat[0]) #*180./math.pi
        else:
            inclin = 999.*math.pi/180.
            Omega  = 999.*math.pi/180.

        if ecc<1.:
            ORBIT_SHAPE='elliptical'
            # Semi-major axis, sma
            sma   = (1./((2./r) - (v**2/mu)))  # Semimajor axis (m)

            # Periapsis
            peri     = sma*(1.-ecc)  # Periapsis radius (m)
            peri_alt = (peri-R_body)/1000.# Periapsis altitude (km)
            # Apoapsis
            apo      = sma*(1.+ecc)  # Apoapsis radius (m)
            apo_alt  = (apo-R_body)/1000. # Apoapsis altitude (km)


        else:
            if ecc==1.:
                ORBIT_SHAPE='parabolic'
            if ecc>1.:
                ORBIT_SHAPE='hyperbolic'
            sma=999e+3
            peri=999
            peri_alt=999
            apo=999
            apo_alt=999

        # From http://ccar.colorado.edu/asen5070/handouts.htm
        sp_ang_vec = np.cross(r_rel_vec,v_vec)
        sp_ang     = np.sqrt(sp_ang_vec.dot(sp_ang_vec))
        if sp_ang>0.:
            inclin_2   = math.acos(sp_ang_vec[2]/sp_ang) #*180./math.pi
        else:
            inclin_2=17.4358 # 999 deg in rad
        Omega_2    = math.atan2(sp_ang_vec[0],-1.*sp_ang_vec[1]) #*180./math.pi
        if inclin_2 != 0. and inclin_2 != 17.4358:
            arg_lat    = math.atan2(r_rel_vec[2]/math.sin(inclin_2), (r_rel_vec[0]*math.cos(Omega_2)+r_rel_vec[1]*math.sin(Omega_2)))
        else:
            arg_lat = 999. # Can't calculate if inclination=0
        semi_lat     = sma*(1.-ecc**2)  # Semi-latus rectum
        if semi_lat>0.:
            true_anom  = math.atan2(math.sqrt(semi_lat/mu)*(v_vec.dot(r_rel_vec)) , semi_lat-r)
            if inclin_2 != 0.:
                arg_peri   = arg_lat - true_anom
            else:
                arg_peri = 999.
            ecc_anom   = 2.*math.atan(math.sqrt((1.-ecc)/(1.+ecc))*math.tan(true_anom/2.))
            mean_motion     = math.sqrt(mu/sma**3)  # Mean motion
            T_peri     = t - (ecc_anom - ecc*math.sin(ecc_anom))/mean_motion
        else:
            true_anom  = 999.
            arg_peri   = 999.
            ecc_anom   = 999.
            mean_motion= 999.
            T_peri     = 999.

    else:       # If not in countdown or launch:
        true_anom  = 999.
        arg_peri   = 999.
        ecc_anom   = 999.
        mean_motion= 999.
        T_peri     = 999.
        arg_lat    = 999.
        sma        = 999e+3
        peri       = 999
        peri_alt   = 999
        apo        = 999
        apo_alt    = 999
        ecc        = 999
        ORBIT_SHAPE='n/a'
        Omega      = 17.4358 # 999 deg in rad
        Omega_2    = 17.4358 # 999 deg in rad
        inclin     = 17.4358 # 999 deg in rad
        inclin_2   = 17.4358 # 999 deg in rad
    # +--------------------------+
    # | End Keplerian parameters |
    # +--------------------------+


    # Load this iteration's values into lists for later plotting:
    # -----------------------------------------------------------
    x_list.append(x)
    y_list.append(y)
    z_list.append(z)
    x_km_list.append(x/1000.)
    y_km_list.append(y/1000.)
    z_km_list.append(z/1000.)
    h_E_km_list.append(h_E_km)
    h_M_km_list.append(h_M_km)
    phi_E_deg_list.append(phi_E_deg)
    theta_E_deg_list.append(theta_E_deg)
    v_list.append(v)
    if abs(F_gE)>0.:  # B/C grav plotted in log, only use values>0
        t_min_F_gE_list.append(t_in_min)
        F_gE_list.append(-F_gE)
    if abs(F_gM)>0.:
        t_min_F_gM_list.append(t_in_min)
        F_gM_list.append(-F_gM)
    t_min_list.append(t_in_min)
    g_sub_list.append(g_sub)
    h_dot_list.append(h_dot)
    KE_wind_list.append(KE_wind)



    x_old=x_0
    y_old=y_0
    z_old=z_0

    # Update this iteration's final values to be the next iteration's initial values:
    # -------------------------------------------------------------------------------
    x_0        =x
    y_0        =y
    z_0        =z
    phi_E0     =phi_E
    theta_E0   =theta_E
    phi_M0     =phi_M
    theta_M0   =theta_M
    r_E0       =r_E
    r_M0       =r_M
    h_E0       =h_E
    h_M0       =h_M
    h_current0 =h_current
    v_0x       =v_x
    v_0y       =v_y
    v_0z       =v_z

    # Increment simulation iteration and time:
    # ----------------------------------------
    i=i+1
    t=t+dt


    # Display status on terminal window
    # ---------------------------------
    #if t%1==0.0:   # Every integer second, display updated status
    if i%SimDir_display_interval==0.0:   # Every x intervals, display updated status
        # Display current status:
        # Blank all fields before rewriting, to avoid problems with text of different lengths
        win.addstr(10,2, "                                            ") # Editor line
        win.addstr(10,3, "                                            ") # Error message line
        for ii in range(WIN_N_BLANK_LINES):
            win.addstr(ii,10,"                                        ")
        win.refresh()
        win.addstr(10,10,"MET             {0:02.0f}".format(t_hr)+":{0:02.0f}".format(t_min)+":{0:02.0f}".format(t_sec))
        win.addstr(11,10,"V:              {:.2f}".format(v)+" m/s")
        win.addstr(12,10,"HEIGHT:         {:.3f}".format(h_current)+" km")
        win.addstr(13,10,"T_max:          {:.1f}".format(T_max)+" N")
        win.addstr(14,10,"ENGINE:         "+("ON" if ENGINE_FLAG else "OFF"))
        win.addstr(15,10,"T:               {0:.0f}".format(T))
        win.addstr(16,10,"t:               {0:.1f}".format(t))
        if PROGRADE_FLAG==1:  win.addstr(17,10,"ATTITUDE: PROGRADE  ",curses.A_BOLD)
        if PROGRADE_FLAG==-1: win.addstr(17,10,"ATTITUDE: RETROGRADE",curses.A_BOLD)
        if HEADUP_FLAG==1:    win.addstr(17,31,"HEAD-UP  ",curses.A_BOLD)
        if HEADUP_FLAG==-1:   win.addstr(17,31,"HEAD-DOWN",curses.A_BOLD)
        win.addstr(18,10,"EARTH LONGITUDE:{:.1f}".format(phi_E_deg)+" deg")
        win.addstr(19,10,"EARTH LATITUDE: {:.1f}".format(90.-theta_E_deg)+" deg")
        win.addstr(20,10,"SIC :"+str(SIC_F1_1_FLAG)+str(SIC_F1_2_FLAG)+str(SIC_F1_3_FLAG)+str(SIC_F1_4_FLAG)+str(SIC_F1_5_FLAG))
        win.addstr(21,10,"SII :"+str(SII_J2_1_FLAG)+str(SII_J2_2_FLAG)+str(SII_J2_3_FLAG)+str(SII_J2_4_FLAG)+str(SII_J2_5_FLAG))
        win.addstr(22,10,"SIVB:"+str(SIVB_J2_1_FLAG))
        #win.addstr(20,10,"ATTITUDE X:     {:.2f}".format(att_x))
        #win.addstr(21,10,"ATTITUDE Y:     {:.2f}".format(att_y))
        #win.addstr(22,10,"ATTITUDE Z:     {:.2f}".format(att_z))
        win.addstr(23,10,"TIMESTEP:       "+str(dt)+" sec")
        win.addstr(24,10,"DELAY:          "+str(delay)+" sec")
        win.addstr(25,10,"time for loop:  "+str(time_loop_diff)+" sec")
        win.addstr(26,10,"MOON_FLAG:      "+str(MOON_FLAG))
        win.addstr(27,10,"SMA:            {:.1f}".format(sma/1000.)+" km")
        win.addstr(28,10,"ECC:            {:.3f}".format(ecc))
        win.addstr(29,10,"PERI_ALT:       {:.1f}".format(peri_alt)+" km")
        win.addstr(30,10,"APO_ALT:        {:.1f}".format(apo_alt)+" km")
        win.addstr(31,10,"ORBIT_SHAPE: "+ORBIT_SHAPE)
        win.addstr(32,10,"INCLINATION:    {:.1f}".format(inclin_2*180./math.pi)+" deg")
        win.addstr(33,10,"OMEGA:          {:.1f}".format(Omega_2*180./math.pi)+" deg")
        win.addstr(34,10,"T_PERI:         {:.1f}".format(T_peri)+" sec")
        win.addstr(35,10,"m_craft:        {:.1f}".format(m_craft)+" kg")
        win.addstr(36,10,"acceleration:   {:0.2f}".format(g_sub)+" g")
        win.addstr(37,10,"h-dot:          {:0.2f}".format(h_dot)+" m/s")
        win.addstr(38,10,"CHUTE_STATUS:   "+str(CHUTE_STATUS))
        win.addstr(39,10,"KE_wind:         {0:.0f}".format(KE_wind))
        win.addstr(40,10,"ION_BLACKOUT_FLAG:"+str(ION_BLACKOUT_FLAG))
        #win.addstr(41,10,"pitch_E_deg:     {0:.1f}".format(pitch_E_deg))
        win.addstr(41,10,"X:        "+"{:.1f}".format(x)+" m")
        win.addstr(42,10,"Y:        "+"{:.1f}".format(y)+" m")
        win.addstr(43,10,"Z:        "+"{:.1f}".format(z)+" m")
        #win.addstr(44,10,"X0:        "+"{:.1f}".format(x_old)+" m")
        #win.addstr(45,10,"Y0:        "+"{:.1f}".format(y_old)+" m")
        #win.addstr(46,10,"Z0:        "+"{:.1f}".format(z_old)+" m")
        win.addstr(47,10,"VX:        "+"{:.1f}".format(v_x)+" m/s")
        win.addstr(48,10,"VY:        "+"{:.1f}".format(v_y)+" m/s")
        win.addstr(49,10,"VZ:        "+"{:.1f}".format(v_z)+" m/s")

        # ------------------------
        # Apollo Guidance Computer
        # ------------------------
        if agc_verb==16 or (agc_verb==6 and AGC_V06_FLAG==0):
            if agc_verb==6 and AGC_V06_FLAG==0:
                # Have AGC_V06_FLAG initially set to 0. 
                # 1st time a v6+noun combination is set, if AGC_V06_FLAG=0, it displays, then sets AGC_V06_FLAG=1.
                # If already AGC_V06_FLAG=1, no change in value of registers.
                AGC_V06_FLAG=1
            if agc_noun==0:  # Display zeros in registers
                agc_r1=0.
                agc_r2=0.
                agc_r3=0.
            if agc_noun==43: # Ground track
                agc_r1_fmt="{0:04.2f}"
                agc_r2_fmt="{0:04.2f}"
                agc_r3_fmt="{0:05.1f}"
                if MOON_FLAG:
                    agc_r1=90.-theta_M_deg
                    agc_r2=phi_M_deg
                    agc_r3=h_current
                else:
                    agc_r1=90.-theta_E_deg
                    agc_r2=phi_E_deg
                    agc_r3=h_current
            if agc_noun==44: # Orbit info
                agc_r1_fmt="{0:05.1f}"
                agc_r2_fmt="{0:05.1f}"
                agc_r3_fmt="{0:05.1f}"
                agc_r1=apo_alt
                agc_r2=peri_alt
                agc_r3=h_current           # In real AGC this shows TFF (Time of Free Fall)
            if agc_noun==62: # Altitude and h-dot
                agc_r1_fmt="{0:05.0f}"
                agc_r2_fmt="{0:05.0f}"
                agc_r3_fmt="{0:05.1f}"
                agc_r1=v
                agc_r2=h_dot
                agc_r3=h_current
            if agc_noun==64: # reentry
                agc_r1_fmt="{0:04.2f}"
                agc_r2_fmt="{0:05.0f}"
                agc_r3_fmt="{0:05.1f}"
                agc_r1=g_sub
                agc_r2=v
                agc_r3=h_current    # Range to splash not implemented yet.
                                    # if need a blank ragister, set: agc_r3=float('nan')
            if agc_noun==73: # Altitude, v, flight path angle
                agc_r1_fmt="{0:05.0f}"
                agc_r2_fmt="{0:05.0f}"
                agc_r3_fmt="{0:06.3f}"
                agc_r1=h_current
                agc_r2=v
                agc_r3=90.-pitch_E_deg
        if agc_verb==37:  # Change program
            agc_prog=agc_noun
        # --------------------------------
        # Apollo Guidance Computer display
        # --------------------------------
        # Verb & Noun
        win.addstr(DSKY_U+5, DSKY_L+5,"{0:02.0f}".format(agc_verb), curses.color_pair(1)) # 2 digits
        win.addstr(DSKY_U+5, DSKY_L+9,"{0:02.0f}".format(agc_noun), curses.color_pair(1))
        #else:                     # Blank the registers
        # AGC Program
        win.addstr(DSKY_U+2, DSKY_L+9,"{:02.0f}".format(agc_prog),curses.color_pair(1))
        # AGC Registers
        # Blank the registers
        win.addstr(DSKY_U+8, DSKY_L+5,"      ")
        win.addstr(DSKY_U+9, DSKY_L+5,"      ")
        win.addstr(DSKY_U+10,DSKY_L+5,"      ")
        if not agc_verb==0:  # Leave registers blank if V00
            # Determine numerical format based on noun:
            win.addstr(DSKY_U+8, DSKY_L+5,agc_r1_fmt.format(agc_r1),curses.color_pair(1)) # leading 0's, 6 digits, no decimal
            win.addstr(DSKY_U+9, DSKY_L+5,agc_r2_fmt.format(agc_r2),curses.color_pair(1))
            if not math.isnan(agc_r3):  # If R3 is used
                win.addstr(DSKY_U+10,DSKY_L+5,agc_r3_fmt.format(agc_r3),curses.color_pair(1))
        # Entry Monitor Subsystem Delta-v
        win.addstr(DSKY_U+14,DSKY_L+5,"{:06.0f}".format(delta_v_rm),curses.color_pair(1))

        win.refresh()

        # NB: Still in the "every 2 intervals..." block. Keep here or do every interval?
    # +----------------------------------------------+
    # | Send data to Astronauts & Flight Controllers |
    # +----------------------------------------------+
    # 
    # ...
    # 
    # +------------------------------------+
    # | Check for external station actions |
    # +------------------------------------+

    if USE_PANEL:
        win.addstr(1,10,"Control Panel In Use")

        # *********************************
        # Add routines here to send/receive
        # packets to/from Control Panel
        # *********************************
        #
        # Flags sent to Control Panel:
        # ----------------------------
        # ABORT_REQUEST_FLAG
        # ENGINE_FLAG
        # 
        # Flags received from Control Panel:
        # ----------------------------------
        # ABORT_FLAG
        # ENGINE_FLAG
        # SEP_SIVB_FLAG
        # SEP_SM_FLAG
        # CHUTE_DROGUE_FLAG
        # CHUTE_MAIN_FLAG
        # HEADUP_FLAG
        # PROGRADE_FLAG
        # ATT_HOLD_FLAG

        if CHUTE_DROGUE_FLAG and not CHUTE_MAIN_FLAG:
            if CHUTE_STATUS==0:
                CHUTE_STATUS=1  # Deploy drogues
                t_chute=0       # reset timer for unreefing
        if CHUTE_MAIN_FLAG:
            if CHUTE_STATUS==2:
                CHUTE_STATUS=2  # Don't change anything if main is already deployed.
            if CHUTE_STATUS==1:
                CHUTE_STATUS=2  # Deploy main
                t_chute=0       # reset timer for unreefing


    if USE_ASTRONAUTS:
        win.addstr(2,10,"Astronaut Station In Use")

        # ***********************************
        # Add routines here to send/receive
        # packets to/from Astronauts' station
        # ***********************************
        # 
        # Astronauts receive the following variables:
        # t
        # agc_r1, agc_r2, agc_r3, agc_prog
        # delta_v_rm
        # 
        # Astronauts send the following variables:
        # delta_v_target
        #
        # Astronauts send & receive the following variables:
        # agc_verb, agc_noun
        # 

    if USE_FDO:
        win.addstr(3,10,"FDO In Use")

        # Flags & variables related to flight controllers:
        # ------------------------------------------------
        # 
        # FDO sends & receives the following variables:
        # ABORT_REQUEST_FLAG
        # STATUS_FDO_FLAG
        # 
        # FDO receives the following variables:
        # t
        # x, y, z
        # v_x, v_y, v_z
        # r_E, h_E_km
        # r_M, h_M_km
        # m_craft
        # T_max
        # T
        # MOON_FLAG
        # ION_BLACKOUT_FLAG
        # ABORT_FLAG

        if time.time()-last_time>=FDO_plot_interval:  # If it has been at least * sec since last plot generation
            last_time=time.time()
            # Save FDO's plots as PDFs
            # Earth region orbital plot (x-y plane):
            P.xlim(-4.e5,8.e3) # Use when going to Moon
            P.ylim(-8.e4,8.e4)
            # Draw Earth (x-y plane):
            theta_circle=range(361)
            earth_x=[((R_Earth/1000.)*math.cos(theta_temp*(math.pi/180.))+(x_Earth/1000.)) for theta_temp in theta_circle]
            earth_y=[((R_Earth/1000.)*math.sin(theta_temp*(math.pi/180.))+(y_Earth/1000.)) for theta_temp in theta_circle]
            P.plot(earth_x,earth_y,color='green')
            # Draw Moon (x-y plane):
            moon_x=[((R_Moon/1000.)*math.cos(theta_temp*(math.pi/180.))+(x_Moon/1000.)) for theta_temp in theta_circle]
            moon_y=[((R_Moon/1000.)*math.sin(theta_temp*(math.pi/180.))+(y_Moon/1000.)) for theta_temp in theta_circle]
            P.plot(moon_x,moon_y,color='gray')
            # Plot orbit (x-y plane):
            P.plot(x_km_list,y_km_list)
            P.plot([0],[0],'ro')        # Mark North Pole with red circle
            P.plot([x_0/1000.],[y_0/1000.],'rx')  # Mark current position with red x
            P.xlabel('x (km)')
            P.ylabel('y (km)')
            P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
            P.savefig('spaceflight6-orbit-earth-moon.'+ext)

            # Earth region orbital plot (x-y plane):
            P.xlim(-8.e3,8.e3)  # Use for Earth orbit
            P.ylim(-8.e3,8.e3)
            # Draw Earth (x-y plane):
            theta_circle=range(361)
            earth_x=[((R_Earth/1000.)*math.cos(theta_temp*(math.pi/180.))+(x_Earth/1000.)) for theta_temp in theta_circle]
            earth_y=[((R_Earth/1000.)*math.sin(theta_temp*(math.pi/180.))+(y_Earth/1000.)) for theta_temp in theta_circle]
            P.plot(earth_x,earth_y,color='green')
            # Draw Moon (x-y plane):
            moon_x=[((R_Moon/1000.)*math.cos(theta_temp*(math.pi/180.))+(x_Moon/1000.)) for theta_temp in theta_circle]
            moon_y=[((R_Moon/1000.)*math.sin(theta_temp*(math.pi/180.))+(y_Moon/1000.)) for theta_temp in theta_circle]
            P.plot(moon_x,moon_y,color='gray')
            # Plot orbit (x-y plane):
            P.plot(x_km_list,y_km_list)
            P.plot([0],[0],'ro')        # Mark North Pole with red circle
            P.plot([x_0/1000.],[y_0/1000.],'rx')  # Mark current position with red x
            P.xlabel('x (km)')
            P.ylabel('y (km)')
            P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
            P.savefig('spaceflight6-orbit-earth.'+ext)

            # Plot altitudes
            P.clf()
            if MOON_FLAG:
                # P.semilogy(t_min_list,h_M_km_list)
                P.plot(t_min_list,h_M_km_list)
            else:
                # P.semilogy(t_min_list,h_E_km_list)
                P.plot(t_min_list,h_E_km_list)
            P.xlabel('t (min)')
            P.ylabel('Altitude, h (km)')
            P.savefig('spaceflight6-altitude.'+ext)

            # Plot velocity
            P.clf()
            P.plot(t_min_list,v_list)
            P.xlabel('t (min)')
            P.ylabel('v (m/s)')
            P.savefig('spaceflight6-velocity.'+ext)

            handle = open("spaceflight6-orbit-earth-moon." + ext, "rb")
            handle.seek(0, 2)
            file_size = handle.tell()
            handle.seek(0, 0)
            orbit_earth_moon_pdf_data = handle.read(file_size)
            handle.close()

            handle = open("spaceflight6-orbit-earth." + ext, "rb")
            handle.seek(0, 2)
            file_size = handle.tell()
            handle.seek(0, 0)
            orbit_earth_pdf_data = handle.read(file_size)
            handle.close()

            handle = open("spaceflight6-altitude." + ext, "rb")
            handle.seek(0, 2)
            file_size = handle.tell()
            handle.seek(0, 0)
            altitude_pdf_data = handle.read(file_size)
            handle.close()

            handle = open("spaceflight6-velocity." + ext, "rb")
            handle.seek(0, 2)
            file_size = handle.tell()
            handle.seek(0, 0)
            velocity_pdf_data = handle.read(file_size)
            handle.close()

            for net_id in FDO_NET_ID_LIST:
                protocol.transmit_command(net_id, 15, [1])
                protocol.transmit_command(net_id, 13, orbit_earth_moon_pdf_data)
                protocol.transmit_command(net_id, 15, [2])
                protocol.transmit_command(net_id, 13, orbit_earth_pdf_data)
                protocol.transmit_command(net_id, 15, [3])
                protocol.transmit_command(net_id, 13, altitude_pdf_data)
                protocol.transmit_command(net_id, 15, [4])
                protocol.transmit_command(net_id, 13, velocity_pdf_data)



    if USE_GUIDO:
        win.addstr(3,10,"GUIDO In Use")

        # Flags & variables related to flight controllers:
        # ------------------------------------------------
        # 
        # GUIDO receives:
        # t_hr
        # t_min
        # t_sec
        # ecc
        # peri_alt
        # apo_alt
        #
        # MOON_FLAG
        # PROGRADE_FLAG
        # HEADUP_FLAG
        # ATT_HOLD_FLAG

    if USE_GNC:
        win.addstr(3,10,"GNC In Use")
        # GNC Receives:
        # t_hr		
        # t_min		
        # t_sec
        # ABORT_REQUEST_FLAG
        # ABORT_FLAG	
        # ENGINE_FLAG	
        # SEP_SIVB_FLAG
        # CHUTE_DROGUE_FLAG
        # CHUTE_MAIN_FLAG
        # HEADUP_FLAG
        # ATT_HOLD_FLAG

    if USE_CAPCOM:
        win.addstr(3,10,"CAPCOM In Use")
        # No data sent or received

    if USE_BOOSTER:
        win.addstr(3,10,"BOOSTER In Use")
        # BOOSTER receives:
        # t_hr
        # t_min
        # t_sec
        # 
        # m_craft
        # 
        # T
        # 
        # SIC_F1_1_FLAG
        # SIC_F1_2_FLAG
        # SIC_F1_3_FLAG
        # SIC_F1_4_FLAG
        # SIC_F1_5_FLAG
        # 
        # SEP_SIC_FLAG
        # 
        # SIC_J2_1_FLAG
        # SIC_J2_2_FLAG
        # SIC_J2_3_FLAG
        # SIC_J2_4_FLAG
        # SIC_J2_5_FLAG
        # 
        # SEP_SII_FLAG
        # 
        # SIVB_J2_1_FLAG
        # 
        # SEP_SIVB_FLAG

    if USE_RETRO:
        win.addstr(3,10,"RETRO In Use")
        # RETRO receives:
        # t_hr
        # t_min
        # t_sec
        # 
        # h_E_km
        # m_craft
        # 
        # v
        # m_craft
        # T
        # MOON_FLAG
        # 
        # ecc	
        # peri_alt
        # apo_alt	
        # 
        # ABORT_REQUEST_FLAG
        # ABORT_FLAG	
        # ENGINE_FLAG	
        # SM_SEP_FLAG	
        # CHUTE_DROGUE_FLAG
        # CHUTE_MAIN_FLAG
        # PROGRADE_FLAG
        # HEADUP_FLAG


    time_loop_end =datetime.datetime.now()  # Used for timing the main loop
    time_loop_diff=(time_loop_end.microsecond-time_loop_init.microsecond)/1.e6   # How much time did it take? (sec)

    # Delay by enough to keep the simulation running close to real time.
    delay_adaptive=delay-time_loop_diff
    if delay_adaptive>0.:
        if time_loop_diff>0.:
            time.sleep(delay_adaptive)  # sec  Start w/delay=dt, because each loop is much faster than real time
        else:
            time.sleep(delay)

# Clean up from curses:
# ---------------------
curses.curs_set(1)
curses.nocbreak()
win.keypad(0)
curses.echo()
curses.endwin()

# Did spacecraft land or crash?
# -----------------------------
if CRASH_E:
    print "********************************"
    print "*  You crashed into the Earth  *"
    print "********************************"
    print
if LAND_E:
    print "************************************"
    print "*  You successfully splashed down  *"
    print "************************************"
    print
if CRASH_M:
    print "*******************************"
    print "*  You crashed into the Moon  *"
    print "*******************************"
    print
if LAND_M:
    print "*****************************************"
    print "*  You successfully landed on the Moon  *"
    print "*****************************************"
    print
if BREAKUP_FLAG:
    print "*****************************"
    print "*  Your spacecraft broke up *"
    print "*****************************"
    print


# Save final data:
# ----------------
now=datetime.datetime.now()
outfilesuffix=str(now).split()[0]+'-'+str(now).split()[1]
outfilename=outfilerootname+outfilesuffix

outfile=open(outfilename.replace(":", "__"), 'w')
#outfile.write('m_craft       '+str(m_craft)+'\n')
#outfile.write('T_max         '+str(T_max)+'\n')
#outfile.write('ENGINE_FLAG   '+str(ENGINE_FLAG)+'\n')
#outfile.write('CHUTE_STATUS  '+str(CHUTE_STATUS)+'\n')
#outfile.write('t_chute       '+str(t_chute)+'\n')
#outfile.write('T             '+str(T)+'\n')
#outfile.write('MOON_FLAG     '+str(MOON_FLAG)+'\n')
outfile.write('t_hr          '+str(t_hr)+'\n')
outfile.write('t_min         '+str(t_min)+'\n')
outfile.write('t_sec         '+str(t_sec)+'\n')
outfile.write('INIT_RADIAL    0\n')   # Setting INIT_RADIAL=0 for now.  Use cartesian.
outfile.write('USE_LAUNCH    '+str(USE_LAUNCH)+'\n')
outfile.write('SEP_SIVB_FLAG '+str(SEP_SIVB_FLAG)+'\n')
outfile.write('SEP_SM_FLAG   '+str(SEP_SM_FLAG)+'\n')
outfile.write('PROGRADE_FLAG '+str(PROGRADE_FLAG)+'\n')
outfile.write('HEADUP_FLAG   '+str(HEADUP_FLAG)+'\n')
outfile.write('SEP_SIVB_FLAG '+str(SEP_SIVB_FLAG)+'\n')
outfile.write('SEP_SM_FLAG   '+str(SEP_SM_FLAG)+'\n')
outfile.write('agc_verb      '+str(agc_verb)+'\n')
outfile.write('agc_noun      '+str(agc_noun)+'\n')
outfile.write('t             '+str(t)+'\n')
outfile.write('x_0           '+str(x_0)+'\n')
outfile.write('y_0           '+str(y_0)+'\n')
outfile.write('z_0           '+str(z_0)+'\n')
outfile.write('v_0x          '+str(v_0x)+'\n')
outfile.write('v_0y          '+str(v_0y)+'\n')
outfile.write('v_0z          '+str(v_0z)+'\n')
outfile.close()


# Display results
# ---------------
if MAKE_PLOTS:
    if EARTH_MOON:
        P.xlim(-4.e8,8.e6) # Use when going to Moon
        P.ylim(-8.e7,8.e7)
    else:
        P.xlim(-8.e6,8.e6)  # Use for Earth orbit
        P.ylim(-8.e6,8.e6)

    # Draw Earth:
    theta_circle=range(361)
    earth_x=[(R_Earth*math.cos(theta_temp*math.pi/180.)+x_Earth) for theta_temp in theta_circle]
    earth_y=[(R_Earth*math.sin(theta_temp*math.pi/180.)+y_Earth) for theta_temp in theta_circle]
    P.plot(earth_x,earth_y,color='green')

    # Draw Moon:
    moon_x=[(R_Moon*math.cos(theta_temp*math.pi/180.)+x_Moon) for theta_temp in theta_circle]
    moon_y=[(R_Moon*math.sin(theta_temp*math.pi/180.)+y_Moon) for theta_temp in theta_circle]
    P.plot(moon_x,moon_y,color='gray')

    # Plot orbit
    P.plot(x_list,y_list)
    P.xlabel('x (m)')
    P.ylabel('y (m)')
    P.gca().set_aspect('equal', adjustable='box') # Makes plot be square
    P.savefig('spaceflight6-orbit.pdf')

    # Plot altitudes
    P.clf()
    P.semilogy(t_min_list,h_E_km_list)
    P.xlabel('t (min)')
    P.ylabel('Earth altitude, h (km)')
    P.savefig('spaceflight6-h_km.pdf')

    # Plot h-dot
    P.clf()
    P.ylim(-300,300)
    P.plot(t_min_list,h_dot_list)
    P.xlabel('t (min)')
    P.ylabel('h-dot (m/s)')
    P.savefig('spaceflight6-h_dot.pdf')

    # Plot velocity
    P.clf()
    P.plot(t_min_list,v_list)
    P.xlabel('t (min)')
    P.ylabel('v (m/s)')
    P.savefig('spaceflight6-v.pdf')

    # Plot ground track (phi=longitude, theta=colatitude):
    P.clf()
    P.xlim(-180.,180)
    P.ylim(-90.,90)
    lon_list=[phi for phi in phi_E_deg_list]           # longitude  (deg)
    lat_list=[90.-theta for theta in theta_E_deg_list] # colatitude (deg)
    P.plot(lon_list,lat_list)              # Plot track in blue
    P.plot([phi_E0*(180./math.pi)],[90.-theta_E0*(180./math.pi)], 'ro')  # Mark current position w/red circle
    P.savefig('spaceflight6-earth-track.pdf')

    # Plot gravity
    P.clf()
    #P.plot(t_min_list,F_gE_list)
    #P.plot(t_min_list,F_gM_list)
    P.semilogy(t_min_F_gE_list,F_gE_list,color='red')
    P.semilogy(t_min_F_gM_list,F_gM_list,color='blue')
    P.xlabel('t (min)')
    P.ylabel('F_g (N) for Earth and Moon')
    P.savefig('spaceflight6-F_g.pdf')

    # Plot g-forces
    P.clf()
    P.ylim(0.,5.)
    P.plot(t_min_list,g_sub_list)
    P.xlabel('t (min)')
    P.ylabel('Acceleration (g)')
    P.savefig('spaceflight6-g.pdf')

    # Plot KE of relative wind
    P.clf()
    P.plot(t_min_list,KE_wind_list)
    P.xlabel('t (min)')
    P.ylabel('KE/volume of relative wind (J/m^-3)')
    P.savefig('spaceflight6-KE_wind.pdf')

    # Plot altitude vs. velocity
    P.clf()
    P.plot(v_list,h_E_km_list)
    P.xlabel('v (m/s)')
    P.ylabel('Altitude (km)')
    P.savefig('spaceflight6-alt-v.pdf')


# If DEBUG is set:
if DEBUG:
    print
    print "*********"
    print "INIT_RADIAL=",INIT_RADIAL
    print "x_0,y_0,z_0"
    print x_0,y_0,z_0
    print
    print "r_E0,phi_E0,theta_E0"
    print r_E0,phi_E0,theta_E0
    print "*********"
    print 
