#!/usr/bin/env python3

# ORBIT_M2E
# Computes the trajectory from Moon to Earth.
# Stops when it hits perigee

import numpy as np
import math
import sys
from astropy import units as u
from poliastro.bodies import Earth, Moon
from poliastro.twobody import Orbit
from poliastro.twobody.propagation import kepler
from poliastro.twobody.propagation import cowell
import matplotlib.pyplot as P

SAVE_PLOTS=0      # 1=save, 0=display
STAGE2_COMPUTE=1  # 1=compute path in Earth's SOI

t_incr=1.*u.min  # Time increment
t_incr_annotate=120.*u.min     # Time increment for timestamps on plots

# Earth & Moon coordinates
# ------------------------
pos_earth =[0., 0., 0.]*u.km       #  Earth position in km (for drawing)
pos_moon  =[-3.85e5, 0., 0.]*u.km  #  Moon position in km (for drawing & reference frame)
vel_moon  =[0., 0., 0.]*u.km/u.s   #  Moon velocity in km (for reference frame)

x_Earth=pos_earth[0].value # In km
y_Earth=pos_earth[1].value
z_Earth=pos_earth[2].value
x_Moon= pos_moon[0].value
y_Moon= pos_moon[1].value
z_Moon= pos_moon[2].value
R_Earth=6373.296*u.km
R_Moon=1.7375e3*u.km
R_SOI_Moon=6.629e4*u.km            # Radius of Moon's Sphere of Influence in km

# Lists for orbit plotting
stage1_moon_x=[]
stage1_moon_y=[]
stage1_moon_z=[]
stage1_x=[]
stage1_y=[]
stage1_z=[]
stage2_x=[]
stage2_y=[]
stage2_z=[]
annotate_x=[]
annotate_y=[]
annotate_z=[]
annotate_label=[]

if len(sys.argv)==2:        # Command-line option: filename for input parameters
    infilename=sys.argv[1]
    infile=open(infilename, 'r')
    parameters={}                            # Create parameter dictionary
    for param_line in infile:
        if param_line != '\n':               # Skip blank lines
            param_temp = param_line.split()  # If line not blank, split it
            if param_temp[0] != '#':         # If not a comment line (marked with '#')
                param_name = param_temp[0]   # 1st element is parameter name
                param_value= param_temp[1]   # 2nd element is parameter value
                parameters[param_name] = param_value
    infile.close()
    t_input         = float(parameters['t'])*u.s  # must be in seconds
    x_0             = float(parameters['x_0'])    # must be in meters
    y_0             = float(parameters['y_0'])
    z_0             = float(parameters['z_0'])
    v_read_0x       = float(parameters['v_0x'])   # must be in m/s
    v_read_0y       = float(parameters['v_0y'])
    v_read_0z       = float(parameters['v_0z'])

print('t_input=  ',t_input)
print('x_0=      ',x_0)
print('y_0=      ',y_0)
print('z_0=      ',z_0)
print('v_read_0x=',v_read_0x)
print('v_read_0y=',v_read_0y)
print('v_read_0z=',v_read_0z)

# Calculate velocity unit vector
v_read_mag=math.sqrt(v_read_0x**2 + v_read_0y**2 + v_read_0z**2)
v_0x_hat=v_read_0x/v_read_mag
v_0y_hat=v_read_0y/v_read_mag
v_0z_hat=v_read_0z/v_read_mag

print('Speed before maneuver (m/s): {:.1f}'.format(v_read_mag))
print('Enter speed after maneuver (m/s) or')
input_temp=input('hit <enter> for no change:  ')
try:
    v_0_mag=float(input_temp)
    delta_v=v_0_mag-v_read_mag
    FLAG_DELTAV=1
except:
    v_0_mag=v_read_mag
    FLAG_DELTAV=0

v_0x=v_0_mag*v_0x_hat
v_0y=v_0_mag*v_0y_hat
v_0z=v_0_mag*v_0z_hat

print("\n\n\n*********************")
print(      "* Trajectory Design *")
print(      "*********************\n\n\n")


r_in=[x_0, y_0, z_0]*u.m
v_in=[v_0x, v_0y, v_0z]*u.m/u.s

# Moon -> Earth SOI leg
# ---------------------
r_moon_in=r_in-pos_moon # Position in Moon frame
v_moon_in=v_in-vel_moon # Velocity in Moon frame

# Initial orbit around Moon
orbit_moon=Orbit.from_vectors(Moon,r_moon_in,v_moon_in)
# Propagate forward to edge of Moon's SOI
i=0
delta_t=0.
FLAG_SOI=1
while FLAG_SOI==1 and delta_t<6*u.d: # Upper limit of 3 days to search over
    delta_t=i*t_incr
    # 'Future' values end in "_f".
    orbit_moon_f=orbit_moon.propagate(delta_t, method=cowell) # Use Cowell for Moon
    r_moon_f=(orbit_moon_f.r).to(u.km)
    v_moon_f=(orbit_moon_f.v).to(u.km/u.s)
    r_moon_f_mag=math.sqrt((r_moon_f[0].value**2)+(r_moon_f[1].value**2)+(r_moon_f[2].value**2))*u.km
    v_moon_f_mag=math.sqrt((v_moon_f[0].value**2)+(v_moon_f[1].value**2)+(v_moon_f[2].value**2))*u.km/u.s
    
    r_f=r_moon_f+pos_moon
    v_f=v_moon_f+vel_moon
    v_f_mag=math.sqrt((v_f[0].value**2)+(v_f[1].value**2)+(v_f[2].value**2))*u.km/u.s
    
    # Append position to list for plotting
    stage1_moon_x.append(r_moon_f[0].value)
    stage1_moon_y.append(r_moon_f[1].value)
    stage1_moon_z.append(r_moon_f[2].value)
    stage1_x.append(r_f[0].value)
    stage1_y.append(r_f[1].value)
    stage1_z.append(r_f[2].value)

    if (t_input+delta_t)%t_incr_annotate==0:
        annotate_x.append(r_f[0].value)
        annotate_y.append(r_f[1].value)
        annotate_z.append(r_f[2].value)
        annotate_label.append(str((t_input+delta_t).to(u.hr)))


    if r_moon_f_mag > R_SOI_Moon: # If outside the Moon's Sphere of Influence
        FLAG_SOI=0
        r_exit=r_f
        v_exit=v_f
        v_mag_exit=v_f_mag
        t_exit=(t_input+delta_t)
        break

    i=i+1
    
if FLAG_SOI:
    # End here if spacecraft doesn't leave Moon's SOI
    print('\n******************************************')
    print(  '* FAILED TO LEAVE MOON SOI WITHIN 6 DAYS *')
    print(  '******************************************\n')
    STAGE2_COMPUTE=0
else:
    print('\n***************************************')
    print(  ' Exit from Moon\'s Sphere of Influence:\n')
    print('Mission Elapsed Time:       {0:.1f}'.format(t_exit.to(u.d))+\
              ' or {0:.1f}'.format(t_exit.to(u.h))+' or {0:0.2f}'.format(t_exit))
    print('speed (Earth frame):  {0:.3f}'.format(v_mag_exit))
    print('')


# In Earth Sphere of Influence
if STAGE2_COMPUTE:
    r_entry=r_exit
    v_entry=v_exit
    t_entry=t_exit
    # Create earth-centered orbit
    orbit_earth=Orbit.from_vectors(Earth,r_entry,v_entry)
    
    # Propagate forward to find time of perigee
    i=0
    delta_t=0.
    FLAG_PG=0
    while FLAG_PG==0 and delta_t<3*u.d: # Upper limit of 3 days to search over
        delta_t=i*t_incr
        # 'Future' values end in "_f".
        orbit_earth_f=orbit_earth.propagate(delta_t, method=kepler) # Use Kepler for Earth
        r_f=(orbit_earth_f.r).to(u.km)
        v_f=(orbit_earth_f.v).to(u.km/u.s)
        r_f_mag=math.sqrt((r_f[0].value**2)+(r_f[1].value**2)+(r_f[2].value**2))*u.km

        # Append position to list for plotting        
        stage2_x.append(r_f[0].value)
        stage2_y.append(r_f[1].value)
        stage2_z.append(r_f[2].value)
        
        if (delta_t+t_entry)%t_incr_annotate==0:
            annotate_x.append(r_f[0].value)
            annotate_y.append(r_f[1].value)
            annotate_z.append(r_f[2].value)
            annotate_label.append(str((delta_t+t_entry).to(u.hr).value))
        
        if i>0:
            if r_f_mag>r_mag_prev:      # If you're getting farther away now...
                i_pg=i-1                # The previous iteration is the closest one
                delta_t_pg=i_pg*t_incr  # Time to perigee (PG) since entering Earth SOI
                t_total_pg=delta_t_pg+t_entry  # Total time to PG from beginning of Stage 1
                # Propagate to time of perigee
                orbit_earth_pg=orbit_earth.propagate(delta_t_pg, method=kepler)
                r_pg=(orbit_earth_pg.r).to(u.km)
                v_pg=(orbit_earth_pg.v).to(u.km/u.s)
                r_pg_mag=math.sqrt((r_pg[0].value**2)+(r_pg[1].value**2)+(r_pg[2].value**2))*u.km
                v_pg_mag=math.sqrt((v_pg[0].value**2)+(v_pg[1].value**2)+(v_pg[2].value**2))*u.km/u.s
                alt_pg=r_pg_mag-R_Earth       # Perigee altitude
                print("Total time:         {0:.1f}".format(t_total_pg.to(u.d))+\
                          " or {0:.1f}".format(t_total_pg.to(u.h))+"  or  {0:0.2f}".format(t_total_pg))
                print("altitude:           {0:0.5}".format(alt_pg))
                print("radius:             {0:0.6}".format(r_pg_mag))
                print("speed:              {0:0.4}".format(v_pg_mag))
                if FLAG_DELTAV:
                    print('delta-v (m/s)    {:,.1f}'.format(delta_v))
                else:
                    print('No delta-v.')
                print('t   (s)          {:,.0f}'.format(t_total_pg.to(u.s)))
                print('x0  (m)          {:,.0f}'.format(r_pg[0].to(u.m)))
                print('y0  (m)          {:,.0f}'.format(r_pg[1].to(u.m)))
                print('z0  (m)          {:,.0f}'.format(r_pg[2].to(u.m)))
                print('v0x (m/s)        {:,.1f}'.format(v_pg[0].to(u.m/u.s)))
                print('v0y (m/s)        {:,.1f}'.format(v_pg[1].to(u.m/u.s)))
                print('v0z (m/s)        {:,.1f}'.format(v_pg[2].to(u.m/u.s)))
                print('\nFor further orbit design, \n')
                print('Copy directly to input parameter file:')
                print('--------------------------------------\n')
                print('t             {:.0f}'.format(t_total_pg.to(u.s).value))
                print('x_0           {:.0f}'.format(r_pg[0].to(u.m).value))
                print('y_0           {:.0f}'.format(r_pg[1].to(u.m).value))
                print('z_0           {:.0f}'.format(r_pg[2].to(u.m).value))
                print('v_0x          {:.1f}'.format(v_pg[0].to(u.m/u.s).value))
                print('v_0y          {:.1f}'.format(v_pg[1].to(u.m/u.s).value))
                print('v_0z          {:.1f}'.format(v_pg[2].to(u.m/u.s).value))
                print('\n')
                FLAG_PG=1
        r_mag_prev=r_f_mag # Save current value as "previous" to compare
                           # against the next iteration.
        i=i+1
    if FLAG_PG==0:
        print('\n***********************************************')
        print(  '* FAILED TO REACH EARTH PERIGEE WITHIN 3 DAYS *')
        print(  '***********************************************\n')

# Moon-Earth Transit Plot
# -----------------------
# Draw Earth (x-y plane):
P.clf()
theta_circle=range(361)
earth_x=[(R_Earth.value*math.cos(theta_temp*(math.pi/180.))+x_Earth) for theta_temp in theta_circle]
earth_y=[(R_Earth.value*math.sin(theta_temp*(math.pi/180.))+y_Earth) for theta_temp in theta_circle]
P.plot(earth_x,earth_y,color='green')
# Draw Moon (x-y plane):
moon_x=[(R_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
moon_y=[(R_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(moon_x,moon_y,color='gray')
# Draw Moon's SOI:
soi_x=[(R_SOI_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
soi_y=[(R_SOI_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(soi_x,soi_y,color='gray',linewidth=1.0, linestyle=':')
# Plot orbit (x-y plane):
P.plot(stage1_x,stage1_y,color='blue')
if STAGE2_COMPUTE:
    P.plot(stage2_x,stage2_y,color='red')
for ii in range(len(annotate_label)):
    P.annotate(annotate_label[ii], (annotate_x[ii],annotate_y[ii]))
P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
P.xlabel('x (km)')
P.ylabel('y (km)')
P.title('Moon-Earth Transit')
if SAVE_PLOTS:
    P.savefig('orbit_m2e_moon_earth.pdf')
else:
    P.show()


print('\nTo find spacecraft state vector at time t, ')
print('enter time in hours between',t_input.to(u.h),'and ',t_total_pg.to(u.h))
input_temp=input('Time: (hr) ')
t_selected=float(input_temp)*u.h


# Determine if t_selected is before or after passage into Earth's SOI
if t_selected<=t_entry:
    # Stage 1 orbit
    delta_t=t_selected-t_input   # Must start from beginning of calculation
    orbit_moon_f=orbit_moon.propagate(delta_t, method=cowell) # Use Cowell for Moon
    r_moon_f=orbit_moon_f.r
    v_moon_f=orbit_moon_f.v
    
    r_selected=(r_moon_f+pos_moon).to(u.m)     # Convert to Earth frame & m
    v_selected=(v_moon_f+vel_moon).to(u.m/u.s) # m/s
    
    x_selected=r_selected[0].value    
    y_selected=r_selected[1].value    
    z_selected=r_selected[2].value    
    vx_selected=v_selected[0].value
    vy_selected=v_selected[1].value
    vz_selected=v_selected[2].value

else:
    # Stage 2 orbit
    delta_t=t_selected-t_entry
    orbit_earth_f=orbit_earth.propagate(delta_t, method=kepler) # Use Kepler for Earth
    r_f=(orbit_earth_f.r).to(u.m)     # Convert to m
    v_f=(orbit_earth_f.v).to(u.m/u.s) # Convert to m/s
    x_selected=r_f[0].value
    y_selected=r_f[1].value
    z_selected=r_f[2].value
    vx_selected=v_f[0].value
    vy_selected=v_f[1].value
    vz_selected=v_f[2].value

    
# Print results:
print('*********************************')
print('  State Vector at Selected Time   \n')
print('To move spacecraft to position, ')
print('read the following to Simulation Director ')
print('for manual entry to input parameter file:')
print('----------------------------------\n')
if FLAG_DELTAV:
    print('delta-v (m/s)                  {:,.1f}'.format(delta_v))
    print('velocity after maneuver (m/s)  {:,.1f}'.format(v_0_mag))
else:
    print('No delta-v.')
    print('initial velocity: (m/s) {:,.1f}'.format(v_0_mag))
print('t   (s)          {:,.0f}'.format(t_selected.to(u.s)))
print('x0  (m)          {:,.0f}'.format(x_selected))
print('y0  (m)          {:,.0f}'.format(y_selected))
print('z0  (m)          {:,.0f}'.format(z_selected))
print('v0x (m/s)        {:,.1f}'.format(vx_selected))
print('v0y (m/s)        {:,.1f}'.format(vy_selected))
print('v0z (m/s)        {:,.1f}'.format(vz_selected))       
print('\nFor further trajectory design,')
print('Copy directly to input parameter file:')
print('--------------------------------------\n')
print('t             {:.0f}'.format(t_selected.to(u.s)))
print('x_0           {:.0f}'.format(x_selected))        
print('y_0           {:.0f}'.format(y_selected))        
print('z_0           {:.0f}'.format(z_selected))        
print('v_0x          {:.1f}'.format(vx_selected))       
print('v_0y          {:.1f}'.format(vy_selected))       
print('v_0z          {:.1f}'.format(vz_selected))       
print('\n')
