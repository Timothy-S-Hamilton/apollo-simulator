from __future__ import division
import pygui
import time
from pygui.gui.widget import Widget
from pygui.gui.label import Label


class Textfield(Widget):
    def __init__(self, x, y, width, height, place_holder_text="", color=pygui.black):
        super(Textfield, self).__init__(x, y, color)
        pygui.add_key_listener(self)
        pygui.add_mouse_listener(self)
        self.width = width
        self.height = height
        self.label = Label(x + 10, y + height / 2, place_holder_text)
        self.label.font_size = self.height - 6
        self.label.color = color
        self.carat = Label(x + 10, y + height / 2, "")
        self.carat.color = self.color
        self.shift_down = False
        self.rect = (x, y, width, height)
        self.label.halign = "left"
        self.timer = time.time()
        self.blink = True
        self.has_focus = False
        self.enter_callback = None
        self.tab_callback = None
        self.max_characters = 20
        self.carat_pos = 0
        if place_holder_text == "":
            self.had_placeholder = False
        else:
            self.had_placeholder = True
            self.label.color = pygui.gray

    def get_text(self):
        if self.had_placeholder:
            return ""
        else:
            return self.label.text

    def update(self):
        self.rect = (self.x, self.y, self.width, self.height)
        self.label.x = self.x + 10
        self.label.y = self.y + self.height / 2

        if self.carat_pos < 0:
            self.carat_pos = 0
        if self.carat_pos > self.max_characters:
            self.carat_pos = self.max_characters

        self.carat.x = self.x + 10 + pygui.get_text_dimensions(self.label.font, self.label.font_size, self.label.text[0:self.carat_pos])[0]
        self.carat.y = self.y + self.height / 2

        if not self.has_focus:
            return

        if self.had_placeholder:
            self.had_placeholder = False
            self.label.text = ""
            self.label.color = pygui.black

        self.shift_down = pygui.is_key_down("left shift")

        if time.time() - self.timer > 0.5:
            self.timer = time.time()
            if self.blink:
                self.carat.text += chr(124)
                self.blink = False
            else:
                self.carat.text = self.carat.text[0:len(self.carat.text)-1]
                self.blink = True

    def render(self):
        if self.visible:
            pygui.draw_rect(self.rect, pygui.white)
            pygui.draw_rect(self.rect, pygui.silver, 3)
            self.label.render()
            self.carat.render()

    def event(self, e_type, e_value):
        if e_type == "MouseDown" and e_value == 1:
            if pygui.contains(self.rect, pygui.get_mouse_position()):
                self.has_focus = True
            else:
                self.has_focus = False
                if not self.blink:
                    self.carat.text = self.carat.text[0:len(self.carat.text)-1]
                    self.blink = True
        if e_type == "MouseUp" and e_value == 1:
            pass

        if not self.has_focus:
            return

        if e_type == "KeyDown":
            if not self.blink:
                self.carat.text = self.carat.text[0:len(self.carat.text)-1]
                self.blink = True

            if e_value == 27:
                self.has_focus = False

            if e_value == 9:
                if self.tab_callback is not None:
                    self.tab_callback()
                self.has_focus = False

            if e_value == 13:
                if self.enter_callback is not None:
                    self.enter_callback()
                self.has_focus = False

            if e_value == 276:  # left arrow
                if self.carat_pos > 0:
                    self.carat_pos -= 1
            if e_value == 275:  # right arrow
                if self.carat_pos < len(self.label.text):
                    self.carat_pos += 1

            if e_value == 8:
                if self.carat_pos == 0:
                    return

                p1 = self.label.text[0:self.carat_pos-1]
                p2 = self.label.text[self.carat_pos:len(self.label.text)]

                if len(p1) <= 0:
                    self.label.text = p2
                else:
                    self.label.text = p1 + p2

                self.carat_pos -= 1
            elif 32 <= e_value <= 126:
                if len(self.label.text) >= self.max_characters:
                    return

                self.carat_pos += 1
                if self.shift_down:
                    if e_value == 48:
                        character = chr(41)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 49:
                        character = chr(33)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 50:
                        character = chr(64)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 51:
                        character = chr(35)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 52:
                        character = chr(36)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 53:
                        character = chr(37)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 54:
                        character = chr(94)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 55:
                        character = chr(38)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 56:
                        character = chr(42)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 57:
                        character = chr(40)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 96:
                        character = chr(126)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 45:
                        character = chr(95)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 61:
                        character = chr(43)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 91:
                        character = chr(123)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 92:
                        character = chr(124)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 93:
                        character = chr(125)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 59:
                        character = chr(58)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 39:
                        character = chr(34)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 44:
                        character = chr(60)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 46:
                        character = chr(62)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    elif e_value == 47:
                        character = chr(63)
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                    else:
                        character = (chr(e_value).upper())
                        self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
                else:
                    character = chr(e_value)
                    self.label.text = self.label.text[0:self.carat_pos-1] + character + self.label.text[self.carat_pos-1:len(self.label.text)]
