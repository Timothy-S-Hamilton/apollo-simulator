#!/usr/bin/env python3

# ORBIT_EPO
# Computes the earth parking orbit
# Stops after one orbit.

import numpy as np
import math
import sys
from astropy import units as u
from poliastro.bodies import Earth, Moon
from poliastro.twobody import Orbit
from poliastro.twobody.propagation import kepler
from poliastro.twobody.propagation import cowell
import matplotlib.pyplot as P

SAVE_PLOTS=0      # 1=save, 0=display

t_incr=15*u.s  # Time increment
t_incr_annotate=10.*u.min     # Time increment for timestamps on plots
t_prep=5.*u.min  # Preparation time before next maneuver (TLI)

# Earth & Moon coordinates
# ------------------------
pos_earth =[0., 0., 0.]*u.km       #  Earth position in km (for drawing)
pos_moon  =[-3.85e5, 0., 0.]*u.km  #  Moon position in km (for drawing & reference frame)
vel_moon  =[0., 0., 0.]*u.km/u.s   #  Moon velocity in km (for reference frame)

x_Earth=pos_earth[0].value # In km
y_Earth=pos_earth[1].value
z_Earth=pos_earth[2].value
x_Moon= pos_moon[0].value
y_Moon= pos_moon[1].value
z_Moon= pos_moon[2].value
R_Earth=6373.296*u.km
R_Moon=1.7375e3*u.km
R_SOI_Moon=6.629e4*u.km            # Radius of Moon's Sphere of Influence in km

# Lists for orbit plotting
stage1_x=[]
stage1_y=[]
stage1_z=[]
annotate_x=[]
annotate_y=[]
annotate_z=[]
annotate_label=[]

if len(sys.argv)==2:        # Command-line option: filename for input parameters
    infilename=sys.argv[1]
    infile=open(infilename, 'r')
    parameters={}                            # Create parameter dictionary
    for param_line in infile:
        if param_line != '\n':               # Skip blank lines
            param_temp = param_line.split()  # If line not blank, split it
            if param_temp[0] != '#':         # If not a comment line (marked with '#')
                param_name = param_temp[0]   # 1st element is parameter name
                param_value= param_temp[1]   # 2nd element is parameter value
                parameters[param_name] = param_value
    infile.close()
    t_input         = float(parameters['t'])*u.s  # must be in seconds
    x_0             = float(parameters['x_0'])    # must be in meters
    y_0             = float(parameters['y_0'])
    z_0             = float(parameters['z_0'])
    v_read_0x       = float(parameters['v_0x'])   # must be in m/s
    v_read_0y       = float(parameters['v_0y'])
    v_read_0z       = float(parameters['v_0z'])

print('t_input=  ',t_input)
print('x_0=      ',x_0)
print('y_0=      ',y_0)
print('z_0=      ',z_0)
print('v_read_0x=',v_read_0x)
print('v_read_0y=',v_read_0y)
print('v_read_0z=',v_read_0z)

# Calculate velocity unit vector
v_read_mag=math.sqrt(v_read_0x**2 + v_read_0y**2 + v_read_0z**2)
v_0x_hat=v_read_0x/v_read_mag
v_0y_hat=v_read_0y/v_read_mag
v_0z_hat=v_read_0z/v_read_mag

#print('Speed before maneuver (m/s): {:.1f}'.format(v_read_mag))
#print('Enter speed after maneuver (m/s) or')
#input_temp=input('hit <enter> for no change:  ')
#try:
#    v_0_mag=float(input_temp)
#    delta_v=v_0_mag-v_read_mag
#    FLAG_DELTAV=1
#except:
#    v_0_mag=v_read_mag
#    FLAG_DELTAV=0

v_0_mag=v_read_mag  # Not using delta-v option now

v_0x=v_0_mag*v_0x_hat
v_0y=v_0_mag*v_0y_hat
v_0z=v_0_mag*v_0z_hat

print("\n\n\n***********************")
print(      "* Earth Parking Orbit *")
print(      "***********************\n")


r_in=[x_0, y_0, z_0]*u.m
v_in=[v_0x, v_0y, v_0z]*u.m/u.s

# Create earth-centered orbit
orbit_earth=Orbit.from_vectors(Earth,r_in,v_in)

# Propagate forward until longitude=0
i=0
delta_t=0
FLAG_LON0=0
while FLAG_LON0==0 and delta_t<200*u.min:  # Upper limit of 200 minutes to search over
    delta_t=i*t_incr
    # 'Future' values end in "_f".
    orbit_earth_f=orbit_earth.propagate(delta_t, method=kepler) # Use Kepler for Earth
    r_f=(orbit_earth_f.r).to(u.km)
    v_f=(orbit_earth_f.v).to(u.km/u.s)
    r_f_mag=math.sqrt((r_f[0].value**2)+(r_f[1].value**2)+(r_f[2].value**2))*u.km
    v_f_mag=math.sqrt((v_f[0].value**2)+(v_f[1].value**2)+(v_f[2].value**2))*u.km/u.s
    
    # Append position to list for plotting        
    stage1_x.append(r_f[0].value)
    stage1_y.append(r_f[1].value)
    stage1_z.append(r_f[2].value)
    
    # Calculate longitude, phi:
    lon=math.atan2(r_f[1].value,r_f[0].value)
    
    if i>0:
        if lon>0 and lon_prev<0: # If current & previous points straddle longitude 0
            i_lon0=i             # go with the current point.
            delta_t_lon0=delta_t
            t_total_lon0=delta_t_lon0+t_input
            r_lon0=r_f
            v_lon0=v_f
            r_lon0_mag=r_f_mag
            v_lon0_mag=v_f_mag
            alt_lon0=r_lon0_mag-R_Earth

            t_min,t_sec=divmod((t_total_lon0.to(u.s)).value,60) # calculates time in hr, min, sec
            t_hr, t_min=divmod(t_min,60)                        # using modulo


            print('\n****************')
            print(  '* TLI maneuver *')
            print(  '****************')
            #print("Total time:         {0:.1f}".format(t_total_lon0.to(u.d))+\
            #          " or {0:.1f}".format(t_total_lon0.to(u.h))+"  or  {0:0.2f}".format(t_total_lon0))
            print('Perform TLI maneuver at time:  {0:02.0f}'.format(t_hr)+':{0:02.0f}'.format(t_min)+':{0:02.0f}'.format(t_sec))
            print('\nSpacecraft will have the following altitude and speed immediately before TLI:')
            print("altitude:           {0:0.5}".format(alt_lon0))
            print("speed:              {0:0.4}".format(v_lon0_mag))
            #if FLAG_DELTAV:
            #    print('delta-v (m/s)    {:,.1f}'.format(delta_v))
            #else:
            #    print('No delta-v.')
            #print('t   (s)          {:,.0f}'.format(t_total_lon0.to(u.s)))
            #print('x0  (m)          {:,.0f}'.format(r_lon0[0].to(u.m)))
            #print('y0  (m)          {:,.0f}'.format(r_lon0[1].to(u.m)))
            #print('z0  (m)          {:,.0f}'.format(r_lon0[2].to(u.m)))
            #print('v0x (m/s)        {:,.1f}'.format(v_lon0[0].to(u.m/u.s)))
            #print('v0y (m/s)        {:,.1f}'.format(v_lon0[1].to(u.m/u.s)))
            #print('v0z (m/s)        {:,.1f}'.format(v_lon0[2].to(u.m/u.s)))
            print('\nTo plan TLI maneuver,')
            print('save the following as parameter file orbit_e2m.txt:')
            print('---------------------------------------------------\n')
            print('t             {:.0f}'.format(t_total_lon0.to(u.s).value))
            print('x_0           {:.0f}'.format(r_lon0[0].to(u.m).value))
            print('y_0           {:.0f}'.format(r_lon0[1].to(u.m).value))
            print('z_0           {:.0f}'.format(r_lon0[2].to(u.m).value))
            print('v_0x          {:.1f}'.format(v_lon0[0].to(u.m/u.s).value))
            print('v_0y          {:.1f}'.format(v_lon0[1].to(u.m/u.s).value))
            print('v_0z          {:.1f}'.format(v_lon0[2].to(u.m/u.s).value))
            print('\n')
            FLAG_LON0=1
    lon_prev=lon
    i=i+1
if FLAG_LON0==0:
    print('\n**************************************************')
    print(  '* FAILED TO REACH LONGITUDE 0 WITHIN 200 MINUTES *')
    print(  '**************************************************\n')

            




# Earth Parking Orbit Plot
# -----------------------
# Draw Earth (x-y plane):
P.clf()
theta_circle=range(361)
earth_x=[(R_Earth.value*math.cos(theta_temp*(math.pi/180.))+x_Earth) for theta_temp in theta_circle]
earth_y=[(R_Earth.value*math.sin(theta_temp*(math.pi/180.))+y_Earth) for theta_temp in theta_circle]
P.plot(earth_x,earth_y,color='green')
# Plot orbit (x-y plane):
P.plot(stage1_x,stage1_y,color='blue')
#for ii in range(len(annotate_label)):
#    P.annotate(annotate_label[ii], (annotate_x[ii],annotate_y[ii]))
P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
P.xlabel('x (km)')
P.ylabel('y (km)')
P.title('Earth Parking Orbit')
if SAVE_PLOTS:
    P.savefig('orbit_epo.pdf')
else:
    P.show()


#print('enter time in minutes between',t_input.to(u.min),'and ',t_total_lon0.to(u.min))
#input_temp=input('Time: (min) ')
#t_selected=float(input_temp)*u.min
t_selected=t_total_lon0-t_prep


delta_t=t_selected-t_input   # Must start from beginning of calculation
orbit_earth_f=orbit_earth.propagate(delta_t, method=kepler) # Use Kepler for Earth
r_f=(orbit_earth_f.r).to(u.m)     # Convert to m
v_f=(orbit_earth_f.v).to(u.m/u.s) # Convert to m/s
x_selected=r_f[0].value
y_selected=r_f[1].value
z_selected=r_f[2].value
vx_selected=v_f[0].value
vy_selected=v_f[1].value
vz_selected=v_f[2].value

    
# Print results:
print('\n*********************************')
print('When Simulation Director is ready to prepare for TLI maneuver,')
print('send the following data to him verbally:')
print('----------------------------------\n')
#if FLAG_DELTAV:
#    print('delta-v (m/s)                  {:,.1f}'.format(delta_v))
#    print('velocity after maneuver (m/s)  {:,.1f}'.format(v_0_mag))
#else:
#    print('No delta-v.')
#    print('initial velocity: (m/s) {:,.1f}'.format(v_0_mag))
print('Preparation time: ',t_prep)
print('t   (s)          {:,.0f}'.format(t_selected.to(u.s)))
print('x0  (m)          {:,.0f}'.format(x_selected))
print('y0  (m)          {:,.0f}'.format(y_selected))
print('z0  (m)          {:,.0f}'.format(z_selected))
print('v0x (m/s)        {:,.1f}'.format(vx_selected))
print('v0y (m/s)        {:,.1f}'.format(vy_selected))
print('v0z (m/s)        {:,.1f}'.format(vz_selected))       
#print('\nFor further trajectory design,')
#print('Copy directly to input parameter file:')
#print('--------------------------------------\n')
#print('t             {:.0f}'.format(t_selected.to(u.s)))
#print('x_0           {:.0f}'.format(x_selected))        
#print('y_0           {:.0f}'.format(y_selected))        
#print('z_0           {:.0f}'.format(z_selected))        
#print('v_0x          {:.1f}'.format(vx_selected))       
#print('v_0y          {:.1f}'.format(vy_selected))       
#print('v_0z          {:.1f}'.format(vz_selected))       
print('\n')
