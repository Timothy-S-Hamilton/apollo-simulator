#define ABORT (13)
#define ABORT_LED (5)
#define ENGINE (7)
#define ENGINE_LED (6)
#define S_IVB_SEP (8)
#define SM_SEP (9)
#define DROGUE (10)
#define MAIN (12)
#define GRADE (11)
#define HEAD (3)
#define ATT_HOLD (4)

#define INPUT_CNT (9)
const uint8_t INPUTS[INPUT_CNT] = {
  ABORT,
  ENGINE,
  S_IVB_SEP,
  SM_SEP,
  DROGUE,
  MAIN,
  GRADE,
  HEAD,
  ATT_HOLD
};

#define DEBOUNCE_DELAY (50)
bool buttonState[INPUT_CNT];
bool lastButtonState[INPUT_CNT];
uint32_t lastDebounceTime[INPUT_CNT];

void debounce(uint8_t index)
{
  bool reading = !digitalRead(INPUTS[index]);
  if(reading != lastButtonState[index])
  {
    lastDebounceTime[index] = millis();
  }
  if((millis() - lastDebounceTime[index]) > DEBOUNCE_DELAY)
  {
    if(reading != buttonState[index])
    {
      buttonState[index] = reading;
    }
  }
  lastButtonState[index] = reading;
}

void setup() 
{
  Serial.begin(115200);
  
  pinMode(ABORT_LED, OUTPUT);
  pinMode(ENGINE_LED, OUTPUT);

  digitalWrite(ABORT_LED, LOW);
  digitalWrite(ENGINE_LED, LOW);
  
  for(uint8_t i = 0; i < INPUT_CNT; i++)
  {
    pinMode(INPUTS[i], INPUT_PULLUP);
    buttonState[i] = lastButtonState[i] = false;
    lastDebounceTime[i] = 0;
  }

  Serial.print("R");
}

void loop() 
{
  for(uint8_t i = 0; i < INPUT_CNT; i++) { debounce(i); }

  while(Serial.available())
  {
    const uint8_t c = Serial.read();

    if(c == '?')
    {
      Serial.print('R');
      break;
    }
    
    const uint8_t cmd = (c & 0xF0) >> 4;
    
    if(cmd == 1)
    {
      uint16_t state_bitfield = 0;
      for(uint8_t i = 0; i < INPUT_CNT; i++)
      {
        state_bitfield |= (buttonState[i] << i);
      }
      
      uint8_t data[2];
      memcpy(data, &state_bitfield, sizeof(uint16_t));
      Serial.write(data, 2);
    }
    else if(cmd == 2)
    {
      digitalWrite(ABORT_LED, c & 1);
      digitalWrite(ENGINE_LED, c & 2);
    }
  }
}
