import sys
import time
import struct
import pygame
import pygui
from networking import *
from datetime import timedelta
from pygui.gui.button import Button
from pygui.gui.label import Label

DEBUG = False

CLIENT_TYPE = 5

met = 0
altitude_above_earth = 0
spacecraft_mass = 0
speed = 0
current_engine_thrust = 0
sphere_of_influence = 0
orbital_eccentricity = 0
altitude_at_periapsis = 0
altitude_at_apoapsis = 0
abort_request_flag = 0
abort_flag = 0
engine_flag = 0
sm_sep_flag = 0
chute_drogue_flag = 0
chute_main_flag = 0
attitude_x = 0
attitude_z = 0

network_location = raw_input("Server IP & Port: ") if len(sys.argv) < 2 else sys.argv[1]

ip = network_location.split(":")[0]
port = int(network_location.split(":")[1])

print "Connecting to:", ip + ":" + str(port)

protocol = ClientNetworkProtocol(ip, port)

def set_met(data):
    global met
    if DEBUG: print "set_met:", data
    met = data[0]

def set_altitude_above_earth(data):
    global altitude_above_earth
    if DEBUG: print "set_altitude_above_earth:", data
    altitude_above_earth = data[0]

def set_spacecraft_mass(data):
    global spacecraft_mass
    if DEBUG: print "set_spacecraft_mass:", data
    spacecraft_mass = data[0]

def set_speed(data):
    global speed
    if DEBUG: print "set_speed:", data
    speed = data[0]

def set_current_engine_thrust(data):
    global current_engine_thrust
    if DEBUG: print "set_current_engine_thrust:", data
    current_engine_thrust = data[0]

def set_sphere_of_influence(data):
    global sphere_of_influence
    if DEBUG: print "set_sphere_of_influence:", data
    sphere_of_influence = data[0]

def set_orbital_eccentricity(data):
    global orbital_eccentricity
    if DEBUG: print "set_orbital_eccentricity:", data
    orbital_eccentricity = data[0]

def set_altitude_at_periapsis(data):
    global altitude_at_periapsis
    if DEBUG: print "set_altitude_at_periapsis:", data
    altitude_at_periapsis = data[0]

def set_altitude_at_apoapsis(data):
    global altitude_at_apoapsis
    if DEBUG: print "set_altitude_at_apoapsis:", data
    altitude_at_apoapsis = data[0]

def set_abort_request_flag(data):
    global abort_request_flag
    if DEBUG: print "set_abort_request_flag:", data
    abort_request_flag = data[0]

def set_abort_flag(data):
    global abort_flag
    if DEBUG: print "set_abort_flag:", data
    abort_flag = data[0]

def set_engine_flag(data):
    global engine_flag
    if DEBUG: print "set_engine_flag:", data
    engine_flag = data[0]

def set_sm_sep_flag(data):
    global sm_sep_flag
    if DEBUG: print "set_sm_sep_flag:", data
    sm_sep_flag = data[0]

def set_chute_drogue_flag(data):
    global chute_drogue_flag
    if DEBUG: print "set_chute_drogue_flag:", data
    chute_drogue_flag = data[0]

def set_chute_main_flag(data):
    global chute_main_flag
    if DEBUG: print "set_chute_main_flag:", data
    chute_main_flag = data[0]

def set_attitude_x(data):
    global attitude_x
    if DEBUG: print "set_attitude_x:", data
    attitude_x = data[0]

def set_attitude_z(data):
    global attitude_z
    if DEBUG: print "set_attitude_z:", data
    attitude_z = data[0]

def client_type_request(data):
    if DEBUG: print "client_type_request:", data
    protocol.transmit_command(1, [CLIENT_TYPE])

protocol.define_receive_command(0, "<I", set_met)
protocol.define_receive_command(1, "<f", set_altitude_above_earth)
protocol.define_receive_command(2, "<f", set_spacecraft_mass)
protocol.define_receive_command(3, "<f", set_speed)
protocol.define_receive_command(4, "<f", set_current_engine_thrust)
protocol.define_receive_command(5, "<B", set_sphere_of_influence)
protocol.define_receive_command(6, "<f", set_orbital_eccentricity)
protocol.define_receive_command(7, "<f", set_altitude_at_periapsis)
protocol.define_receive_command(8, "<f", set_altitude_at_apoapsis)
protocol.define_receive_command(9, "<B", set_abort_request_flag)
protocol.define_receive_command(10, "<B", set_abort_flag)
protocol.define_receive_command(11, "<B", set_engine_flag)
protocol.define_receive_command(12, "<B", set_sm_sep_flag)
protocol.define_receive_command(13, "<B", set_chute_drogue_flag)
protocol.define_receive_command(14, "", client_type_request)
protocol.define_receive_command(15, "<B", set_chute_main_flag)
protocol.define_receive_command(16, "<b", set_attitude_x)
protocol.define_receive_command(17, "<b", set_attitude_z)

protocol.define_transmit_command(1, "<B") # Send Client Type

pygui.init("Retrofire Officer (RETRO)", (1280, 720))

running = True
clock = pygame.time.Clock()

labels = []

last_gen_label = None
def make_label(text, x=None, y=None, l_font_size=32):
    global last_gen_label

    spacing = 5
    x_start = 5
    y_start = 5 if last_gen_label is None else last_gen_label.y + last_gen_label.height + spacing
    
    x_pos = x_start if x is None else x
    y_pos = y_start if y is None else y

    l = Label(x_pos, y_pos, text, font_size=l_font_size)
    l.halign = 'left'
    l.valign = 'top'
    last_gen_label = l
    labels.append(l)
    return l

met_label = make_label("MET: 000:00:00")
altitude_above_earth_label = make_label("Altitude Above Earth (km): 0")
spacecraft_mass_label = make_label("Spacecraft Mass (kg): 0")
speed_label = make_label("Speed (m/s): 0")
current_engine_thrust_label = make_label("Current Engine Thrust (N): 0")
sphere_of_influence_label = make_label("Sphere of Influence: Earth")
orbital_eccentricity_label = make_label("Orbital Eccentricity: 0")
altitude_at_periapsis_label = make_label("Altitude at Periapsis (km): 0")
altitude_at_apoapsis_label = make_label("Altitude at Apoapsis (km): 0")
abort_request_flag_label = make_label("Abort Request: False")
abort_flag_label = make_label("Abort: False")
engine_flag_label = make_label("Engine: Off")
sm_sep_flag_label = make_label("SM Sep: False")
chute_drogue_flag_label = make_label("Drogue Chutes: Stowed")
chute_main_flag_label = make_label("Main Chutes: Stowed")
attitude_x_label = make_label("Attitude X: Prograde")
attitude_z_label = make_label("Attitude Z: Head-Up")

def prefix(s, ch, cnt):
    while len(s) < cnt:
        s = ch + s
    return s

def label_update(label, rhs):
    parts = label.text.split(":")
    label.text = parts[0] + ": " + rhs

done = False
while not done:
    done = pygui.update()

    protocol.update()

    # ------ Update MET Text ------
    td = timedelta(seconds=met)
    s = td.total_seconds()
    hours, remainder = divmod(s, 3600)
    minutes = remainder // 60
    seconds = round(remainder - (minutes * 60))
    hours_str = prefix(str(int(hours)), '0', 3)
    minutes_str = prefix(str(int(minutes)), '0', 2)
    seconds_str = prefix(str(int(seconds)), '0', 2)
    met_label.text = "MET: " + hours_str + ":" + minutes_str + ":" + seconds_str
    # ------ Update MET Text ------

    label_update(altitude_above_earth_label, str(round(altitude_above_earth, 5)))
    label_update(spacecraft_mass_label, str(round(spacecraft_mass, 5)))
    label_update(speed_label, str(round(speed, 5)))
    label_update(current_engine_thrust_label, str(round(current_engine_thrust, 5)))
    label_update(sphere_of_influence_label, "Earth" if sphere_of_influence == 0 else "Moon")
    label_update(orbital_eccentricity_label, str(round(orbital_eccentricity, 5)))
    label_update(altitude_at_periapsis_label, str(round(altitude_at_periapsis, 5)))
    label_update(altitude_at_apoapsis_label, str(round(altitude_at_apoapsis, 5)))
    label_update(abort_request_flag_label, "False" if abort_request_flag == 0 else "True")
    label_update(abort_flag_label, "False" if abort_flag == 0 else "True")
    label_update(engine_flag_label, "Off" if engine_flag == 0 else "On")
    label_update(sm_sep_flag_label, "False" if sm_sep_flag == 0 else "True")
    label_update(chute_drogue_flag_label, "Stowed" if chute_drogue_flag == 0 else "Deployed")
    label_update(chute_main_flag_label, "Stowed" if chute_main_flag == 0 else "Deployed")
    label_update(attitude_x_label, "Prograde" if attitude_x == 1 else "Retrograde")
    label_update(attitude_z_label, "Head-Up" if attitude_z == 1 else "Head-Down")
    
    pygui.clear_screen()

    for l in labels: l.render()

    pygui.display_flip()

protocol.close()
pygui.quit()
