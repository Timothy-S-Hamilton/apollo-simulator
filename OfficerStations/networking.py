import sys
import time
import socket
import struct
import random


class ClientNetworkProtocol(object):
    def __init__(self, ip, port, connect_now=True):
        self.client = SpaceFlightClient((ip, port))
        
        if connect_now and not self.connect():
            print "No Server!"
            sys.exit(1)

        self.transmit_commands = {}
        self.receive_commands = {}
        self.data_buffer = ""
    
    def connect(self):
        try:
            self.client.connect()
        except socket.error:
            return False
        return True

    def define_transmit_command(self, cid, format_str):
        self.transmit_commands[cid] = format_str
    
    def define_receive_command(self, cid, format_str, callback, is_variable=False):
        self.receive_commands[cid] = (format_str, callback, is_variable)
    
    def transmit_command(self, cid, data=[]):
        self.client.transmit(chr(cid) + struct.pack(self.transmit_commands[cid], *data))
    
    def update(self):
        receiving = True
        while receiving:
            success, data, data_len = self.client.receive(64, 0)
            if success and data_len != 0:
                self.data_buffer += data
            receiving = success and data_len != 0
        
        # decode
        while True:
            if len(self.data_buffer) < 1: 
                return

            cid = ord(self.data_buffer[0])
            if cid in self.receive_commands:
                fmt_str, callback, is_variable = self.receive_commands[cid]
                if is_variable:
                    if len(self.data_buffer) < 5:
                        break
                    data_size = struct.unpack("<I", self.data_buffer[1:5])[0] + 5
                else:
                    data_size = struct.calcsize(fmt_str)
                
                if len(self.data_buffer) >= data_size + 1:
                    if is_variable:
                        data = self.data_buffer[5:data_size]
                        callback(data)
                        self.data_buffer = self.data_buffer[data_size:]
                    else:
                        data = struct.unpack(fmt_str, self.data_buffer[1:data_size+1])
                        callback(data)
                        self.data_buffer = self.data_buffer[data_size+1:]
                else:
                    break
            else:
                print "Warning, Unknown Command ID:", cid
                self.data_buffer = self.data_buffer[1:]
    
    def close(self):
        self.client.close()


class SpaceFlightClient(object):
    def __init__(self, location=('localhost', 10000)):
        # store location
        self.location = location
        
        # handle to the socket
        self.handle = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
    def connect(self):
        self.handle.connect(self.location)
        
        # put the socket in non-blocking mode
        self.handle.setblocking(0)

    def transmit(self, message, timeout=0.1):
        start_time = time.time()
        while time.time() - start_time < timeout:
            try:
                self.handle.sendall(message)
                return True
            except socket.error:
                time.sleep(timeout / 10.0)
        return False
    
    def receive(self, amount, timeout=0.1):
        if timeout == 0:
            try:
                data = self.handle.recv(amount)
                return True, data, len(data)
            except socket.error:
                return False, "", 0
        else:
            start_time = time.time()
            while time.time() - start_time < timeout:
                try:
                    data = self.handle.recv(amount)
                    return True, data, len(data)
                except socket.error:
                    time.sleep(timeout / 10.0)
            return False, "", 0

    def close(self):
        self.handle.close()
