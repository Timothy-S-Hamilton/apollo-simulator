from __future__ import division
from pygui.gui.slider import Slider
from pygui.gui.radiobutton import *


class ScrollView(Widget):
    def __init__(self, x, y, width, height, max_height, color):
        super(ScrollView, self).__init__(x, y, color)
        self.widgets = []
        self.width = width
        self.height = height
        self.rect = (self.x, self.y, self.width, self.height)
        self.s = Slider(self.x + self.width - 12, self.y + 10, 10, self.height - 20, True)
        self.s.set_value(0)
        self.translate_y = 0
        self.max_height = max_height

    def update(self):
        self.translate_y = -self.s.get_value() * (self.max_height - self.height)
        self.rect = (self.x, self.y, self.width, self.height)
        self.s.x = self.x + self.s.start_pos[0]
        self.s.y = self.y + self.s.start_pos[1]
        self.s.update()

        for w in self.widgets:
            w.x = self.x + w.start_pos[0]
            w.y = self.y + w.start_pos[1] + self.translate_y

            if isinstance(w, RadioButtonGroup):
                buttons = w.get_buttons()
                for b in buttons:
                    if b.y + b.height > self.y + self.height or b.y - b.size < self.y:
                        b.visible = False
                    else:
                        b.visible = True
            elif isinstance(w, Slider):
                if w.y + w.height > self.y + self.height or w.y - w.v_size < self.y:
                    w.visible = False
                else:
                    w.visible = True
            elif isinstance(w, Label):
                if w.valign == "center":
                    if w.y + int(w.height / 2) > self.y + self.height or w.y - int(w.height / 2) < self.y:
                        w.visible = False
                    else:
                        w.visible = True
                elif w.valign == "top":
                    if w.y > self.y + self.height or w.y < self.y:
                        w.visible = False
                    else:
                        w.visible = True
                elif w.valign == "bottom":
                    if w.y + w.height > self.y + self.height or w.y - w.height < self.y:
                        w.visible = False
                    else:
                        w.visible = True
            else:
                if w.y + w.height > self.y + self.height or w.y < self.y:
                    w.visible = False
                else:
                    w.visible = True
            w.update()

    def render(self):
        for w in self.widgets:
            w.render()

        self.s.render()

    def add_widget(self, w):
        w.x = self.x + w.x
        w.y = self.y + w.y
        if w.y > self.y + self.height:
            w.visible = False
        self.widgets.append(w)

    def remove_widget(self, w):
        self.widgets.remove(w)
