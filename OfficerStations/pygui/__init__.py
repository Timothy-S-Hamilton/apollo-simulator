# Keith Webb

from __future__ import division
import pygame

screen = None
deltaTime = None
clock = None
images = {}
fonts = {}
keys = {}
key_listeners = []
mouse_listeners = []

screen_width = None
screen_height = None

black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
lime = (0, 255, 0)
blue = (0, 0, 255)
gray = (128, 128, 128)
dark_gray = (68, 68, 68)
very_dark_gray = (40, 40, 40)
yellow = (255, 255, 0)
cyan = (0, 255, 255)
magenta = (0, 255, 255)
silver = (192, 192, 192)
maroon = (128, 0, 0)
olive = (128, 128, 0)
green = (0, 128, 0)
purple = (128, 0, 128)
teal = (0, 128, 128)
navy = (0, 0, 128)


def rgb(r, g, b):
    """Creates a tuple of three 0 to 255 bound integers"""
    return r, g, b


def init(title, resolution = (800, 600)):
    """Sets up pygame and pygui"""
    pygame.init()
    pygame.font.init()

    global screen_width
    global screen_height

    screen_width = resolution[0]
    screen_height = resolution[1]

    global screen
    global clock

    if screen is None:
        screen = pygame.display.set_mode(resolution)
        pygame.display.set_caption(title)
    if clock is None:
        clock = pygame.time.Clock()


def quit():
    """Frees data and stops pygame"""
    images.clear()
    fonts.clear()
    pygame.quit()


def update():
    """Updates the internal event structures and delta time, returns a boolean based on if the 'x' was pressed"""
    global deltaTime

    if deltaTime is None:
        deltaTime = 0.0
    else:
        deltaTime = clock.tick() / 1000

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
             return True

        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                for m in mouse_listeners:
                    m.event("MouseDown", 1)
            if event.button == 2:
                for m in mouse_listeners:
                    m.event("MouseDown", 2)
            if event.button == 3:
                for m in mouse_listeners:
                    m.event("MouseDown", 3)

        if event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                for m in mouse_listeners:
                    m.event("MouseUp", 1)
            if event.button == 2:
                for m in mouse_listeners:
                    m.event("MouseUp", 2)
            if event.button == 3:
                for m in mouse_listeners:
                    m.event("MouseUp", 3)

        if event.type == pygame.KEYDOWN:
            for k in key_listeners:
                k.event("KeyDown", event.key)
            keys[pygame.key.name(event.key)] = True
        if event.type == pygame.KEYUP:
            for k in key_listeners:
                k.event("KeyUp", event.key)
            keys[pygame.key.name(event.key)] = False
    return False


def add_key_listener(listener):
    key_listeners.append(listener)


def remove_key_listener(listener):
    key_listeners.remove(listener)


def add_mouse_listener(listener):
    mouse_listeners.append(listener)


def remove_mouse_listener(listener):
    mouse_listeners.remove(listener)

def clear_screen(color=(0, 0, 0)):
    """clears the screen"""
    screen.fill(color)


def display_flip():
    """wrapper for pygame flip function"""
    pygame.display.flip()


def contains(rect, point):
    """checks if point is inside rectangle"""
    (x, y) = point
    (rx, ry, rw, rh) = rect
    if y >= ry and rx <= x <= rx + rw and y <= ry + rh:
        return True

    return False


def is_key_down(key_name):
    """Return True if the key is down, False if Up. Takes a pygame common key name"""
    if key_name in keys:
        return keys[key_name]


def get_mouse_position():
    """Returns a tuple '(x,y)' of the mouse position"""
    return pygame.mouse.get_pos()


def is_mouse_button_down(button_name):
    """Returns True if the button is down, otherwise returns False. Takes 'left', 'middle', or 'right'"""
    pressed = pygame.mouse.get_pressed()

    if button_name == "left" and pressed[0]:
        return True
    elif button_name == "middle" and pressed[1]:
        return True
    elif button_name == "right" and pressed[2]:
        return True
    else:
        return False


def create_image(name, width, height):
    images[name] = pygame.Surface((width, height))


def load_image(name, filename, section=None):
    """Loads an Image from the 'filename' and stores it under the 'name' can take a section to subsurface the image"""
    image = pygame.image.load(filename)

    if section is None:
        images[name] = image
    else:
        images[name] = image.subsurface(section)


def scale_image(name, new_name, scale_x, scale_y):
    """Retrieves 'name' and scales it, then saved it under 'newName'"""
    if name in images:
        images[new_name] = pygame.transform.scale(images[name], (int(scale_x * images[name].get_width()),
                                                                 int(scale_y * images[name].get_height())))
    else:
        print(name, "not loaded!")
        return None


def rotate_image(name, new_name, angle):
    """Retrieves 'name' and rotates it, then saved it under 'newName'"""
    # Code Acquired From https://pygame.org/wiki/RotateCenter
    if name in images:
        orig_rect = images[name].get_rect()
        rot_image = pygame.transform.rotate(images[name], angle)
        rot_rect = orig_rect.copy()
        rot_rect.center = rot_image.get_rect().center
        images[new_name] = rot_image.subsurface(rot_rect).copy()
    else:
        print(name, "not loaded!")
        return None


def get_image(name):
    """Get the direct pygame Surface stored under 'name'"""
    if name in images:
        return images[name]
    else:
        print(name, "not loaded!")
        return None


def draw_image(name, x, y, halign="center", valign="center", target_surf=None):
    """Draws image 'name' at position 'x', 'y' with the provided halign and valign,
    target_surf can be None to use the pygui screen"""
    global screen
    if target_surf is None:
        target_surf = screen

    image = None

    if name in images:
        image = images[name]
    else:
        print(name, "not loaded!")
        return

    if halign == "left":
        final_x = x
    elif halign == "right":
        final_x = x - image.get_width()
    else:
        final_x = x - (image.get_width() / 2)
    if valign == "top":
        final_y = y
    elif valign == "bottom":
        final_y = y - image.get_height()
    else:
        final_y = y - (image.get_height() / 2)

    target_surf.blit(image, (final_x, final_y))


def get_text_dimensions(font_name, size, text):
    if font_name in fonts.keys():
        return fonts[(font_name, size)].size(text)
    else:
        return pygame.font.SysFont(font_name, size).size(text)


def draw_text(font_name, size, text, x, y, color, halign="center", valign="center", background_color=None, target_surf=None, anti_alias=False):
    """Draws font 'fontName' at position 'x', 'y' with the provided halign and valign,
    target_surf can be None to use the pygui screen"""
    if not (font_name, size) in fonts.keys():
        fonts[(font_name, size)] = pygame.font.SysFont(font_name, size)

    global screen
    if target_surf is None:
        target_surf = screen

    rendered_text = None
    if background_color is None:
        rendered_text = fonts[(font_name, size)].render(text, anti_alias, color)
    else:
        rendered_text = fonts[(font_name, size)].render(text, anti_alias, color, background_color)

    if halign == "left":
        final_x = x
    elif halign == "right":
        final_x = x - rendered_text.get_width()
    else:
        final_x = x - (rendered_text.get_width() / 2)
    if valign == "top":
        final_y = y
    elif valign == "bottom":
        final_y = y - rendered_text.get_height()
    else:
        final_y = y - (rendered_text.get_height() / 2)

    target_surf.blit(rendered_text, (final_x, final_y))


def draw_rect(rect, color, width=0, target_surf=None):
    """draw a rectangle shape"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.rect(target_surf, color, rect, width)


def draw_polygon(point_list, color, width=0, target_surf=None):
    """draw a shape with any number of sides"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.polygon(target_surf, color, point_list, width)


def draw_circle(position, radius, color, width=0, target_surf=None):
    """draw a circle around a point"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.circle(target_surf, color, (int(position[0]), int(position[1])), radius, width)


def draw_ellipse(rect, color, width=0, target_surf=None):
    """draw a round shape inside a rectangle"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.ellipse(target_surf, color, rect, width)


def draw_arc(rect, start_angle, stop_angle, color, width=1, target_surf=None):
    """draw a partial section of an ellipse"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.arc(target_surf, color, rect, start_angle, stop_angle, width)


def draw_line(start_position, end_position, color, width=1, target_surf=None):
    """draw a straight line segment"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.line(target_surf, color, start_position, end_position, width)


def draw_lines(point_list, closed_shape, color, width=1, target_surf=None):
    """draw multiple contiguous line segments"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.lines(target_surf, color, closed_shape, point_list, width)


def draw_aaline(start_position, end_position, color, blend=1, target_surf=None):
    """draw fine antialiased lines"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.aaline(target_surf, color, start_position, end_position, blend)


def draw_aalines(point_list, closed_shape, color, blend=1, target_surf=None):
    """draw a connected sequence of antialiased lines"""
    if target_surf is None:
        global screen
        target_surf = screen

    pygame.draw.aalines(target_surf, color, closed_shape, point_list, blend)
