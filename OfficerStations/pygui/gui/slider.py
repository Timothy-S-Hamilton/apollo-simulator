from __future__ import division
import pygui
from pygui.gui.widget import Widget


class Slider(Widget):
    def __init__(self, x, y, width, height, vertical_mode=False, color=pygui.gray):
        super(Slider, self).__init__(x, y, color)
        pygui.add_mouse_listener(self)
        self.width = width
        self.height = height
        self.vertical_mode = vertical_mode
        self.outline_color = pygui.dark_gray
        self.button_color = pygui.gray
        self.button_outline_color = pygui.dark_gray
        self.value = 0.5
        self.v_x = self.x + int(self.width / 2)
        self.v_y = self.y + int(self.height / 2)
        self.v_size = 10
        self.down = False
        self.action = None
        self.sliding_action = None

    def get_value(self):
        return self.value

    def set_value(self, val):
        self.value = val
        if self.vertical_mode:
            self.v_y = int(((self.y + self.height) - self.y) * val + self.y)
        else:
            self.v_x = int(((self.x + self.width) - self.x) * val + self.x)

    def update(self):
        if self.vertical_mode:
            self.v_x = int(self.x) + int(self.width / 2)
            self.v_y = int(((self.y + self.height) - self.y) * self.value + self.y)
        else:
            self.v_x = int(((self.x + self.width) - self.x) * self.value + self.x)
            self.v_y = int(self.y) + int(self.height / 2)

        if self.down:
            if self.vertical_mode:
                my = pygui.get_mouse_position()[1]
                if self.y + self.height >= my >= self.y:
                    self.v_y = my
                if my > self.y + self.height:
                    self.v_y = self.y + self.height
                if my < self.y:
                    self.v_y = self.y
                self.value = ((self.v_y - self.y) / ((self.y + self.height) - self.y))
                if self.sliding_action is not None:
                    self.sliding_action()
            else:
                mx = pygui.get_mouse_position()[0]
                if self.x + self.width >= mx >= self.x:
                    self.v_x = mx
                if mx > self.x + self.width:
                    self.v_x = self.x + self.width
                if mx < self.x:
                    self.v_x = self.x
                self.value = ((self.v_x - self.x) / ((self.x + self.width) - self.x))
                if self.sliding_action is not None:
                    self.sliding_action()

    def render(self):
        if self.visible:
            pygui.draw_rect((self.x, self.y, self.width, self.height), self.color)
            pygui.draw_rect((self.x, self.y, self.width, self.height), self.outline_color, 2)

            pygui.draw_circle((self.v_x, self.v_y), self.v_size, self.button_color)
            pygui.draw_circle((self.v_x, self.v_y), self.v_size + 1, self.button_outline_color, 2)

    def event(self, e_type, e_value):
        if e_type == "MouseDown" and e_value == 1:
            (mx, my) = pygui.get_mouse_position()
            distance = ((mx - self.v_x) ** 2 + (my - self.v_y) ** 2) ** (1 / 2)
            if distance <= self.v_size:
                self.down = True
        if e_type == "MouseUp" and e_value == 1:
            self.down = False
            if self.action is not None:
                self.action()
