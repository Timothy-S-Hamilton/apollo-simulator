import sys
import time
import struct
import pygame
import pygui
from networking import *
from datetime import timedelta
from pygui.gui.button import Button
from pygui.gui.label import Label

DEBUG = False

CLIENT_TYPE = 3

met = 0
abort_request_flag = 0
abort_flag = 0
engine_flag = 0
sep_sivb_flag = 0
sm_sep_flag = 0
chute_drogue_flag = 0
chute_main_flag = 0
attitude_x = 0
attitude_z = 0
att_hold = 0

network_location = raw_input("Server IP & Port: ") if len(sys.argv) < 2 else sys.argv[1]

ip = network_location.split(":")[0]
port = int(network_location.split(":")[1])

print "Connecting to:", ip + ":" + str(port)

protocol = ClientNetworkProtocol(ip, port)

def set_met(data):
    global met
    if DEBUG: print "set_met:", data
    met = data[0]

def set_abort_request_flag(data):
    global abort_request_flag
    if DEBUG: print "set_abort_request_flag:", data
    abort_request_flag = data[0]

def set_abort_flag(data):
    global abort_flag
    if DEBUG: print "set_abort_flag:", data
    abort_flag = data[0]

def set_engine_flag(data):
    global engine_flag
    if DEBUG: print "set_engine_flag:", data
    engine_flag = data[0]

def set_sep_sivb_flag(data):
    global sep_sivb_flag
    if DEBUG: print "set_sep_sivb_flag:", data
    sep_sivb_flag = data[0]

def set_sm_sep_flag(data):
    global sm_sep_flag
    if DEBUG: print "set_sm_sep_flag:", data
    sm_sep_flag = data[0]

def set_chute_drogue_flag(data):
    global chute_drogue_flag
    if DEBUG: print "set_chute_drogue_flag:", data
    chute_drogue_flag = data[0]

def set_chute_main_flag(data):
    global chute_main_flag
    if DEBUG: print "set_chute_main_flag:", data
    chute_main_flag = data[0]

def set_attitude_x(data):
    global attitude_x
    if DEBUG: print "set_attitude_x:", data
    attitude_x = data[0]

def set_attitude_z(data):
    global attitude_z
    if DEBUG: print "set_attitude_z:", data
    attitude_z = data[0]

def set_att_hold(data):
    global att_hold
    if DEBUG: print "set_att_hold:", data
    att_hold = data[0]

def client_type_request(data):
    if DEBUG: print "client_type_request:", data
    protocol.transmit_command(1, [CLIENT_TYPE])

protocol.define_receive_command(0, "<I", set_met)
protocol.define_receive_command(1, "<B", set_abort_request_flag)
protocol.define_receive_command(2, "<B", set_abort_flag)
protocol.define_receive_command(3, "<B", set_engine_flag)
protocol.define_receive_command(4, "<B", set_sep_sivb_flag)
protocol.define_receive_command(5, "<B", set_sm_sep_flag)
protocol.define_receive_command(6, "<B", set_chute_drogue_flag)
protocol.define_receive_command(7, "<B", set_chute_main_flag)
protocol.define_receive_command(8, "<b", set_attitude_x)
protocol.define_receive_command(9, "<b", set_attitude_z)
protocol.define_receive_command(10, "<B", set_att_hold)

protocol.define_transmit_command(1, "<B") # Send Client Type
protocol.define_receive_command(14, "", client_type_request)

pygui.init("Guidance, Navigation, and Control Officer (GNC)", (1280, 720))

running = True
clock = pygame.time.Clock()

labels = []

last_gen_label = None
def make_label(text, x=None, y=None, l_font_size=32):
    global last_gen_label

    spacing = 5
    x_start = 5
    y_start = 5 if last_gen_label is None else last_gen_label.y + last_gen_label.height + spacing
    
    x_pos = x_start if x is None else x
    y_pos = y_start if y is None else y

    l = Label(x_pos, y_pos, text, font_size=l_font_size)
    l.halign = 'left'
    l.valign = 'top'
    last_gen_label = l
    labels.append(l)
    return l

met_label = make_label("MET: 000:00:00")
abort_request_flag_label = make_label("Abort Request: False")
abort_flag_label = make_label("Abort: False")
engine_flag_label = make_label("Engine: Off")
sep_sivb_flag_label = make_label("S-IVB Sep: False")
sm_sep_flag_label = make_label("SM Sep: False")
chute_drogue_flag_label = make_label("Drogue Chutes: Stowed")
chute_main_flag_label = make_label("Main Chutes: Stowed")
attitude_x_label = make_label("Attitude X: Prograde")
attitude_z_label = make_label("Attitude Z: Head-Up")
att_hold_label = make_label("Attitude Hold: False")

def prefix(s, ch, cnt):
    while len(s) < cnt:
        s = ch + s
    return s

def label_update(label, rhs):
    parts = label.text.split(":")
    label.text = parts[0] + ": " + rhs

done = False
while not done:
    done = pygui.update()

    protocol.update()

    # ------ Update MET Text ------
    td = timedelta(seconds=met)
    s = td.total_seconds()
    hours, remainder = divmod(s, 3600)
    minutes = remainder // 60
    seconds = round(remainder - (minutes * 60))
    hours_str = prefix(str(int(hours)), '0', 3)
    minutes_str = prefix(str(int(minutes)), '0', 2)
    seconds_str = prefix(str(int(seconds)), '0', 2)
    met_label.text = "MET: " + hours_str + ":" + minutes_str + ":" + seconds_str
    # ------ Update MET Text ------

    label_update(abort_request_flag_label, "False" if abort_request_flag == 0 else "True")
    label_update(abort_flag_label, "False" if abort_flag == 0 else "True")
    label_update(engine_flag_label, "Off" if engine_flag == 0 else "On")
    label_update(sep_sivb_flag_label, "False" if sep_sivb_flag == 0 else "True")
    label_update(sm_sep_flag_label, "False" if sm_sep_flag == 0 else "True")
    label_update(chute_drogue_flag_label, "Stowed" if chute_drogue_flag == 0 else "Deployed")
    label_update(chute_main_flag_label, "Stowed" if chute_main_flag == 0 else "Deployed")
    label_update(attitude_x_label, "Prograde" if attitude_x == 1 else "Retrograde")
    label_update(attitude_z_label, "Head-Up" if attitude_z == 1 else "Head-Down")
    label_update(att_hold_label, "True" if att_hold == 1 else "False")

    pygui.clear_screen()

    for l in labels: l.render()

    pygui.display_flip()

protocol.close()
pygui.quit()
