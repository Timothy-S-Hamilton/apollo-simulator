from __future__ import division
import pygui
from pygui.gui.widget import Widget


class Label(Widget):
    def __init__(self, x, y, text, font="Arial", font_size=16, color=pygui.white):
        super(Label, self).__init__(x, y, color)
        self.text = text
        self.start_text = text
        self.font = font
        self.font_size = font_size
        (w, h) = pygui.get_text_dimensions(self.font, self.font_size, self.text)
        self.width = w
        self.height = h
        self.start_font_size = font_size
        self.valign = "center"
        self.halign = "center"

    def render(self):
        if self.visible:
            if self.font_size != self.start_font_size or self.text != self.start_text:
                (w, h) = pygui.get_text_dimensions(self.font, self.font_size, self.text)
                self.width = w
                self.height = h
                self.start_font_size = self.font_size
                self.start_text = self.text

            pygui.draw_text(self.font, self.font_size, self.text, self.x, self.y, self.color, self.halign, self.valign, anti_alias=True)
