import sys
import time
import struct
import pygame
import pygui
from networking import *
from datetime import timedelta
from pygui.gui.button import Button
from pygui.gui.label import Label

DEBUG = False

CLIENT_TYPE = 4

met = 0
spacecraft_mass = 0
current_engine_thrust = 0
sic_fi_1_flag = 1
sic_fi_2_flag = 1
sic_fi_3_flag = 1
sic_fi_4_flag = 1
sic_fi_5_flag = 1
sep_sic_flag = 0
sic_j2_1_flag = 1
sic_j2_2_flag = 1
sic_j2_3_flag = 1
sic_j2_4_flag = 1
sic_j2_5_flag = 1
sep_sii_flag = 0
sivb_j2_1_flag = 0
sep_sivb_flag = 0

network_location = raw_input("Server IP & Port: ") if len(sys.argv) < 2 else sys.argv[1]

ip = network_location.split(":")[0]
port = int(network_location.split(":")[1])

print "Connecting to:", ip + ":" + str(port)

protocol = ClientNetworkProtocol(ip, port)

def set_met(data):
    global met
    if DEBUG: print "set_met:", data
    met = data[0]

def set_spacecraft_mass(data):
    global spacecraft_mass
    if DEBUG: print "set_spacecraft_mass:", data
    spacecraft_mass = data[0]

def set_current_engine_thrust(data):
    global current_engine_thrust
    if DEBUG: print "set_current_engine_thrust:", data
    current_engine_thrust = data[0]

def set_sic_fi_1_flag(data):
    global sic_fi_1_flag
    if DEBUG: print "set_sic_fi_1_flag:", data
    sic_fi_1_flag = data[0]

def set_sic_fi_2_flag(data):
    global sic_fi_2_flag
    if DEBUG: print "set_sic_fi_2_flag:", data
    sic_fi_2_flag = data[0]

def set_sic_fi_3_flag(data):
    global sic_fi_3_flag
    if DEBUG: print "set_sic_fi_3_flag:", data
    sic_fi_3_flag = data[0]

def set_sic_fi_4_flag(data):
    global sic_fi_4_flag
    if DEBUG: print "set_sic_fi_4_flag:", data
    sic_fi_4_flag = data[0]

def set_sic_fi_5_flag(data):
    global sic_fi_5_flag
    if DEBUG: print "set_sic_fi_5_flag:", data
    sic_fi_5_flag = data[0]

def set_sep_sic_flag(data):
    global sep_sic_flag
    if DEBUG: print "set_sep_sic_flag:", data
    sep_sic_flag = data[0]

def set_sic_j2_1_flag(data):
    global sic_j2_1_flag
    if DEBUG: print "set_sic_j2_1_flag:", data
    sic_j2_1_flag = data[0]

def set_sic_j2_2_flag(data):
    global sic_j2_2_flag
    if DEBUG: print "set_sic_j2_2_flag:", data
    sic_j2_2_flag = data[0]

def set_sic_j2_3_flag(data):
    global sic_j2_3_flag
    if DEBUG: print "set_sic_j2_3_flag:", data
    sic_j2_3_flag = data[0]

def set_sic_j2_4_flag(data):
    global sic_j2_4_flag
    if DEBUG: print "set_sic_j2_4_flag:", data
    sic_j2_4_flag = data[0]

def set_sic_j2_5_flag(data):
    global sic_j2_5_flag
    if DEBUG: print "set_sic_j2_5_flag:", data
    sic_j2_5_flag = data[0]

def set_sep_sii_flag(data):
    global sep_sii_flag
    if DEBUG: print "set_sep_sii_flag:", data
    sep_sii_flag = data[0]

def set_sivb_j2_1_flag(data):
    global sivb_j2_1_flag
    if DEBUG: print "set_sivb_j2_1_flag:", data
    sivb_j2_1_flag = data[0]

def set_sep_sivb_flag(data):
    global sep_sivb_flag
    if DEBUG: print "set_sep_sivb_flag:", data
    sep_sivb_flag = data[0]

def client_type_request(data):
    if DEBUG: print "client_type_request:", data
    protocol.transmit_command(1, [CLIENT_TYPE])

protocol.define_receive_command(0, "<I", set_met)
protocol.define_receive_command(1, "<f", set_spacecraft_mass)
protocol.define_receive_command(2, "<f", set_current_engine_thrust)
protocol.define_receive_command(3, "<B", set_sic_fi_1_flag)
protocol.define_receive_command(4, "<B", set_sic_fi_2_flag)
protocol.define_receive_command(5, "<B", set_sic_fi_3_flag)
protocol.define_receive_command(6, "<B", set_sic_fi_4_flag)
protocol.define_receive_command(7, "<B", set_sic_fi_5_flag)
protocol.define_receive_command(8, "<B", set_sep_sic_flag)
protocol.define_receive_command(9, "<B", set_sic_j2_1_flag)
protocol.define_receive_command(10, "<B", set_sic_j2_2_flag)
protocol.define_receive_command(11, "<B", set_sic_j2_3_flag)
protocol.define_receive_command(12, "<B", set_sic_j2_4_flag)
protocol.define_receive_command(13, "<B", set_sic_j2_5_flag)
protocol.define_receive_command(14, "", client_type_request)
protocol.define_receive_command(15, "<B", set_sep_sii_flag)
protocol.define_receive_command(16, "<B", set_sivb_j2_1_flag)
protocol.define_receive_command(17, "<B", set_sep_sivb_flag)

protocol.define_transmit_command(1, "<B") # Send Client Type

pygui.init("Booster Officer (BOOST)", (1280, 720))

running = True
clock = pygame.time.Clock()

labels = []

last_gen_label = None
def make_label(text, x=None, y=None, l_font_size=32):
    global last_gen_label

    spacing = 5
    x_start = 5
    y_start = 5 if last_gen_label is None else last_gen_label.y + last_gen_label.height + spacing
    
    x_pos = x_start if x is None else x
    y_pos = y_start if y is None else y

    l = Label(x_pos, y_pos, text, font_size=l_font_size)
    l.halign = 'left'
    l.valign = 'top'
    last_gen_label = l
    labels.append(l)
    return l

met_label = make_label("MET: 000:00:00")
spacecraft_mass_label = make_label("Spacecraft Mass (kg): 0")
current_engine_thrust_label = make_label("Current Engine Thrust (N): 0")
sic_fi_1_flag_label = make_label("S-IC Engine 1: Off")
sic_fi_2_flag_label = make_label("S-IC Engine 2: Off")
sic_fi_3_flag_label = make_label("S-IC Engine 3: Off")
sic_fi_4_flag_label = make_label("S-IC Engine 4: Off")
sic_fi_5_flag_label = make_label("S-IC Engine 5: Off")
sep_sic_flag_label = make_label("S-IC SEP: False")
sic_j2_1_flag_label = make_label("S-II Engine 1: Off")
sic_j2_2_flag_label = make_label("S-II Engine 2: Off")
sic_j2_3_flag_label = make_label("S-II Engine 3: Off")
sic_j2_4_flag_label = make_label("S-II Engine 4: Off")
sic_j2_5_flag_label = make_label("S-II Engine 5: Off")
sep_sii_flag_label = make_label("S-II SEP: False")
sivb_j2_1_flag_label = make_label("S-IVB Engine: Off")
sep_sivb_flag_label = make_label("S-IVB SEP: False")

def prefix(s, ch, cnt):
    while len(s) < cnt:
        s = ch + s
    return s

def label_update(label, rhs):
    parts = label.text.split(":")
    label.text = parts[0] + ": " + rhs

done = False
while not done:
    done = pygui.update()

    protocol.update()

    # ------ Update MET Text ------
    td = timedelta(seconds=met)
    s = td.total_seconds()
    hours, remainder = divmod(s, 3600)
    minutes = remainder // 60
    seconds = round(remainder - (minutes * 60))
    hours_str = prefix(str(int(hours)), '0', 3)
    minutes_str = prefix(str(int(minutes)), '0', 2)
    seconds_str = prefix(str(int(seconds)), '0', 2)
    met_label.text = "MET: " + hours_str + ":" + minutes_str + ":" + seconds_str
    # ------ Update MET Text ------

    label_update(spacecraft_mass_label, str(round(spacecraft_mass, 5)))
    label_update(current_engine_thrust_label, str(round(current_engine_thrust, 5)))
    label_update(sic_fi_1_flag_label, "On" if sic_fi_1_flag == 0 else "Off")
    label_update(sic_fi_2_flag_label, "On" if sic_fi_2_flag == 0 else "Off")
    label_update(sic_fi_3_flag_label, "On" if sic_fi_3_flag == 0 else "Off")
    label_update(sic_fi_4_flag_label, "On" if sic_fi_4_flag == 0 else "Off")
    label_update(sic_fi_5_flag_label, "On" if sic_fi_5_flag == 0 else "Off")
    label_update(sep_sic_flag_label, "False" if sep_sic_flag == 0 else "True")
    label_update(sic_j2_1_flag_label, "On" if sic_j2_1_flag == 0 else "Off")
    label_update(sic_j2_2_flag_label, "On" if sic_j2_2_flag == 0 else "Off")
    label_update(sic_j2_3_flag_label, "On" if sic_j2_3_flag == 0 else "Off")
    label_update(sic_j2_4_flag_label, "On" if sic_j2_4_flag == 0 else "Off")
    label_update(sic_j2_5_flag_label, "On" if sic_j2_5_flag == 0 else "Off")
    label_update(sep_sii_flag_label, "False" if sep_sii_flag == 0 else "True")
    label_update(sivb_j2_1_flag_label, "On" if sivb_j2_1_flag == 0 else "Off")
    label_update(sep_sivb_flag_label, "False" if sep_sivb_flag == 0 else "True")

    pygui.clear_screen()

    for l in labels: l.render()

    pygui.display_flip()

protocol.close()
pygui.quit()
