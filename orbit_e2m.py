#!/usr/bin/env python3

# ORBIT_E2M
# Computes the trajectory from Earth to Moon.
# Stops when it hits pericynthion.

# 10930 m/s is a very good post-TLI velocity!

import numpy as np
import math
import sys
from astropy import units as u
from poliastro.bodies import Earth, Moon
from poliastro.twobody import Orbit
from poliastro.twobody.propagation import kepler
from poliastro.twobody.propagation import cowell
import matplotlib.pyplot as P

#from poliastro.plotting import plot
#P.style.use("seaborn")
#P.ion()  # Show plots immediately

SAVE_PLOTS=0      # 1=save, 0=display


# Flags for which parts of the program to compute
STAGE1_COMPUTE=1  # Compute Earth -> Moon SOI leg?
STAGE2_COMPUTE=1  # Compute Moon SOI -> PC leg?

t_incr_stage1=1.*u.min  # Time increment
t_incr_stage2=1.*u.min  # Time increment
t_entry=0.*u.min        # Default time for entering Moon SOI
                        # in case STAGE1 is not computed.
t_incr_annotate=120.*u.min     # Time increment for timestamps on plots

# Earth & Moon coordinates
# ------------------------
pos_earth =[0., 0., 0.]*u.km       #  Earth position in km (for drawing)
pos_moon  =[-3.85e5, 0., 0.]*u.km  #  Moon position in km (for drawing & reference frame)
vel_moon  =[0., 0., 0.]*u.km/u.s   #  Moon velocity in km (for reference frame)

x_Earth=pos_earth[0].value # In km
y_Earth=pos_earth[1].value
z_Earth=pos_earth[2].value
x_Moon= pos_moon[0].value
y_Moon= pos_moon[1].value
z_Moon= pos_moon[2].value
R_Earth=6373.296*u.km
R_Moon=1.7375e3*u.km
R_SOI_Moon=6.629e4*u.km            # Radius of Moon's Sphere of Influence in km


# Lists for orbit plotting
stage1_x=[]
stage1_y=[]
stage1_z=[]
stage2_moonframe_x=[]
stage2_moonframe_y=[]
stage2_moonframe_z=[]
stage2_x=[]
stage2_y=[]
stage2_z=[]
annotate_x=[]
annotate_y=[]
annotate_z=[]
annotate_label=[]

if len(sys.argv)==2:        # Command-line option: filename for input parameters
    infilename=sys.argv[1]
    infile=open(infilename, 'r')
    parameters={}                            # Create parameter dictionary
    for param_line in infile:
        if param_line != '\n':               # Skip blank lines
            param_temp = param_line.split()  # If line not blank, split it
            if param_temp[0] != '#':         # If not a comment line (marked with '#')
                param_name = param_temp[0]   # 1st element is parameter name
                param_value= param_temp[1]   # 2nd element is parameter value
                parameters[param_name] = param_value
    infile.close()
    t_input         = float(parameters['t'])*u.s  # must be in seconds
    x_0             = float(parameters['x_0'])    # must be in meters
    y_0             = float(parameters['y_0'])
    z_0             = float(parameters['z_0'])
    v_read_0x       = float(parameters['v_0x'])   # must be in m/s
    v_read_0y       = float(parameters['v_0y'])
    v_read_0z       = float(parameters['v_0z'])

print('t_input=  ',t_input)
print('x_0=      ',x_0)
print('y_0=      ',y_0)
print('z_0=      ',z_0)
print('v_read_0x=',v_read_0x)
print('v_read_0y=',v_read_0y)
print('v_read_0z=',v_read_0z)

# Calculate velocity unit vector
v_read_mag=math.sqrt(v_read_0x**2 + v_read_0y**2 + v_read_0z**2)
v_0x_hat=v_read_0x/v_read_mag
v_0y_hat=v_read_0y/v_read_mag
v_0z_hat=v_read_0z/v_read_mag

print('Speed before maneuver (m/s): {:.1f}'.format(v_read_mag))
print('Enter speed after maneuver (m/s) or')
input_temp=input('hit <enter> for no change:  ')
try:
    v_0_mag=float(input_temp)
    delta_v=v_0_mag-v_read_mag
    FLAG_DELTAV=1
except:
    v_0_mag=v_read_mag
    FLAG_DELTAV=0

v_0x=v_0_mag*v_0x_hat
v_0y=v_0_mag*v_0y_hat
v_0z=v_0_mag*v_0z_hat

print("\n\n\n*********************")
print(      "* Trajectory Design *")
print(      "*********************\n\n\n")


# Earth -> Moon SOI leg
# ---------------------
if STAGE1_COMPUTE:
    
    #speed_in=10926.
    #speed_in=10930.
    #r_in=[6555000., 0., 0.]*u.m
    #v_in=[0., speed_in, 0.]*u.m/u.s
    
    r_in=[x_0, y_0, z_0]*u.m
    v_in=[v_0x, v_0y, v_0z]*u.m/u.s
    
    
    # Initial orbit around Earth
    orbit_earth_leave=Orbit.from_vectors(Earth,r_in,v_in)
    ecc_earth =orbit_earth_leave.ecc
    sma_earth =orbit_earth_leave.a
    inc_earth =orbit_earth_leave.inc
    argp_earth=orbit_earth_leave.argp
    nu_earth  =orbit_earth_leave.nu
    raan_earth=orbit_earth_leave.raan
    
    print("\n***************************************")
    print("          Earth Orbit after TLI")
    print("")
    print("Input vectors:")
    print("r=",orbit_earth_leave.r)
    print("v=",orbit_earth_leave.v)
    print("")
    #print("Initial Keplerian elements at time t=",orbit_earth_leave.epoch.iso)
    print("Initial Keplerian elements at time t=",t_input)
    print("Eccentricity:       {0:.6}".format(ecc_earth))
    print("Semi-major axis:    {0:.6}".format(sma_earth))
    print("Inclination:        {0:.4}".format(inc_earth.to(u.deg)))
    print("Arg of perigee      {0:.4}".format(argp_earth.to(u.deg)))
    print("RAAN:               {0:.4}".format(raan_earth.to(u.deg)))
    print("True anomaly:       {0:.4}".format(nu_earth.to(u.deg)))
    print("***************************************\n")
    
    
    
    r_entry=0  # r & v at entry into Moon's SOI
    v_entry=0
    FLAG_SOI=0
    
    # Iterate over time steps and calculate new positions
    i=0
    delta_t=0.
    while FLAG_SOI==0 and delta_t<6*u.d:
        
        delta_t=i*t_incr_stage1
        
        # 'Future' values end in "_f".
        orbit_earth_leave_f=orbit_earth_leave.propagate(delta_t, method=kepler)
        r_f=orbit_earth_leave_f.r
        v_f=orbit_earth_leave_f.v
        r_f_mag=math.sqrt((r_f[0].value**2)+(r_f[1].value**2)+(r_f[2].value**2))*u.km
        v_f_mag=math.sqrt((v_f[0].value**2)+(v_f[1].value**2)+(v_f[2].value**2))*u.km/u.s
        
        # Append position to list for plotting
        stage1_x.append(r_f[0].value)
        stage1_y.append(r_f[1].value)
        stage1_z.append(r_f[2].value)
        
        if (t_input+delta_t)%t_incr_annotate==0:
            annotate_x.append(r_f[0].value)
            annotate_y.append(r_f[1].value)
            annotate_z.append(r_f[2].value)
            annotate_label.append(str((t_input+delta_t).to(u.hr)))
        
        
        # Calculate distance from moon:
        r_moon=math.sqrt(((r_f[0].value-pos_moon[0].value)**2)+((r_f[1].value-pos_moon[1].value)**2)+((r_f[2].value-pos_moon[2].value)**2))*u.km
        
        if r_moon<R_SOI_Moon: # If within the Moon's Sphere of Influence?
            FLAG_SOI=1
            r_entry=r_f
            v_entry=v_f
            v_mag_entry=v_f_mag
            t_entry=(t_input+delta_t)
            break
        
        # print("{0:.1f}".format(delta_t.to(u.d)), delta_t,"  {0:0.4}".format(r_f_mag), "  {0:0.4}".format(v_f_mag),"  {0:0.4}".format(r_moon))
        i=i+1
    if not FLAG_SOI:
        print('\n******************************************')
        print('* FAILED TO REACH MOON SOI WITHIN 6 DAYS *')
        print('******************************************\n')
        STAGE2_COMPUTE=0
    else:
        print("\n***************************************")
        print(" Entry into Moon's Sphere of Influence:\n")
        print('time since TLI:       {0:.1f}'.format(t_entry.to(u.d))+\
                  ' or {0:.1f}'.format(t_entry.to(u.h))+' or {0:0.2f}'.format(t_entry))
        print('speed (Earth frame):  {0:.3f}'.format(v_mag_entry))
        print("")
        #print('r (Earth frame):      ',r_entry)
        #print('v (Earth frame):      ',v_entry)
        #print("")



# Moon SOI -> Pericynthion leg
# ----------------------------
if not STAGE1_COMPUTE:
    # If not computing Moon SOI entry in STAGE1, 
    # use values computed in earlier runs
    #r_entry=[-323681.81015511, 24771.31347745, 0.]*u.km
    #v_entry=[-0.42468463, -0.18876542, 0.]*u.km/u.s
    r_entry=[-325743.09158658, 29668.53308306, 0.]*u.km
    v_entry=[-0.50462861, -0.17398545, -0.]*u.km/u.s
    t_entry=4003.0*u.min
    print("")
    print("       Entry point into Moon's SOI\n")
    print("Not computing Stage 1--Using pre-computed r & v.\n")
    print("r (Earth frame):  ",r_entry)
    print("v (Earth frame):  ",v_entry)
    print("***************************************\n")
    
if STAGE2_COMPUTE:
    # Convert entry coordinates into Moon's frame of reference
    # --------------------------------------------------------
    r_m_in=r_entry-pos_moon # Position in Moon frame
    v_m_in=v_entry-vel_moon # Velocity in Moon frame
    #print("r (Moon frame):  ",r_m_in)
    #print("v (Moon frame):  ",v_m_in)
    #print("***************************************\n")

    
    # Create an orbit relative to the Moon
    orbit_moon_enter=Orbit.from_vectors(Moon,r_m_in,v_m_in)
    
    ecc =orbit_moon_enter.ecc
    sma =orbit_moon_enter.a
    inc =orbit_moon_enter.inc
    nu  =orbit_moon_enter.nu
    argp=orbit_moon_enter.argp
    #r_confirm=orbit_moon_enter.r  # Use these if you need to check that this orbit
    #v_confirm=orbit_moon_enter.v  # is patched correctly to the Earth-centered one
    
    mu_moon = (6.674e-11)*(7.3459e22)                                             # G*M_Moon
    sma_val = sma.to(u.m).value                                                   # SMA in m (used in PC computation)
    r_pc_predict   = orbit_moon_enter.a*(1.-orbit_moon_enter.ecc)                 # Pericynthion radius
    alt_pc_predict = r_pc_predict-R_Moon                                          # Pericynthion altitude
    v_pc_predict   = math.sqrt(((-1.*mu_moon/sma_val)*(1.+ecc))/(ecc-1.))*u.m/u.s # Pericynthion speed
    

    #print("Moon-centered Keplerian elements at time t=",orbit_moon_enter.epoch.iso)
    print("Moon-centered Keplerian elements at SOI entry\n")
    print("Eccentricity:              {0:.5}".format(ecc))
    print("Semi-major axis:           {0:.6}".format(sma))
    print("Inclination:               {0:.4}".format(inc.to(u.deg)))
    print("Arg of pericynthion        {0:.4}".format(argp.to(u.deg)))
    print("True anomaly at SOI entry: {0:.4}".format(nu.to(u.deg)))
    #print("r  =",r_confirm)
    #print("v  =",v_confirm)
    print("")
    print("Predicted Pericynthion (PC)")
    print("altitude:                  {0:.5}".format(alt_pc_predict))
    print("radius:                    {0:.6}".format(r_pc_predict))
    print("speed:                     {0:.4}".format(v_pc_predict.to(u.km/u.s)))
    print("***************************************\n")
    
    
    # Propagate forward to find time of PC passage:
    i=0
    delta_t=0.
    FLAG_PC=0
    while FLAG_PC==0 and delta_t<3*u.d: # Upper limit of 3 days to search over
        delta_t=i*t_incr_stage2
        # 'Future' values end in "_f".
        orbit_moon_pc_f=orbit_moon_enter.propagate(delta_t, method=cowell) # Use Cowell for Moon
        #orbit_moon_pc_f=orbit_moon_enter.propagate(delta_t, method=kepler) # Use Kepler for Earth

        r_f=orbit_moon_pc_f.r
        v_f=orbit_moon_pc_f.v
        nu_f=orbit_moon_pc_f.nu
        argp_f=orbit_moon_pc_f.argp
        r_f_mag=math.sqrt((r_f[0].value**2)+(r_f[1].value**2)+(r_f[2].value**2))*u.km
        v_f_mag=math.sqrt((v_f[0].value**2)+(v_f[1].value**2)+(v_f[2].value**2))*u.km/u.s
        #if i==0:
        #    print("\n\n")
        #    print("Propagating: Initial value at {0:.1f}".format(delta_t.to(u.d)), delta_t)
        #    print("r=",r_f)
        #    print("v=",v_f)
        #    print("\n\n")
        
        # Append position to list for plotting
        stage2_moonframe_x.append(r_f[0].value)
        stage2_moonframe_y.append(r_f[1].value)
        stage2_moonframe_z.append(r_f[2].value)
        stage2_x.append(r_f[0].value+x_Moon)
        stage2_y.append(r_f[1].value+y_Moon)
        stage2_z.append(r_f[2].value+z_Moon)

        if (delta_t+t_entry)%t_incr_annotate==0:
            annotate_x.append(r_f[0].value+x_Moon)
            annotate_y.append(r_f[1].value+y_Moon)
            annotate_z.append(r_f[2].value+z_Moon)
            annotate_label.append(str((delta_t+t_entry).to(u.hr).value))

        
        if i>0:  # Have to do one iteration first, to have a "previous" distance 
                 # to compare with & tell if you're getting closer or farther.
            if r_f_mag>r_mag_prev:      # If you're getting farther away now...
                i_pc=i-1                # The previous iteration is the closest one
                delta_t_pc=i_pc*t_incr_stage2  # Time to PC since entering Moon SOI
                t_total_pc=delta_t_pc+t_entry  # Total time to PC from beginning of Stage 1
                orbit_moon_pc=orbit_moon_enter.propagate(delta_t_pc, method=cowell)
                r_pc=orbit_moon_pc.r         # Get orbital elements & state vectors at PC
                v_pc=orbit_moon_pc.v         
                nu_pc=orbit_moon_pc.nu       # True anomaly
                argp_pc=orbit_moon_pc.argp   # Argument of Pericynthion
                raan_pc=orbit_moon_pc.raan   # Right Ascenscion of Ascending Node
                r_pc_mag=math.sqrt((r_pc[0].value**2)+(r_pc[1].value**2)+(r_pc[2].value**2))*u.km
                v_pc_mag=math.sqrt((v_pc[0].value**2)+(v_pc[1].value**2)+(v_pc[2].value**2))*u.km/u.s
                alt_pc=r_pc_mag-R_Moon       # Pericynthion altitude
                r_pc_earth_frame=r_pc+pos_moon  # r & v in Earth frame
                v_pc_earth_frame=v_pc+vel_moon
                
                print("\n")
                print("***********************************")
                print("          Pericynthion")
                print("")
                #print("time since entering ")
                #print("Moon's SOI:         {0:.1f}".format(delta_t_pc.to(u.d))+\
                #          " or {0:.1f}".format(delta_t_pc.to(u.h))+"  or  {0:0.2f}".format(delta_t_pc))
                print("Total time:         {0:.1f}".format(t_total_pc.to(u.d))+\
                          " or {0:.1f}".format(t_total_pc.to(u.h))+"  or  {0:0.2f}".format(t_total_pc))
                print("altitude:           {0:0.5}".format(alt_pc))
                print("radius:             {0:0.6}".format(r_pc_mag))
                print("speed:              {0:0.4}".format(v_pc_mag))
                print("RAAN:               {0:0.4}".format(raan_pc))
                print("argument of PC:     {0:0.4}".format(argp_pc.to(u.deg)))
                print("true anomaly at PC: {0:0.4}".format(nu_pc.to(u.deg)))
                print("")
                #print("r (Moon frame):   ",r_pc)
                #print("v (Moon frame):   ",v_pc)
                #print("")
                #print("r (Earth frame):  ",r_pc_earth_frame)
                #print("v (Earth frame):  ",v_pc_earth_frame)
                #print("")
                print('**********************************')
                print('   State Vector at Pericynthion   \n')
                print('To move spacecraft to Pericynthion, ')
                print('read the following to Simulation Director ')
                print('for manual entry to input parameter file:')
                print('----------------------------------\n')
                if FLAG_DELTAV:
                    print('delta-v (m/s)    {:,.1f}'.format(delta_v))
                else:
                    print('No delta-v.')
                print('t   (s)          {:,.0f}'.format(t_total_pc.to(u.s)))
                print('x0  (m)          {:,.0f}'.format(r_pc_earth_frame[0].to(u.m)))
                print('y0  (m)          {:,.0f}'.format(r_pc_earth_frame[1].to(u.m)))
                print('z0  (m)          {:,.0f}'.format(r_pc_earth_frame[2].to(u.m)))
                print('v0x (m/s)        {:,.1f}'.format(v_pc_earth_frame[0].to(u.m/u.s)))
                print('v0y (m/s)        {:,.1f}'.format(v_pc_earth_frame[1].to(u.m/u.s)))
                print('v0z (m/s)        {:,.1f}'.format(v_pc_earth_frame[2].to(u.m/u.s)))
                print('\nFor further orbit design, \n')
                print('Copy directly to input parameter file:')
                print('--------------------------------------\n')
                print('t             {:.0f}'.format(t_total_pc.to(u.s).value))
                print('x_0           {:.0f}'.format(r_pc_earth_frame[0].to(u.m).value))
                print('y_0           {:.0f}'.format(r_pc_earth_frame[1].to(u.m).value))
                print('z_0           {:.0f}'.format(r_pc_earth_frame[2].to(u.m).value))
                print('v_0x          {:.1f}'.format(v_pc_earth_frame[0].to(u.m/u.s).value))
                print('v_0y          {:.1f}'.format(v_pc_earth_frame[1].to(u.m/u.s).value))
                print('v_0z          {:.1f}'.format(v_pc_earth_frame[2].to(u.m/u.s).value))
                print('\n')
                FLAG_PC=1
        r_mag_prev=r_f_mag # Save current value as "previous" to compare
                           # against the next iteration.
        i=i+1
    if FLAG_PC==0:
        print('\n***************************************')
        print('* FAILED TO REACH MOON PC WITHIN 3 DAYS *')
        print('*****************************************\n')



# Plot trajectories
# -----------------

# Earth-Moon Transit Plot
# -----------------------
# Draw Earth (x-y plane):
P.clf()
theta_circle=range(361)
earth_x=[(R_Earth.value*math.cos(theta_temp*(math.pi/180.))+x_Earth) for theta_temp in theta_circle]
earth_y=[(R_Earth.value*math.sin(theta_temp*(math.pi/180.))+y_Earth) for theta_temp in theta_circle]
P.plot(earth_x,earth_y,color='green')
# Draw Moon (x-y plane):
moon_x=[(R_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
moon_y=[(R_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(moon_x,moon_y,color='gray')
# Draw Moon's SOI:
soi_x=[(R_SOI_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
soi_y=[(R_SOI_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(soi_x,soi_y,color='gray',linewidth=1.0, linestyle=':')
# Plot orbit (x-y plane):
if STAGE1_COMPUTE:
    P.plot(stage1_x,stage1_y,color='blue')
    #print stage1_x
if STAGE2_COMPUTE:
    P.plot(stage2_x,stage2_y,color='red')
for ii in range(len(annotate_label)):
    P.annotate(annotate_label[ii], (annotate_x[ii],annotate_y[ii]))
P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
P.xlabel('x (km)')
P.ylabel('y (km)')
P.title('Earth-Moon Transit')
if SAVE_PLOTS:
    P.savefig('orbit_e2m_earth_moon.pdf')
else:
    P.show()


# Moon SOI Plot
# -------------
P.clf()
P.xlim(x_Moon-R_SOI_Moon.value-5000,x_Moon+R_SOI_Moon.value+5000)
P.ylim(y_Moon-R_SOI_Moon.value-5000,y_Moon+R_SOI_Moon.value+5000)
# Draw Moon (x-y plane):
moon_x=[(R_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
moon_y=[(R_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(moon_x,moon_y,color='gray')
# Draw Moon's SOI:
soi_x=[(R_SOI_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
soi_y=[(R_SOI_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(soi_x,soi_y,color='gray',linewidth=1.0, linestyle=':')
# Plot orbit (x-y plane):
if STAGE1_COMPUTE:
    P.plot(stage1_x,stage1_y,color='blue')
    #print stage1_x
if STAGE2_COMPUTE:
    P.plot(stage2_x,stage2_y,color='red')
P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
P.xlabel('x (km)')
P.ylabel('y (km)')
P.title('Moon Sphere of Influence')
if SAVE_PLOTS:
    P.savefig('orbit_e2m_moon_soi.pdf')
else:
    P.show()
    

# Moon Close-up Plot
# ------------------
P.clf()
P.xlim(x_Moon-R_Moon.value-2000,x_Moon+R_Moon.value+2000)
P.ylim(y_Moon-R_Moon.value-2000,y_Moon+R_Moon.value+2000)
# Draw Moon (x-y plane):
moon_x=[(R_Moon.value*math.cos(theta_temp*(math.pi/180.))+x_Moon) for theta_temp in theta_circle]
moon_y=[(R_Moon.value*math.sin(theta_temp*(math.pi/180.))+y_Moon) for theta_temp in theta_circle]
P.plot(moon_x,moon_y,color='gray')
# Plot orbit (x-y plane):
if STAGE1_COMPUTE:
    P.plot(stage1_x,stage1_y,color='blue')
    #print stage1_x
if STAGE2_COMPUTE:
    P.plot(stage2_x,stage2_y,color='red')
P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
P.xlabel('x (km)')
P.ylabel('y (km)')
P.title("Close to Moon's Surface")
if SAVE_PLOTS:
    P.savefig('orbit_e2m_moon.pdf')
else:
    P.show()


# Earth Close-up Plot
# -------------------
P.clf()
P.xlim(x_Earth-R_Earth.value-2000,x_Earth+R_Earth.value+2000)
P.ylim(y_Earth-R_Earth.value-2000,y_Earth+R_Earth.value+2000)
# Draw Earth (x-y plane):
earth_x=[(R_Earth.value*math.cos(theta_temp*(math.pi/180.))+x_Earth) for theta_temp in theta_circle]
earth_y=[(R_Earth.value*math.sin(theta_temp*(math.pi/180.))+y_Earth) for theta_temp in theta_circle]
P.plot(earth_x,earth_y,color='green')
# Plot orbit (x-y plane):
if STAGE1_COMPUTE:
    P.plot(stage1_x,stage1_y,color='blue')
    #print stage1_x
if STAGE2_COMPUTE:
    P.plot(stage2_x,stage2_y,color='red')
P.gca().set_aspect('equal', adjustable='box') # Makes plot have square aspect ratio
P.xlabel('x (km)')
P.ylabel('y (km)')
P.title("Close to Earth's Surface")
if SAVE_PLOTS:
    P.savefig('orbit_e2m_earth.pdf')
else:
    P.show()


print('\nTo find spacecraft state vector at time t, ')
print('enter time in hours between',t_input.to(u.h),'and ',t_total_pc.to(u.h))
input_temp=input('Time: (hr) ')
t_selected=float(input_temp)*u.h

# Determine if t_selected is before or after passage into Moon's SOI
if t_selected<=t_entry:
    # Stage 1 orbit
    delta_t=t_selected-t_input   # Must start from beginning of calculation
    orbit_earth_leave_f=orbit_earth_leave.propagate(delta_t, method=kepler)
    r_f=(orbit_earth_leave_f.r).to(u.m)     # Convert to m
    v_f=(orbit_earth_leave_f.v).to(u.m/u.s) # Convert to m/s
    x_selected=r_f[0].value
    y_selected=r_f[1].value
    z_selected=r_f[2].value
    vx_selected=v_f[0].value
    vy_selected=v_f[1].value
    vz_selected=v_f[2].value
    

else:
    # Stage 2 orbit
    delta_t=t_selected-t_entry
    orbit_moon_f=orbit_moon_enter.propagate(delta_t, method=cowell) # Use Cowell for Moon
    r_selected_moon_frame=orbit_moon_f.r                # In Moon's frame
    v_selected_moon_frame=orbit_moon_f.v
    r_selected=(r_selected_moon_frame+pos_moon).to(u.m) # Convert to Earth frame
    v_selected=(v_selected_moon_frame+vel_moon).to(u.m/u.s)
    x_selected=r_selected[0].value    
    y_selected=r_selected[1].value    
    z_selected=r_selected[2].value    
    vx_selected=v_selected[0].value
    vy_selected=v_selected[1].value
    vz_selected=v_selected[2].value


    
# Print results:
print('*********************************')
print('  State Vector at Selected Time   \n')
print('To move spacecraft to position, ')
print('read the following to Simulation Director ')
print('for manual entry to input parameter file:')
print('----------------------------------\n')
if FLAG_DELTAV:
    print('delta-v (m/s)                  {:,.1f}'.format(delta_v))
    print('velocity after maneuver (m/s)  {:,.1f}'.format(v_0_mag))
else:
    print('No delta-v.')
    print('initial velocity: (m/s) {:,.1f}'.format(v_0_mag))
print('t   (s)          {:,.0f}'.format(t_selected.to(u.s)))
print('x0  (m)          {:,.0f}'.format(x_selected))
print('y0  (m)          {:,.0f}'.format(y_selected))
print('z0  (m)          {:,.0f}'.format(z_selected))
print('v0x (m/s)        {:,.1f}'.format(vx_selected))
print('v0y (m/s)        {:,.1f}'.format(vy_selected))
print('v0z (m/s)        {:,.1f}'.format(vz_selected))       
print('\nFor further trajectory design,')
print('Copy directly to input parameter file:')
print('--------------------------------------\n')
print('t             {:.0f}'.format(t_selected.to(u.s)))
print('x_0           {:.0f}'.format(x_selected))        
print('y_0           {:.0f}'.format(y_selected))        
print('z_0           {:.0f}'.format(z_selected))        
print('v_0x          {:.1f}'.format(vx_selected))       
print('v_0y          {:.1f}'.format(vy_selected))       
print('v_0z          {:.1f}'.format(vz_selected))       
print('\n')
