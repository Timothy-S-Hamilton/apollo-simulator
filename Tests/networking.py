import sys
import time
import socket
import struct
import random


class Timer(object):
    def __init__(self, duration, is_one_shot=False):
        self.duration = duration
        self.is_one_shot = is_one_shot
        
        self.running = False
        self.paused = False
        self.start_time = 0
        self.paused_passed_time = 0

        self.callbacks = set()
    
    def add_callback(self, cb):
        self.callbacks.add(cb)
    
    def remove_callback(self, cb):
        self.callbacks.remove(cb)
    
    def update(self):
        if not self.running or self.paused: 
            return
        
        current_time = time.time()
        
        if current_time - self.start_time >= self.duration:
            self.trigger()
        
    def trigger(self):
        for cb in self.callbacks:
            cb()
        
        if self.is_one_shot:
            self.stop()
        else:
            self.start()
    
    def elapsed(self):
        return time.time() - self.start_time
    
    def start(self):
        self.running = True
        self.paused = False
        self.start_time = time.time()

    def resume(self):
        self.paused = False
        self.start_time = time.time() - self.paused_passed_time

    def pause(self):
        self.paused = True
        self.paused_passed_time = time.time() - self.start_time
    
    def stop(self):
        self.running = False
        self.paused = False


class ServerNetworkProtocol(object):
    def __init__(self, ip, port, num_clients=10):
        self.server = SpaceFlightServer((ip, port), num_clients)

        self.transmit_commands = {}
        self.receive_commands = {}
        self.data_buffer = {}
        self.new_connection_callback = None

    def define_transmit_command(self, network_id, cid, format_str, is_variable=False):
        self.transmit_commands[network_id][cid] = (format_str, is_variable)
    
    def define_receive_command(self, network_id, cid, format_str, callback, is_variable=False):
        self.receive_commands[network_id][cid] = (format_str, callback, is_variable)
    
    def transmit_command(self, network_id, cid, data=[]):
        fmt_str, is_variable = self.transmit_commands[network_id][cid]
        if is_variable:
            self.server.client_transmit(network_id, chr(cid) + struct.pack("<I", len(data)) + data)
        else:
            self.server.client_transmit(network_id, chr(cid) + struct.pack(fmt_str, *data))
    
    def update(self):
        success, network_id = self.server.server_accept()
        if success:
            self.transmit_commands[network_id] = {}
            self.receive_commands[network_id] = {}
            self.data_buffer[network_id] = ""
            if self.new_connection_callback is not None:
                self.new_connection_callback(network_id)

        for network_id in self.server.get_client_ids():
            success, data, data_len = self.server.client_receive(network_id, 64, 0)
            if success and data_len != 0: 
                self.data_buffer[network_id] += data
            if len(self.data_buffer[network_id]) <= 0: continue
            
            # decode
            cid = ord(self.data_buffer[network_id][0])
            if cid in self.receive_commands[network_id]:
                fmt_str, callback, is_variable = self.receive_commands[network_id][cid]
                if is_variable:
                    if len(self.data_buffer[network_id]) < 5:
                        return
                    data_size = struct.unpack("<I", self.data_buffer[network_id][1:5])[0]
                else:
                    data_size = struct.calcsize(fmt_str)
                
                if len(self.data_buffer[network_id]) >= data_size + 1:
                    if is_variable:
                        data = self.data_buffer[network_id][2:data_size+2]
                        callback(network_id, data)
                        self.data_buffer[network_id] = self.data_buffer[network_id][data_size+5:]
                    else:
                        data = struct.unpack(fmt_str, self.data_buffer[network_id][1:data_size+1])
                        callback(network_id, data)
                        self.data_buffer[network_id] = self.data_buffer[network_id][data_size+1:]
            else:
                print "Warning, Unknown Command ID:", cid
                self.data_buffer[network_id] = self.data_buffer[network_id][1:]
    
    def close(self):
        self.server.server_shutdown()


class SpaceFlightServer(object):
    def __init__(self, location=('127.0.0.1', 10000), num_clients=10):
        # store all connected clients
        self.clients = {}
        self.network_ids = set()

        self.network_id_to_address = {}
        self.address_to_network_id = {}

        # handle to the socket
        self.handle = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        # put the socket in non-blocking mode
        self.handle.setblocking(0)

        # bind socket to desired address
        self.handle.bind(location)

        # tell socket to expect a number of clients
        self.handle.listen(num_clients)

    def __generate_network_id(self):
        while True:
            number = random.randint(1, (2**32)-1)
            if number not in self.network_ids:
                self.network_ids.add(number)
                return number

    def get_client_ids(self):
        return self.network_ids

    def get_network_id_from_address(self, address):
        return self.address_to_network_id[address]
    
    def get_address_from_network_id(self, network_id):
        return self.network_id_to_address[network_id]

    def server_accept(self):
        try:
            conn, addr = self.handle.accept()
            conn.setblocking(0)
            network_id = self.__generate_network_id()
            self.network_id_to_address[network_id] = addr
            self.address_to_network_id[addr] = network_id
            self.clients[network_id] = conn
        except socket.error:
            return False, -1
        
        return True, network_id

    def client_close(self, network_id):
        addr = self.network_id_to_address[network_id]
        self.clients[network_id].close()
        del self.clients[network_id]
        del self.address_to_network_id[addr]
        del self.network_id_to_address[network_id]
        self.network_ids.remove(network_id)
    
    def client_transmit(self, network_id, message):
        try:
            self.clients[network_id].sendall(message)
        except:
            return False
        return True
    
    def client_receive(self, network_id, amnt, timeout=0.1):
        start_time = time.time()
        if timeout == 0:
            try:
                data = self.clients[network_id].recv(amnt)
                return True, data, len(data)
            except socket.error:
                return False, "", 0
        elif timeout == -1:
            while True:
                try:
                    data = self.clients[network_id].recv(amnt)
                    return True, data, len(data)
                except socket.error:
                    return False, "", 0
        else:
            while time.time() - start_time < timeout:
                try:
                    data = self.clients[network_id].recv(amnt)
                    return True, data, len(data)
                except socket.error:
                    time.sleep(timeout / 10.0)
            return False, "", 0

    def server_shutdown(self):
        for network_id in self.network_ids.copy():
            self.client_close(network_id)
        self.handle.close()

# testing timer class
if __name__ == "__main__":
    def cb():
        print 'callback'
    
    t1 = Timer(3, True)
    t1.add_callback(cb)
    
    t1.start()
    print 'started', t1.elapsed()
    time.sleep(1)
    t1.pause()
    print 'paused', t1.elapsed()
    time.sleep(5)
    t1.resume()
    print 'resumed', t1.elapsed()

    while t1.running:
        print t1.elapsed()
        t1.update()
    
    print 'Done'
