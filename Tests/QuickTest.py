import math

def rotationMatrixToEulerAngles(matrix3x3):
    if matrix3x3[1][0] > 0.998:
        heading = math.atan2(matrix3x3[0][2], matrix3x3[2][2])
        attitude = math.pi / 2.0
        bank = 0
        return math.degrees(heading), math.degrees(attitude), math.degrees(bank)
    if matrix3x3[1][0] < -0.998:
        heading = math.atan2(matrix3x3[0][2], matrix3x3[2][2])
        attitude = -math.pi / 2.0
        bank = 0
        return math.degrees(heading), math.degrees(attitude), math.degrees(bank)
    heading = math.atan2(-matrix3x3[2][0], matrix3x3[0][0])
    bank = math.atan2(-matrix3x3[1][2], matrix3x3[1][1])
    attitude = math.asin(matrix3x3[1][0])
    return math.degrees(heading), math.degrees(attitude), math.degrees(bank)

def rotationMatrixToQuaternion(matrix3x3):
    m00, m01, m02 = matrix3x3[0]
    m10, m11, m12 = matrix3x3[1]
    m20, m21, m22 = matrix3x3[2]
    
    tr = m00 + m11 + m22

    if tr > 0:
        S = math.sqrt(tr + 1.0) * 2.0
        qw = 0.25 * S
        qx = (m21 - m12) / S
        qy = (m02 - m20) / S
        qz = (m10 - m01) / S
    elif (m00 > m11) and (m00 > m22):
        S = math.sqrt(1.0 + m00 - m11 - m22) * 2.0
        qw = (m21 - m12) / S
        qx = 0.25 * S
        qy = (m01 + m10) / S
        qz = (m02 + m20) / S
    elif m11 > m22:
        S = math.sqrt(1.0 + m11 - m00 - m22) * 2.0
        qw = (m02 - m20) / S
        qx = (m01 + m10) / S
        qy = 0.25 * S
        qz = (m12 + m21) / S
    else:
        S = math.sqrt(1.0 + m22 - m00 - m11) * 2.0
        qw = (m10 - m01) / S
        qx = (m02 + m20) / S
        qy = (m12 + m21) / S
        qz = 0.25 * S
    
    return qw, qx, qy, qz

def rotationMatrixToQuaternionAlt(matrix3x3):
    m00, m01, m02 = matrix3x3[0]
    m10, m11, m12 = matrix3x3[1]
    m20, m21, m22 = matrix3x3[2]

    w = math.sqrt(max(0.0, 1.0 + m00 + m11 + m22)) / 2.0
    x = math.sqrt(max(0.0, 1.0 + m00 - m11 - m22)) / 2.0
    y = math.sqrt(max(0.0, 1.0 - m00 + m11 - m22)) / 2.0
    z = math.sqrt(max(0.0, 1.0 - m00 - m11 + m22)) / 2.0
    
    x = math.copysign(x, m21 - m12)
    y = math.copysign(y, m02 - m20)
    z = math.copysign(z, m10 - m01)

    return w, x, y, z

testMatrix1 = [
    [-1, 0, 0],
    [0, 1, 0],
    [0, 0, -1]
]

print testMatrix1, rotationMatrixToEulerAngles(testMatrix1)

print rotationMatrixToQuaternion(testMatrix1), rotationMatrixToQuaternionAlt(testMatrix1)

"""
Earth Radius: 6,371 km
Moon Radius:  1,737 km
Command Module Radius: 1.955 m
"""
