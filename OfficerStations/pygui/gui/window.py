from __future__ import division
import pygui
from pygui.gui.widget import Widget
from pygui.gui.label import Label
from pygui.gui.button import Button


class Window(Widget):
    def __init__(self, x, y, width, height, title="", color=pygui.gray):
        super(Window, self).__init__(x, y, color)
        pygui.add_mouse_listener(self)
        self.width = width
        self.height = height
        self.rect = (x, y, width, height)
        self.header_rect = (x, y, width, height / 8)
        self.label = Label(x + width / 2, y, title)
        self.alive = True
        self.label.font_size = 1
        (text_w, text_h) = pygui.get_text_dimensions(self.label.font, self.label.font_size, self.label.text)
        while text_w < self.header_rect[2] and text_h < self.header_rect[3]:
            self.label.font_size += 1
            (text_w, text_h) = pygui.get_text_dimensions(self.label.font, self.label.font_size, self.label.text)
        while text_w > self.header_rect[2] or text_h > self.header_rect[3]:
            self.label.font_size -= 1
            (text_w, text_h) = pygui.get_text_dimensions(self.label.font, self.label.font_size, self.label.text)

        self.label.y += self.header_rect[3] / 2

        self.outline_color = pygui.silver
        self.header_color = pygui.maroon
        self.widgets = []
        self.down = False
        self.minimized = False
        self.grab_x_offset = 0
        self.grab_y_offset = 0
        self.exit = Button(self.x + self.width - self.header_rect[2] / 8, self.y, self.header_rect[2] / 8, self.header_rect[3], "X", self.set_dead)
        self.exit.normal_color = pygui.red
        self.min = Button(self.x + self.width - (self.header_rect[2] / 8) * 2, self.y, self.header_rect[2] / 8, self.header_rect[3], "-", self.toggle_minimized)
        self.min.normal_color = pygui.blue
        self.min.pressed_color = pygui.dark_gray

    def set_dead(self):
        self.alive = False

    def toggle_minimized(self):
        if self.minimized:
            self.minimized = False
        else:
            self.minimized = True

    def is_alive(self):
        return self.alive

    def add_widget(self, w):
        w.x = self.x + w.x
        w.y = self.y + self.header_rect[3] + w.y
        self.widgets.append(w)

    def update(self):
        if not self.alive:
            return

        if self.down:
            self.update_state()

        self.rect = (self.x, self.y, self.width, self.height)
        self.header_rect = (self.x, self.y, self.width, self.height / 8)
        self.label.x = self.x + self.width / 2
        self.label.y = self.y + self.header_rect[3] / 2

        for w in self.widgets:
            w.x = self.x + w.start_pos[0]
            w.y = self.y + w.start_pos[1] + self.header_rect[3]
            w.update()

        if not self.minimized:
            for w in self.widgets:
                w.update()

        self.exit.x = self.x + self.width - self.exit.width
        self.exit.y = self.y
        self.exit.update()
        self.min.x = self.x + self.width - self.min.width * 2
        self.min.y = self.y
        self.min.update()

    def update_state(self):
        (mx, my) = pygui.get_mouse_position()
        self.x = mx + self.grab_x_offset
        self.y = my + self.grab_y_offset

        self.rect = (self.x, self.y, self.width, self.height)
        self.header_rect = (self.x, self.y, self.width, self.height / 8)
        self.label.x = self.x + self.width / 2
        self.label.y = self.y + self.header_rect[3] / 2

        for w in self.widgets:
            w.x = self.x + w.start_pos[0]
            w.y = self.y + w.start_pos[1] + self.header_rect[3]
            w.update()

    def render(self):
        if not self.alive:
            return

        if not self.minimized:
            pygui.draw_rect(self.rect, self.color)
            pygui.draw_rect(self.rect, self.outline_color, 2)

        pygui.draw_rect(self.header_rect, self.header_color)
        pygui.draw_rect(self.header_rect, self.outline_color, 2)

        self.label.render()
        self.exit.render()
        self.min.render()

        if not self.minimized:
            for w in self.widgets:
                w.render()

    def event(self, e_type, e_value):
        if e_type == "MouseDown" and e_value == 1:
            temp = (self.header_rect[0], self.header_rect[1], self.header_rect[2] - (self.header_rect[2] / 8) * 2, self.header_rect[3])
            if pygui.contains(temp, pygui.get_mouse_position()):
                if not self.down:
                    self.grab_x_offset = self.x - pygui.get_mouse_position()[0]
                    self.grab_y_offset = self.y - pygui.get_mouse_position()[1]
                    self.down = True
        if e_type == "MouseUp" and e_value == 1:
            self.down = False
