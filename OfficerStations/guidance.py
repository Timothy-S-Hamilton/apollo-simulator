import sys
import time
import struct
import pygame
import pygui
from networking import *
from datetime import timedelta
from pygui.gui.button import Button
from pygui.gui.label import Label

DEBUG = False

CLIENT_TYPE = 2

met = 0
orbital_eccentricity = 0
altitude_at_periapsis = 0
altitude_at_apoapsis = 0
sphere_of_influence = 0
attitude_x = 0
attitude_z = 0
att_hold = 0

network_location = raw_input("Server IP & Port: ") if len(sys.argv) < 2 else sys.argv[1]

ip = network_location.split(":")[0]
port = int(network_location.split(":")[1])

print "Connecting to:", ip + ":" + str(port)

protocol = ClientNetworkProtocol(ip, port)

def set_met(data):
    global met
    if DEBUG: print "set_met:", data
    met = data[0]

def set_orbital_eccentricity(data):
    global orbital_eccentricity
    if DEBUG: print "set_orbital_eccentricity:", data
    orbital_eccentricity = data[0]

def set_altitude_at_periapsis(data):
    global altitude_at_periapsis
    if DEBUG: print "set_altitude_at_periapsis:", data
    altitude_at_periapsis = data[0]

def set_altitude_at_apoapsis(data):
    global altitude_at_apoapsis
    if DEBUG: print "set_altitude_at_apoapsis:", data
    altitude_at_apoapsis = data[0]

def set_sphere_of_influence(data):
    global sphere_of_influence
    if DEBUG: print "set_sphere_of_influence:", data
    sphere_of_influence = data[0]

def set_attitude_x(data):
    global attitude_x
    if DEBUG: print "set_attitude_x:", data
    attitude_x = data[0]

def set_attitude_z(data):
    global attitude_z
    if DEBUG: print "set_attitude_z:", data
    attitude_z = data[0]

def set_att_hold(data):
    global att_hold
    if DEBUG: print "set_att_hold:", data
    att_hold = data[0]

def client_type_request(data):
    if DEBUG: print "client_type_request:", data
    protocol.transmit_command(1, [CLIENT_TYPE])

protocol.define_receive_command(0, "<I", set_met)
protocol.define_receive_command(1, "<f", set_orbital_eccentricity)
protocol.define_receive_command(2, "<f", set_altitude_at_periapsis)
protocol.define_receive_command(3, "<f", set_altitude_at_apoapsis)
protocol.define_receive_command(4, "<B", set_sphere_of_influence)
protocol.define_receive_command(5, "<b", set_attitude_x)
protocol.define_receive_command(6, "<b", set_attitude_z)
protocol.define_receive_command(7, "<B", set_att_hold)

protocol.define_transmit_command(1, "<B") # Send Client Type
protocol.define_receive_command(14, "", client_type_request)

pygui.init("Guidance Officer (GUIDO)", (1280, 720))

running = True
clock = pygame.time.Clock()

labels = []

last_gen_label = None
def make_label(text, x=None, y=None, l_font_size=32):
    global last_gen_label

    spacing = 5
    x_start = 5
    y_start = 5 if last_gen_label is None else last_gen_label.y + last_gen_label.height + spacing
    
    x_pos = x_start if x is None else x
    y_pos = y_start if y is None else y

    l = Label(x_pos, y_pos, text, font_size=l_font_size)
    l.halign = 'left'
    l.valign = 'top'
    last_gen_label = l
    labels.append(l)
    return l

met_label = make_label("MET: 000:00:00")
orbital_eccentricity_label = make_label("Orbital Eccentricity: 0")
altitude_at_periapsis_label = make_label("Altitude at Periapsis (km): 0")
altitude_at_apoapsis_label = make_label("Altitude at Apoapsis (km): 0")
sphere_of_influence_label = make_label("Sphere of Influence: Earth")
attitude_x_label = make_label("Attitude X: Prograde")
attitude_z_label = make_label("Attitude Z: Head-Up")
att_hold_label = make_label("Attitude Hold: False")

def prefix(s, ch, cnt):
    while len(s) < cnt:
        s = ch + s
    return s

done = False
while not done:
    done = pygui.update()

    protocol.update()

    # ------ Update MET Text ------
    td = timedelta(seconds=met)
    s = td.total_seconds()
    hours, remainder = divmod(s, 3600)
    minutes = remainder // 60
    seconds = round(remainder - (minutes * 60))
    hours_str = prefix(str(int(hours)), '0', 3)
    minutes_str = prefix(str(int(minutes)), '0', 2)
    seconds_str = prefix(str(int(seconds)), '0', 2)
    met_label.text = "MET: " + hours_str + ":" + minutes_str + ":" + seconds_str
    # ------ Update MET Text ------

    orbital_eccentricity_label.text = "Orbital Eccentricity: " + str(round(orbital_eccentricity, 5))
    altitude_at_periapsis_label.text = "Altitude at Periapsis (km): " + str(round(altitude_at_periapsis, 5))
    altitude_at_apoapsis_label.text = "Altitude at Apoapsis (km): " + str(round(altitude_at_apoapsis, 5))    
    sphere_of_influence_label.text = "Sphere of Influence: " + ("Earth" if sphere_of_influence == 0 else "Moon")
    attitude_x_label.text = "Attitude X: " + ("Prograde" if attitude_x == 1 else "Retrograde")
    attitude_z_label.text = "Attitude Z: " + ("Head-Up" if attitude_z == 1 else "Head-Down")
    att_hold_label.text = "Attitude Hold: " + ("True" if att_hold == 1 else "False")

    pygui.clear_screen()

    for l in labels: l.render()

    pygui.display_flip()

protocol.close()
pygui.quit()
