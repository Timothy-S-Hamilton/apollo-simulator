﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using UnityEngine;
using UnityEngine.UI;

public class HardwareManager : MonoBehaviour
{
    public InputField ComInputField;
    public Text ErrorText;
    public Button ConnectButton;

    private bool _abortLed = false;
    private bool _engineLed = false;
    private bool[] _buttonStates = {false, false, false, false, false, false, false, false, false};

    private SerialPort _port;
    private bool _deviceReady = false;
    private bool _buttonsChanged = false;

    public bool ButtonsHaveChanged()
    {
        bool temp = _buttonsChanged;
        _buttonsChanged = false;
        return temp;
    }

    public bool IsReady()
    {
        return _deviceReady;
    }

    public void ConnectButtonClicked()
    {
        if (!_deviceReady)
        {
            Connect();
        }
        else
        {
            StopCoroutine("StatusSync");
            _port.Close();
            _deviceReady = false;
            ComInputField.gameObject.SetActive(true);
            ConnectButton.GetComponentInChildren<Text>().text = "Connect";
        }
    }

    public void SetLedStates(bool abort, bool engine)
    {
        _abortLed = abort;
        _engineLed = engine;
    }

    public Tuple<bool, bool> GetLedStates()
    {
        return new Tuple<bool, bool>(_abortLed, _engineLed);
    }

    public bool[] GetButtonStates()
    {
        return _buttonStates;
    }

    private bool SafeReadByte(out char data)
    {
        try
        {
            data = (char)_port.ReadByte();
        }
        catch (TimeoutException)
        {
            data = '\0';
            return false;
        }

        return true;
    }

    private bool SafeWriteByte(char data)
    {
        try
        {
            char[] b = new char[1];
            b[0] = data;
            _port.Write(b, 0, 1);
        }
        catch (TimeoutException)
        {
            return false;
        }

        return true;
    }

    private void Connect()
    {
        _port = new SerialPort(ComInputField.text, 115200, Parity.None)
        {
            ReadTimeout = 100,
            WriteTimeout = 100
        };

        ComInputField.interactable = false;
        ConnectButton.interactable = false;
        ErrorText.color = Color.red;

        try
        {
            _port.Open();
            ErrorText.gameObject.SetActive(false);
        }
        catch (IOException)
        {
            ErrorText.text = "Error!";
            ErrorText.gameObject.SetActive(true);

            ComInputField.interactable = true;
            ConnectButton.interactable = true;

            return;
        }

        if(!SafeWriteByte('?'))
        {
            _port.Close();
            ErrorText.text = "Failed!";
            ErrorText.gameObject.SetActive(true);
            ComInputField.interactable = true;
            ConnectButton.interactable = true;
            return;
        }

        StartCoroutine("DeviceChecker");
    }

    private IEnumerator DeviceChecker()
    {
        int failCount = 0;

        ErrorText.color = Color.yellow;
        ErrorText.text = "Checking...";
        ErrorText.gameObject.SetActive(true);

        for (;;)
        {
            yield return new WaitForSeconds(1);

            char data;
            if (SafeReadByte(out data) && data == 'R')
            {
                _deviceReady = true;
                _port.DiscardInBuffer();
                _port.DiscardOutBuffer();
                break;
            }

            if (failCount > 5)
            {
                ErrorText.color = Color.red;
                ErrorText.text = "Failed!";
                ErrorText.gameObject.SetActive(true);
                yield return new WaitForSeconds(1);
                ComInputField.interactable = true;
                ConnectButton.interactable = true;
                yield break;
            }

            failCount++;
        }

        ErrorText.color = Color.green;
        ErrorText.text = "Ready!";
        ErrorText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);

        ErrorText.gameObject.SetActive(false);
        ComInputField.interactable = true;
        ComInputField.gameObject.SetActive(false);
        ConnectButton.interactable = true;
        ConnectButton.GetComponentInChildren<Text>().text = "Disconnect";

        StartCoroutine("StatusSync");
    }

    private IEnumerator StatusSync()
    {
        for (;;)
        {
            byte ledData = 0x20;
            if (_abortLed) { ledData |= 0x01; }
            if (_engineLed) { ledData |= 0x02; }
            SafeWriteByte((char)ledData);
            SafeWriteByte((char) 0x10);

            bool[] _tempButtonStates = new bool[9];

            char d1;
            char d2;
            if (SafeReadByte(out d1) && SafeReadByte(out d2))
            {
                int lower = d1;
                int upper = d2;
                int composite = (upper << 8) | lower;
                for (int i = 0; i < 9; i++)
                {
                    _tempButtonStates[i] = Convert.ToBoolean(composite & (1 << i));
                }
            }
            else
            {
                Debug.Log("Button Read Failed!");
            }

            bool same = true;
            for (uint i = 0; i < 9; i++)
            {
                if (_tempButtonStates[i] == _buttonStates[i]) continue;
                same = false;
                break;
            }

            if (!same)
            {
                _buttonStates = _tempButtonStates;
                _buttonsChanged = true;
            }

            yield return new WaitForSeconds(0.05f);
        }
    }

    private void OnApplicationQuit()
    {
        if (_port != null && _port.IsOpen)
        {
            _port.Close();
        }
    }
}
